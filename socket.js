var server = require('http').Server();
var io = require('socket.io')(server);

var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('test-channel');

redis.on('message', function(channel, message){

    message = JSON.parse(message);
    console.log(message.data);
    //console.log('hello');
    //io.to(channel + message.data.marketId).emit(channel + '.' + message.data.marketId + ':' + message.event, message.data);
    io.emit(channel + '.' + message.data.orderDetails[0].marketId + ':' + message.event, message.data);
    //console.log(channel, message.data.marketId);


});

server.listen(3002);
