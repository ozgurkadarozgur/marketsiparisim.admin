<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneConfirmationStatusColumnToCustomerUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customerUsers', function (Blueprint $table){
            $table->integer('phoneConfirmationStatus')->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customerUsers', function (Blueprint $table){
            $table->dropColumn('phoneConfirmationStatus');
        });
    }
}
