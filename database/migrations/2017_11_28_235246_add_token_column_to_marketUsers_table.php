<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokenColumnToMarketUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketUsers', function (Blueprint $table){
           $table->string('token')->nullable();
           $table->date('tokenExpire')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketUsers', function (Blueprint $table){
            $table->dropColumn('token');
            $table->dropColumn('tokenExpire');
        });
    }
}
