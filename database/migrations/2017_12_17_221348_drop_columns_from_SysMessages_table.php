<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsFromSysMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysMessages', function (Blueprint $table){
           $table->dropColumn('senderId');
           $table->dropColumn('owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sysMessages', function (Blueprint $table){
            $table->integer('senderId');
            $table->string('owner');
        });
    }
}
