<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewProductRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newProductRequests', function (Blueprint $table){
           $table->increments('id');
           $table->integer('marketId');
           $table->string('productName');
           $table->text('productDescription');
           $table->double('price');
           $table->text('productPhoto');
           $table->integer('statusId');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newProductRequests');
    }
}
