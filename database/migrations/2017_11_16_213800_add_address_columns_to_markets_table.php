<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressColumnsToMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('markets', function (Blueprint $table){
           $table->integer('cityId');
           $table->integer('countyId');
           $table->integer('areaId');
           $table->integer('neighborhoodId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('markets', function (Blueprint $table){
            $table->dropColumn('cityId');
            $table->dropColumn('countyId');
            $table->dropColumn('areaId');
            $table->dropColumn('neighborhoodId');
        });
    }
}
