<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sysInvoices', function (Blueprint $table){
            $table->uuid('id')->primary();
            $table->integer('marketId');
            $table->double('totalBill');
            $table->date('startDate');
            $table->date('endDate');
            $table->double('vatRate');
            $table->boolean('isPaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sysInvoices');
    }
}
