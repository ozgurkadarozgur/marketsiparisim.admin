<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketRecommendations', function (Blueprint $table){
           $table->uuid('id')->primary();
           $table->integer('customerId');
           $table->string('marketName');
           $table->string('latitude');
           $table->string('longitude');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketRecommendations');
    }
}
