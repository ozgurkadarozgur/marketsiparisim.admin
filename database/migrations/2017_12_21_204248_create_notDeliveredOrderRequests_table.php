<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotDeliveredOrderRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notDeliveredOrderRequests', function (Blueprint $table){
           $table->uuid('id')->primary();
           $table->string('orderId');
           $table->integer('reasonId');
           $table->string('description');
           $table->integer('statusId');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notDeliveredOrderRequests');
    }
}
