<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiresAtColumnToPhoneConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phoneConfirmations', function (Blueprint $table){
           $table->dateTime('expires_at')->default(\Carbon\Carbon::now()->addMinute(3)->toDateTimeString());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phoneConfirmations', function (Blueprint $table){
            $table->dropColumn('expires_at');
        });
    }
}
