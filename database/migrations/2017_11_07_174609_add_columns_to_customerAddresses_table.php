<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCustomerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customerAddresses', function (Blueprint $table){
           $table->text('addressDescription');
           $table->string('longitude');
           $table->string('latitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customerAddresses', function (Blueprint $table){
            $table->dropColumn('addressDescription');
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
        });
    }
}
