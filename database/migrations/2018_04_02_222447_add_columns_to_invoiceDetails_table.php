<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoiceDetail', function (Blueprint $table){
            $table->float('commission')->nullable();
            $table->float('debtToSystem')->nullable();
            $table->float('credit')->nullable();
            $table->float('final')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoiceDetail', function (Blueprint $table){
            $table->dropIfExists('commission');
            $table->dropIfExists('debtToSystem');
            $table->dropIfExists('credit');
            $table->dropIfExists('final');
        });
    }
}
