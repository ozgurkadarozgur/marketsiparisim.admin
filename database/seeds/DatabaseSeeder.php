<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('customerUsers')->insert([
            'name'=>'ozgur',
            'email'=>'ozgurozdemirci@gmail.com',
            'password'=>bcrypt('1234567'),
            'phone'=>'5318282332'
        ]);

    }
}
