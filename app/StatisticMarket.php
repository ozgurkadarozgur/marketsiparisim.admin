<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StatisticMarket extends Model
{
    public static function getOrderCount($marketId)
    {
        $orderCount = Order::where('marketId', $marketId)
            ->count();
        return $orderCount;
    }

    public static function getCountUserHasOrdered($marketId)
    {
        $userCount = Order::where('marketId', $marketId)
            ->count(DB::raw('distinct orders.customerId'));

        return $userCount;
    }

    public static function getDailyOrderCountByDateRange($marketId, $startDate, $endDate)
    {
        return DB::select("SELECT count(*) as count, date(created_at) as date FROM orders
                            WHERE
                            created_at BETWEEN '$startDate' and '$endDate'
                            GROUP BY date(created_at)");
    }

    public static function getProductCount($marketId)
    {
        return Stock::where('marketId', $marketId)
            ->count();
    }

    public static function getDeniedOrderCount($marketId)
    {
        return Order::where('marketId', $marketId)
            ->where('status', OrderStatus::$denied)
            ->count();
    }

    public static function getCanceledOrderCount($marketId)
    {
        return Order::where('marketId', $marketId)
            ->where('status', OrderStatus::$canceled)
            ->count();
    }

    public static function getCreditCartOrderCount($marketId)
    {
        return Order::where('marketId', $marketId)
                ->where('paymentMethodId', PaymentMethod::$creditCart)
                ->count();
    }

    public static function getTotalPriceCreditCartOrder($marketId)
    {

    }

    public static function getCashOrderCount($marketId)
    {
        return Order::where('marketId', $marketId)
            ->where('paymentMethodId', PaymentMethod::$cash)
            ->count();
    }

}
