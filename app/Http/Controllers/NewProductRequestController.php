<?php

namespace App\Http\Controllers;

use App\NewProductRequest;
use App\NewProductRequestStatuses;
use App\Product;
use App\SubCategory;
use App\TopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewProductRequestController extends Controller
{
    public function index()
    {
        $productRequests = NewProductRequest::getProductRequests();
        return view('newProductRequest.index', compact('productRequests'));
    }

    public function show($id)
    {
        $requestedProduct = NewProductRequest::getRequestedProduct($id);
        $topCategories = TopCategory::getAll();
        $subCategories = SubCategory::getSubCategoryByTopCatId(1);
        return view('newProductRequest.show', compact('requestedProduct', 'topCategories', 'subCategories'));
    }

    public function approve(Request $request, $id)
    {
        $subCatId = $request->input('subCatId');
        $productId = NewProductRequest::saveAsStock($id, $subCatId);
        NewProductRequest::changeStatus($id, NewProductRequestStatuses::$approved);
        $requestedProduct = NewProductRequest::getRequestedProduct($id);
        $photoName = $requestedProduct->productName;
        $photoName = CharacterTransform::transform($photoName);
        $newProductPhotos = json_decode($requestedProduct->productPhoto);
        $photoUrl = 'productImages/' . $subCatId . '/' . $productId;
        $this->savePhoto($productId, $newProductPhotos, $photoName, $photoUrl);
        return redirect()->back()->with('message', 'Ürün stoklara eklendi !');
    }

    public function cancel($id)
    {
        NewProductRequest::changeStatus($id, NewProductRequestStatuses::$canceled);
        return redirect()->back()->with('message', 'Ürün onaylanmadı!');
    }

    public function savePhoto($productId, $newProductPhotos, $photoName, $photoUrl)
    {
        Size::removeSizes();
        Size::newSize('small', 109, 109);
        Size::newSize('medium', 226, 226);
        Size::newSize('large', 400, 400);
        Size::newSize('xLarge', 600, 600);

        $sizeList = Size::getSizeList();
        $obj = (object)[];
        foreach ($sizeList as $size) {

            $prop = $size->sizeName;
            $newPhoto = $newProductPhotos->$prop;

            $i = pathinfo(Storage::url('app/' . $newPhoto));
            $extension = $i["extension"];

            $path = $photoUrl . '/' . $size->sizeName . '-' . $photoName . '.' . $extension;
            $obj->$prop = $path;

            Storage::copy($newPhoto, $path);

        }
        Product::updatePhotoName($productId, json_encode($obj));
    }

}
