<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class AdminSettingController extends Controller
{
    public function index()
    {
        return view('adminSetting.index');
    }

    public function changePassword()
    {
        return view('adminSetting.changePassword');
    }

    public function updatePassword(Request $request)
    {
        $admin = User::find(1);

        $this->validate($request,
            [
                'oldPassword' => 'required|match_with_old_admin_password:oldPassword',
                'newPassword' => 'required|same:newPasswordAgain|different:oldPassword',
                'newPasswordAgain' => 'required',
            ],
            [
                'oldPassword.required' => 'Alan boş bırakılamaz',
                'oldPassword.match_with_old_admin_password' => 'Eski şifre eşleşmedi.',
                'newPassword.required' => 'Alan boş bırakılamaz.',
                'newPassword.same' => 'Yeni şifreler uyuşmuyor.',
                'newPassword.different' => 'Yeni şifre ve eski şifre aynı olamaz.',
                'newPasswordAgain.required' => 'Alan boş bırakılamaz.'
            ]
        );

        $newPassword = $request->input('newPassword');
        $admin->password = bcrypt($newPassword);
        $admin->save();
        return redirect()->back()->with('message', 'Şifre başarıyla değiştirildi.');
    }

}
