<?php

namespace App\Http\Controllers;

use App\SubCategory;
use App\TopCategories;
use App\TopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topCategories = TopCategory::all();
        return view('category.index', compact('topCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function saveTopCategoryPhotos($categoryId, $photo, $photoUrl)
    {
        Size::removeSizes();
        Size::newSize('small', 90, 108);
        Size::newSize('medium', 200, 200);
        Size::newSize('large', 470, 564);
        Size::newSize('xLarge', 600, 720);
        $sizeList = Size::getSizeList();
        $obj = (object)[];
        foreach ($sizeList as $size) {

            $hashName = $photoUrl . '/' . $size->sizeName;
            $path = $photo->hashName($hashName);

            $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
            Storage::put($path, (string)$img->encode());

            $prop = $size->sizeName;
            $obj->$prop = $path;

        }
        TopCategory::updatePhotoName($categoryId, json_encode($obj));
    }


    public function saveSubCategoryPhotos($categoryId, $photo, $photoUrl)
    {
        Size::removeSizes();
        Size::newSize('small', 90, 108);
        Size::newSize('medium', 200, 200);
        Size::newSize('large', 470, 564);
        Size::newSize('xLarge', 600, 720);
        $sizeList = Size::getSizeList();
        $obj = (object)[];
        foreach ($sizeList as $size) {

            $hashName = $photoUrl . '/' . $size->sizeName;
            $path = $photo->hashName($hashName);
            $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
            Storage::put($path, (string)$img->encode());

            $prop = $size->sizeName;
            $obj->$prop = $path;

        }
        SubCategory::updatePhotoName($categoryId, json_encode($obj));
    }

    public function storeTopCategory(Request $request)
    {
        $this->validate($request,
            [
                'topCategoryName' => 'required'
            ],
            [
                'topCategoryName.required' => 'Kategori adı boş birakilamaz.'
            ]
        );

        $categoryName = $request->input('topCategoryName');
        $photo = $request->file('photo');
        $categoryId = TopCategory::newTopCategory($categoryName);
        $photoUrl = 'photos/category/' . $categoryId;
        $this->saveTopCategoryPhotos($categoryId, $photo, $photoUrl);
        //$photoName = $categoryId . '.' . $photo->extension();
        //$photo->storeAs($photoUrl, $photoName);
        //TopCategory::updatePhotoName($categoryId, $photoUrl . '/' . $photoName);
        //

        return redirect()->back();
    }

    public function storeSubCategory(Request $request, $topCategoryId)
    {
        $this->validate($request,
            [
                'subCategoryName' => 'required'
            ],
            [
                'subCategoryName.required' => 'Kategori adı boş birakilamaz.'
            ]
        );

        $categoryName = $request->input('subCategoryName');
        $photo = $request->file('photo');
        $categoryId = SubCategory::newSubCategory($topCategoryId, $categoryName);
        $photoUrl = 'photos/category/' . $topCategoryId . '/sub/' . $categoryId;
        $this->saveSubCategoryPhotos($categoryId, $photo, $photoUrl);
        //$photoName = $categoryId . '.' . $photo->extension();
        //$photo->storeAs($photoUrl, $photoName);
        //SubCategory::updatePhotoName($categoryId, $photoUrl . '/' . $photoName);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showSubCategory($categoryId)
    {
        $subCategory = SubCategory::getCategory($categoryId);
        return view('category.show', compact('subCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
