<?php

namespace App\Http\Controllers;

use App\City;
use App\Market;
use Illuminate\Http\Request;

class MarketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $markets = Market::getMarkets();
        return view('market.index', compact('markets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::getCities();
        return view('market.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'latitude' => 'required',
                'longitude' => 'required',
                'marketName' => 'required',
                'inCharge' => 'required',
                'city' => 'required',
                'county' => 'required',
                'area' => 'required',
                'neighborhood' => 'required',
                'address' => 'required'
            ],
            [
                'latitude.required' => 'Marketin konumunu seçin !',
                'longitude.required' => 'Marketin konumunu seçin !',
                'marketName.required' => 'Market adı boş bırakılamaz !',
                'inCharge.required' => 'Yetkili kişi boş bırakılamaz !',
                'city.required' => 'Şehir boş bırakılamaz !',
                'county.required' => 'İlçe boş bırakılamaz !',
                'area.required' => 'Bölge boş bırakılamaz !',
                'neighborhood.required' => 'Mahalle boş bırakılamaz !',
                'address.required' => 'Adres boş bırakılamaz !'
            ]
        );

        $marketName = $request->input('marketName');
        $inCharge = $request->input('inCharge');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $city = $request->input('city');
        $county = $request->input('county');
        $area = $request->input('area');
        $neighborhood = $request->input('neighborhood');

        Market::newMarket($marketName, $inCharge, $latitude, $longitude, $city, $county, $area, $neighborhood);
        return redirect()->back()->with('message', 'Market başarıyla ekledi !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($marketId)
    {
        $market = Market::getMarket($marketId);
        return view('market.show', compact('market'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function map()
    {
        $markets = Market::getAllMarkets()->toJson();
        return view('market.map', compact('markets'));
    }

    //ajax

    public function getMarketList()
    {
        return Market::all();
    }

    //end ajax

}
