<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderCancelReason;
use App\OrderDenyReason;
use App\OrderNotDeliverReason;
use Illuminate\Http\Request;

class OrderController extends Controller
{


    public function denyReasons()
    {
        $reasons = OrderDenyReason::getReasons();
        return view('order.cancelReasons', compact('reasons'));
    }


}
