<?php

namespace App\Http\Controllers;

use App\Market;
use App\SysMessage;
use Illuminate\Http\Request;

class SysMessageController extends Controller
{
    public function create($marketId)
    {
        $market = Market::getMarket($marketId);
        $messages = SysMessage::getMessages($marketId);
        return view('sysMessage.create', compact('market', 'messages'));
    }

    public function send(Request $request, $marketId)
    {
        $message = $request->input('message');
        SysMessage::createMessage($marketId, $message);
        return redirect()->back()->with('message', 'Mesaj gönderildi!');
    }

}
