<?php

namespace App\Http\Controllers;

use App\OrderNotDeliverReason;
use Illuminate\Http\Request;

class OrderNotDeliverReasonController extends Controller
{
    public function index()
    {
        $reasons = OrderNotDeliverReason::getReasons();
        return view('orderNotDeliverReason.index', compact('reasons'));
    }

    public function create()
    {
        return view('orderNotDeliverReason.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'reason'=>'required'
            ],
            [
                'reason.required'=>'Boş bırakılamaz.'
            ]
        );

        $reason = $request->input('reason');
        OrderNotDeliverReason::newReason($reason);
        return redirect()->back()->with('message', 'Sipariş reddetme nedeni başarıyla eklendi !');

    }

}
