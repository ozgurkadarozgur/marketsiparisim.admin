<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 28.03.2018
 * Time: 20:50
 */

namespace App\Http\Controllers;


class SysNotification
{
    public static function sendProductNotification($marketName, $bodyMessage, $tokens, $data)
    {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'registration_ids' => $tokens,
            'content_available' => true,
            'priority' => 'high',
            'notification' => [
                'body' => $bodyMessage,
                'title' => $marketName
            ],

            'data' => $data

        );

        /*

        '{"small":"photos\/category\/1\/sub\/2\/product\/small-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","medium":"photos\/category\/1\/sub\/2\/product\/medium-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","large":"photos\/category\/1\/sub\/2\/product\/large-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","xLarge":"photos\/category\/1\/sub\/2\/product\/xLarge-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg"}'

         */

        $headers = array(
            'Authorization:key = AAAAsHVPJrU:APA91bE9jk9yGukEvmxRWPMgv1LfbcGN-4EyqE_nCYaHjjaFHPBtGBVF5HIRir9KmvM6037urodBex7Y7HU5gEErb0RFaFvHlbleY9sdBudDR46sj9uUhGFz0bXh3l_0M_g_840p614k',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;

    }

    public static function sendGeneralNotification($bodyMessage, $tokens, $data)
    {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'registration_ids' => $tokens,
            'content_available' => true,
            'priority' => 'high',
            'notification' => [
                'body' => $bodyMessage,
                'title' => 'Market Siparişim'
            ],

            'data' => $data

        );

        /*

        '{"small":"photos\/category\/1\/sub\/2\/product\/small-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","medium":"photos\/category\/1\/sub\/2\/product\/medium-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","large":"photos\/category\/1\/sub\/2\/product\/large-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","xLarge":"photos\/category\/1\/sub\/2\/product\/xLarge-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg"}'

         */

        $headers = array(
            'Authorization:key = AAAAsHVPJrU:APA91bE9jk9yGukEvmxRWPMgv1LfbcGN-4EyqE_nCYaHjjaFHPBtGBVF5HIRir9KmvM6037urodBex7Y7HU5gEErb0RFaFvHlbleY9sdBudDR46sj9uUhGFz0bXh3l_0M_g_840p614k',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;

    }

}