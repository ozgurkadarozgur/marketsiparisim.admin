<?php

namespace App\Http\Controllers;

use App\OrderCancelReason;
use Illuminate\Http\Request;

class OrderCancelReasonController extends Controller
{
    public function index()
    {
        $reasons = OrderCancelReason::getReasons();
        return view('orderCancelReason.index', compact('reasons'));
    }

    public function create()
    {
        return view('orderCancelReason.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'reason'=>'required'
            ],
            [
                'reason.required'=>'Boş bırakılamaz !'
            ]);

        $reason = $request->input('reason');
        OrderCancelReason::newReason($reason);
        return redirect()->back()->with('message', 'Sipariş iptal nedeni başarıyla eklendi!');

    }

}
