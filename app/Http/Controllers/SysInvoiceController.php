<?php

namespace App\Http\Controllers;

use App\SysInvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SysInvoiceController extends Controller
{

    public function index()
    {

        $now = Carbon::now();
        $date = Carbon::parse($now->toDateString())->format('d.m.Y');
        $startOfWeek = Carbon::now()->startOfWeek()->toDateString();
        $endOfWeek = Carbon::now()->endOfWeek()->toDateString();

        $invoiceInfo = SysInvoice::getInvoiceInfoByMarketId(1, $startOfWeek, $endOfWeek);
        $totalBill = $invoiceInfo->totalBill;
        $marketName = $invoiceInfo->marketName;

        $timestamp = $now->timestamp;
        $year = $now->year;
        $month = $now->month;
        $hour = $now->hour;
        $minute = $now->minute;
        $second = $now->second;

        $kickBack = $totalBill * 5 / 100;
        $totalVat = $kickBack * 18 / 100;
        $totalNet = $kickBack + $totalVat;

        $explodeFormat = explode('.', $kickBack);
        $moneyToText1 = MoneyConversionToText::int_to_words($explodeFormat[0]);
        $moneyToText2 = MoneyConversionToText::int_to_words($explodeFormat[1]);



        $invoice = (object)[
            'date' => $date,
            'totalBill' => 'Toplam Tutar:' . $totalBill . ' TL',
            'marketName' => $marketName,
            'timestamp' => $timestamp,
            'year' => $year,
            'month' => $month,
            'hour' => $hour,
            'minute' => $minute,
            'second' => $second,
            'vatRate' => 18,
            'totalVat' => $totalVat,
            'total' => $kickBack,
            'totalNet' => $totalNet,
            'totalNetInText' => $moneyToText1 . ' TL ' . $moneyToText2 . ' KRS'
        ];


        $this->extractInvoice($invoice);

    }

    public function extractInvoice($invoice)
    {

        $xml = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<SALES_INVOICES>
  <INVOICE DBOP=\"INS\" >
    <TYPE>9</TYPE>
    <NUMBER>DENEME</NUMBER>
    <DATE>$invoice->date</DATE>
    <TIME>$invoice->timestamp</TIME>
    <ARP_CODE>120.11111</ARP_CODE>
    <POST_FLAGS>247</POST_FLAGS>
    <VAT_RATE>$invoice->vatRate</VAT_RATE>
    <TOTAL_DISCOUNTED>0</TOTAL_DISCOUNTED>
    <TOTAL_VAT>$invoice->totalVat</TOTAL_VAT>
    <TOTAL_GROSS>$invoice->total</TOTAL_GROSS>
    <TOTAL_NET>$invoice->totalNet</TOTAL_NET>
    <TC_NET>$invoice->totalNet</TC_NET>
    <RC_XRATE>1</RC_XRATE>
    <RC_NET>$invoice->totalNet</RC_NET>
    <CREATED_BY>1</CREATED_BY>
    <DATE_CREATED>$invoice->date</DATE_CREATED>
    <HOUR_CREATED>$invoice->hour</HOUR_CREATED>
    <MIN_CREATED>$invoice->minute</MIN_CREATED>
    <SEC_CREATED>$invoice->second</SEC_CREATED>
    <CURRSEL_TOTALS>1</CURRSEL_TOTALS>
    <DATA_REFERENCE>8027</DATA_REFERENCE>
    <DISPATCHES>
    </DISPATCHES>
    <TRANSACTIONS>
      <TRANSACTION>
        <TYPE>4</TYPE>
        <MASTER_CODE>005</MASTER_CODE>
        <GL_CODE2>391.01.001</GL_CODE2>
        <QUANTITY>1</QUANTITY>
        <PRICE>$invoice->total</PRICE>
        <TOTAL>$invoice->total</TOTAL>
        <RC_XRATE>1</RC_XRATE>
        <DESCRIPTION>$invoice->totalBill</DESCRIPTION>
        <UNIT_CODE>ADET</UNIT_CODE>
        <UNIT_CONV1>1</UNIT_CONV1>
        <UNIT_CONV2>1</UNIT_CONV2>
        <VAT_RATE>$invoice->vatRate</VAT_RATE>
        <VAT_AMOUNT>$invoice->totalVat</VAT_AMOUNT>
        <VAT_BASE>$invoice->total</VAT_BASE>
        <BILLED>1</BILLED>
        <TOTAL_NET>$invoice->total</TOTAL_NET>
        <DATA_REFERENCE>9316</DATA_REFERENCE>
        <DIST_ORD_REFERENCE>0</DIST_ORD_REFERENCE>
        <CAMPAIGN_INFOS>
          <CAMPAIGN_INFO>
          </CAMPAIGN_INFO>
        </CAMPAIGN_INFOS>
        <EDT_CURR>160</EDT_CURR>
        <EDT_PRICE>$invoice->total</EDT_PRICE>
        <ORGLOGOID></ORGLOGOID>
        <GENIUSFLDSLIST>
        </GENIUSFLDSLIST>
        <DEFNFLDSLIST>
        </DEFNFLDSLIST>
        <MONTH>$invoice->month</MONTH>
        <YEAR>$invoice->year</YEAR>
        <PREACCLINES>
        </PREACCLINES>
        <UNIT_GLOBAL_CODE>NIU</UNIT_GLOBAL_CODE>
        <EDTCURR_GLOBAL_CODE>TL</EDTCURR_GLOBAL_CODE>
        <GUID>75D76F7C-4575-4CE7-B881-77D01A2A7C09</GUID>
        <MASTER_DEF>KOMİSYON BEDELİ</MASTER_DEF>
        <FOREIGN_TRADE_TYPE>1</FOREIGN_TRADE_TYPE>
        <DISTRIBUTION_TYPE_WHS>0</DISTRIBUTION_TYPE_WHS>
        <DISTRIBUTION_TYPE_FNO>0</DISTRIBUTION_TYPE_FNO>
      </TRANSACTION>
    </TRANSACTIONS>
    <PAYMENT_LIST>
      <PAYMENT>
        <DATE>$invoice->date</DATE>
        <MODULENR>4</MODULENR>
        <TRCODE>9</TRCODE>
        <TOTAL>$invoice->totalNet</TOTAL>
        <PROCDATE>$invoice->date</PROCDATE>
        <REPORTRATE>1</REPORTRATE>
        <DATA_REFERENCE>0</DATA_REFERENCE>
        <DISCOUNT_DUEDATE>$invoice->date</DISCOUNT_DUEDATE>
        <PAY_NO>1</PAY_NO>
        <DISCTRLIST>
        </DISCTRLIST>
        <DISCTRDELLIST>0</DISCTRDELLIST>
      </PAYMENT>
    </PAYMENT_LIST>
    <ORGLOGOID></ORGLOGOID>
    <DEFNFLDSLIST>
    </DEFNFLDSLIST>
    <DEDUCTIONPART1>2</DEDUCTIONPART1>
    <DEDUCTIONPART2>3</DEDUCTIONPART2>
    <DATA_LINK_REFERENCE>8027</DATA_LINK_REFERENCE>
    <INTEL_LIST>
      <INTEL>
      </INTEL>
    </INTEL_LIST>
    <AFFECT_RISK>0</AFFECT_RISK>
    <PREACCLINES>
    </PREACCLINES>
    <DOC_DATE>$invoice->date</DOC_DATE>
    <PROFILE_ID>2</PROFILE_ID>
    <GUID>B90DA79D-0849-4F08-A82D-DFE339C3E04C</GUID>
    <EDURATION_TYPE>0</EDURATION_TYPE>
    <EDTCURR_GLOBAL_CODE>TL</EDTCURR_GLOBAL_CODE>
    <TOTAL_NET_STR>$invoice->totalNetInText</TOTAL_NET_STR>
    <TOTAL_SERVICES>$invoice->total</TOTAL_SERVICES>
    <EXIMVAT>0</EXIMVAT>
    <EARCHIVEDETR_INTPAYMENTTYPE>0</EARCHIVEDETR_INTPAYMENTTYPE>
  </INVOICE>
</SALES_INVOICES>");


        Storage::put('logoDeneme2018-5.xml', $xml->asXML());

    }

}
