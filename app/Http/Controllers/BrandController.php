<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::getBrands();
        return view('brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'brandName' => 'required|unique:brands,brandName'
            ],
            [
                'brandName.required' => 'Marka adı boş bırakılamaz.',
                'brandName.unique' => 'Girmiş olduğunuz marka adı sistemde kayıtlı.'
            ]
        );

        $brandName = $request->input('brandName');
        Brand::newBrand($brandName);
        return redirect()->back()->with('message', 'Marka başarıyla eklendi !');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::getBrand($id);
        return view('brand.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::getBrand($id);
        return view('brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $brandId)
    {
        $this->validate($request,
            [
                'brandName' => 'required|unique:brands,brandName'
            ],
            [
                'brandName.required' => 'Marka adı boş bırakılamaz.',
                'brandName.unique' => 'Girmiş olduğunuz marka adı sistemde kayıtlı.'
            ]
        );

        $brandName = $request->input('brandName');
        Brand::updateBrand($brandId, $brandName);

        return redirect()->back()->with('message', 'Marka başarıyla güncellendi !');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
