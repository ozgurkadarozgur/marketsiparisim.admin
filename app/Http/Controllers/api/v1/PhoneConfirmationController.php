<?php

namespace App\Http\Controllers\api\v1;

use App\CustomerUser;
use App\PhoneConfirmation;
use App\PhoneConfirmationStatus;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PhoneConfirmationController extends Controller
{
    public function verify(Request $request)
    {
        $customerId = $request->user()->id;
        //$code = $request->input('code');
        $codeForVerify = PhoneConfirmation::getCustomerCodeForVerify($customerId);

        $validator = Validator::make($request->all(),
            [
                'code' => "required|match_phone_code:$codeForVerify|not_expired_pc:$customerId"
            ],
            [
                'code.required' => 'Kod boş bırakılamaz.',
                'code.match_phone_code' => 'Hatalı kod girdiniz.',
                'code.not_expired_pc' => 'Girdiğiniz kodun süresi dolmuştur.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            //$customerId = CustomerUser::getCustomerByPhone($phone)->id;
            PhoneConfirmationStatus::verify($customerId);
            return response()->json(['Status' => 'Success', 'Data' => ''], 200);
        }

    }

    public function sendSMS(Request $request)
    {
        $customerId = $request->user()->id;
        $customer = CustomerUser::getCustomerUser($customerId);
        $code = rand(1000, 9999);
        PhoneConfirmation::sendSMS($customerId, $customer->phone, $code);
        PhoneConfirmation::newAttempt($customerId, $code);
        return response()->json(['Status' => 'Success', 'Data' => '']);
    }

    public function getTimeToVerify(Request $request)
    {
        $customerId = $request->user()->id;
        $remain = PhoneConfirmation::getTimeToVerify($customerId);
        $arr = [
            'remain' => $remain
        ];

        $validator = Validator::make($arr,
            [
                'remain' => 'required|min:1'
            ],
            [
                'remain.min' => 'Onaylama kodunun süresi doldu.'
            ]);

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            return response()->json(['Status' => 'Success', 'Data' => $remain]);
        }

    }

}
