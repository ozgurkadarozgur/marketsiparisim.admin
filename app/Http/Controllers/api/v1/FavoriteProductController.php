<?php

namespace App\Http\Controllers\api\v1;

use App\FavoriteProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class FavoriteProductController extends Controller
{

    public function getFavoriteProducts(Request $request)
    {
        $customerId = $request->user()->id;
        $marketId = $request->input('marketId');
        $favoriteProducts = FavoriteProduct::getFavoriteProducts($customerId, $marketId);
        return response()->json(['Status' => 'Success', 'Data' => $favoriteProducts]);
    }

    public function newFavoriteProduct(Request $request)
    {

        $customerId = $request->user()->id;

        $validator = Validator::make($request->all(),
            [
                'stockId' => ['required',
                    Rule::unique('favoriteProducts')->where(function ($query) use ($customerId) {
                        return $query->where('customerId', $customerId)
                            ->whereNull('deleted_at');
                    })
                ]
            ],
            [
                'stockId.required' => 'Bir hata oluştu.',
                'stockId.unique' => 'Aynı ürün eklenemez.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {

            $stockId = $request->input('stockId');
            FavoriteProduct::newFavoriteProduct($customerId, $stockId);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }

    }

    public function destroyFavoriteProduct(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'stockId' => 'required'
            ],
            [
                'stockId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $stockId = $request->input('stockId');
            FavoriteProduct::destroyFavoriteProduct($customerId, $stockId);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }


    }
}
