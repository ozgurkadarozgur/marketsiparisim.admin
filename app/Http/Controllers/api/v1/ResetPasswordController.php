<?php

namespace App\Http\Controllers\api\v1;

use App\CustomerAddress;
use App\CustomerUser;
use App\Http\Controllers\MSMail;
use App\MarketUser;
use App\ResetPassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{

    public function index($token)
    {
        return ResetPassword::getMarketUserByToken($token);
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'firstPassword' => 'required|same:secondPassword|min:6',
                'secondPassword' => 'required',
                'token' => 'required|exists:customerForgotPassword,token'
            ],
            [
                'firstPassword.required' => 'Boş bırakılamaz.',
                'firstPassword.same' => 'Şifreler eşleşmedi.',
                'firstPassword.min' => 'Şifre en az 6 karakter olmalıdır.',
                'secondPassword.required' => 'Boş bırakılamaz.',
                'token.required'=>'Bir hata oluştu',
                'token.exists'=>'Tıkladığınız bağlantı geçersiz yada süresi dolmuş olabilir.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {

            $token = $request->input('token');
            $resReqCustomer = ResetPassword::getCustomerUserByToken($token);
            $customer = CustomerUser::getCustomerUserByEmail($resReqCustomer->email);

            if($customer){
                $firstPassword = $request->input('firstPassword');
                $customer->password = bcrypt($firstPassword);
                $customer->save();
            }

            return response()->json(['Status' => 'Success', 'Data' => '']);
        }

    }

    public function newRequest(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email'
            ],
            [
                'email.required' => 'Boş bırakılamaz.',
                'email.email' => 'Geçerli bir email adresi girin.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $email = $request->input('email');
            $customer = CustomerUser::getCustomerUserByEmail($email);
            if ($customer) {
                $token = str_random(36);
                ResetPassword::newRequest($email, $token);
                $url = "https://www.marketsiparisim.com/sifresifirla/$token";
                $headers = "From: info@marketsiparisim.com\r\n";
                mail("ozgurozdemirci@gmail.com", "My subject", 'test', $headers);
                //MSMail::send($email, 'Şifre sıfırlama', 'Şifrenizi sıfırlamak için linke <a href="' . $url . '">tıklayın.</a>');
            }
        }
    }

}
