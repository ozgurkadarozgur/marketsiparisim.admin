<?php

namespace App\Http\Controllers\api\v1;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class My3dController extends Controller
{
    public function index()
    {
        return view('my3d.index');
    }

    public function pay(Request $request)
    {

        $clientId = "100200000";  //Banka tarafindan verilen isyeri numarasi
        $amount = "9.95";         //Islem tutari
        $oid = "";      //Siparis Numarasi

        $okUrl = "http://localhost/sanalpos/3DPayResultPage.php";    //Islem basariliysa d�n�lecek isyeri sayfasi  (3D isleminin ve �deme isleminin sonucu)
        $failUrl = "http://localhost/sanalpos/3DPayResultPage.php";  //Islem basarizsa d�n�lecek isyeri sayfasi  (3D isleminin ve �deme isleminin sonucu)

        $rnd = microtime();    //Tarih veya her seferinde degisen bir deger g�venlik ama�li
        $taksit = "0";         //taksit sayisi
        $islemtipi="Auth";     //Islem tipi
        $storekey = "123456";  //isyeri anahtari

// hash hesabinda taksit ve islemtipi de kullanilir.

        $hashstr = $clientId . $oid . $amount . $okUrl . $failUrl .$islemtipi. $taksit  .$rnd . $storekey;


        $hash = base64_encode(pack('H*',sha1($hashstr)));

        $client = new Client();
        $client->post('https://entegrasyon.asseco-see.com.tr/fim/est3Dgate',
            [
                'pan'=>'',
                'cv2'=>'',
                'Ecom_Payment_Card_ExpDate_Year'=>'',
                'Ecom_Payment_Card_ExpDate_Month'=>'',
                'cardType'=>'',
                'amount'=>'',
                'oid'=>'',
                'okUrl'=>'',
                'failUrl'=>'',
                'rnd'=>'',
                'hash'=>'',
                'islemtipi'=>'',
                'taksit'=>'',
                'storetype'=>'',
                'lang'=>'tr',
                ''
            ]
        );

    }

}
