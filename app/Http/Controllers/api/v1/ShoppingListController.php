<?php

namespace App\Http\Controllers\api\v1;

use App\ShoppingList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ShoppingListController extends Controller
{

    public function getShoppingList(Request $request)
    {
        $customerId = $request->user()->id;
        $shoppingList = ShoppingList::getShoppingList($customerId);
        return response()->json(['Status' => 'Success', 'Data' => $shoppingList]);
    }

    public function newItem(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'item' => 'required'
            ],
            [
                'item.required' => 'Item boş bırakılamaz.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $item = $request->input('item');
            ShoppingList::newItem($customerId, $item);
            $shoppingList = ShoppingList::getShoppingList($customerId);
            return response()->json(['Status' => 'Success', 'Data' => $shoppingList]);
        }

    }

    public function destroyItem(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'itemId' => 'required'
            ],
            [
                'itemId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $itemId = $request->input('itemId');
            ShoppingList::destroyItem($itemId);
            $shoppingList = ShoppingList::getShoppingList($customerId);
            return response()->json(['Status' => 'Success', 'Data' => $shoppingList]);
        }
    }

    public function editItem(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'itemId' => 'required',
                'item' => 'required'
            ],
            [
                'itemId.required' => 'Bir hata oluştu.',
                'item.required' => 'Item boş bırakılamaz.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $itemId = $request->input('itemId');
            $newItem = $request->input('item');
            ShoppingList::editItem($itemId, $newItem);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }


    }

}
