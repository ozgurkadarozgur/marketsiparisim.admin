<?php

namespace App\Http\Controllers\api\v1;

use App\Market;
use App\Product;
use App\ShoppingCart;
use App\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ShoppingCartController extends Controller
{

    public function postShoppingCarts(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'shoppingCarts' => 'required',
                'marketId' => 'required'
            ],
            [
                'shoppingCarts.required' => 'Bir hata oluştu.',
                'marketId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $marketId = $request->input('marketId');
            $shoppingCarts = $request->input('shoppingCarts');
            $shoppingCarts = json_decode($shoppingCarts);

            $productEnoughErrArr = [];
            foreach ($shoppingCarts as $shoppingCart) {
                $totalInStock = Stock::getTotalCountInStock($shoppingCart->id);
                $totalInShoppingCart = Stock::getTotalCountInShoppingCart($customerId, $shoppingCart->id);
                $totalInStock = $totalInStock - $totalInShoppingCart;
                if ($totalInStock >= $shoppingCart->quantity) {
                    for ($i = 1; $i <= $shoppingCart->quantity; $i++) {
                        ShoppingCart::newShoppingCart($customerId, $shoppingCart->id);
                    }
                } else {
                    for ($i = 1; $i <= $totalInStock; $i++) {
                        ShoppingCart::newShoppingCart($customerId, $shoppingCart->id);
                    }
                    $prd = Stock::getProduct($shoppingCart->id);
                    $obj = (object)[
                        'stockId' => [$shoppingCart->id],
                        'productName' => [$prd->product->productName],
                        'count' => [$totalInStock]
                    ];
                    array_push($productEnoughErrArr, $obj);
                }

            }

            $shoppingCarts = ShoppingCart::getShoppingCarts($customerId, $marketId);
            $shoppingAmount = ShoppingCart::getShoppingAmount($customerId, $marketId);

            if (count($productEnoughErrArr) > 0) {
                return response()->json(['Status' => 'Warning', 'Data' => (object)[
                    'cartWarnings' => $productEnoughErrArr,
                    'shoppingCarts' => $shoppingCarts,
                    'amount' => $shoppingAmount
                ]]);
            } else {

                return response()->json(['Status' => 'Success', 'Data' => (object)[
                    'shoppingCarts' => $shoppingCarts,
                    'amount' => $shoppingAmount
                ]
                ]);
            }
        }
    }

    public function postShoppingCartsForAlternatives(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'shoppingCarts' => 'required',
                'marketId' => 'required'
            ],
            [
                'shoppingCarts.required' => 'Bir hata oluştu.',
                'marketId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $marketId = $request->input('marketId');
            $shoppingCarts = $request->input('shoppingCarts');
            $shoppingCarts = json_decode($shoppingCarts);
            $arr = array();
            $productEnoughErrArr = [];
            foreach ($shoppingCarts as $shoppingCart) {
                array_push($arr, "$shoppingCart->id");
                if(ShoppingCart::getShoppingCart($customerId, $marketId, $shoppingCart->id)){
                    ShoppingCart::destroyShoppingCartsByStockId($customerId, $shoppingCart->id);
                }
                $totalInStock = Stock::getTotalCountInStock($shoppingCart->id);
                $totalInShoppingCart = Stock::getTotalCountInShoppingCart($customerId, $shoppingCart->id);
                $totalInStock = $totalInStock - $totalInShoppingCart;
                if ($totalInStock >= $shoppingCart->quantity) {
                    for ($i = 1; $i <= $shoppingCart->quantity; $i++) {
                        ShoppingCart::newShoppingCart($customerId, $shoppingCart->id);
                    }
                } else {
                    for ($i = 1; $i <= $totalInStock; $i++) {
                        ShoppingCart::newShoppingCart($customerId, $shoppingCart->id);
                    }
                    $prd = Stock::getProduct($shoppingCart->id);
                    $obj = (object)[
                        'stockId' => [$shoppingCart->id],
                        'productName' => [$prd->product->productName],
                        'count' => [$totalInStock]
                    ];
                    array_push($productEnoughErrArr, $obj);
                }

            }

            $shoppingCarts = ShoppingCart::getShoppingCartsForAlternatives($customerId, $marketId, $arr);
            $shoppingAmount = ShoppingCart::getShoppingAmount($customerId, $marketId);

            if (count($productEnoughErrArr) > 0) {
                return response()->json(['Status' => 'Warning', 'Data' => (object)[
                    'cartWarnings' => $productEnoughErrArr,
                    'shoppingCarts' => $shoppingCarts,
                    'amount' => $shoppingAmount
                ]]);
            } else {

                return response()->json(['Status' => 'Success', 'Data' => (object)[
                    'shoppingCarts' => $shoppingCarts,
                    'amount' => $shoppingAmount
                ]
                ]);
            }


        }
    }

    public function getShoppingCarts(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'marketId' => 'required|exists:markets,id'
            ],
            [
                'marketId.required' => 'Bir hata oluştu:required',
                'marketId.exists' => 'Bir hata olustu:exists'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $marketId = $request->input('marketId');
            $shoppingCarts = ShoppingCart::getShoppingCarts($customerId, $marketId);
            $shoppingAmount = ShoppingCart::getShoppingAmount($customerId, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => (object)[
                'shoppingCarts' => $shoppingCarts,
                'amount' => $shoppingAmount
            ]
            ]);
        }
    }

    public function newShoppingCart(Request $request)
    {
        $customerId = $request->user()->id;
        $stockId = $request->input('stockId');
        $marketId = Stock::getMarketIdByStockId($stockId);
        $validator = Validator::make($request->all(),
            [
                'stockId' => "required|exists:stocks,id|product_in_stock:$customerId"
            ],
            [
                'stockId.required' => 'Bir hata oluştu:required.',
                'stockId.exists' => 'Bir hata oluştu:exists.',
                'stockId.product_in_stock' => 'Seçtiğiniz ürün stoklarda tükenmiştir.'
            ]);

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            ShoppingCart::newShoppingCart($customerId, $stockId);
            $shoppingCarts = ShoppingCart::getShoppingCarts($customerId, $marketId);
            $shoppingAmount = ShoppingCart::getShoppingAmount($customerId, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => (object)[
                'shoppingCarts' => $shoppingCarts,
                'amount' => $shoppingAmount
            ]
            ]);
        }
    }

    public function getAlternatives(Request $request)
    {
        $products = $request->input('products');
        $longitude = $request->input('longitude');
        $latitude = $request->input('latitude');
        $marketId = $request->input('marketId');
        $markets = DB::select("select markets.id, markets.marketName, markets.address, markets.minAmount,
            st_distance_sphere(POINT('$longitude', '$latitude'), POINT(markets.longitude, markets.latitude)) as distance,
            longitude, latitude
            from market.markets 
            where id != $marketId and
             st_distance_sphere(POINT('$longitude', '$latitude'), POINT(markets.longitude, markets.latitude)) <= markets.maxDistance;");

        $products = json_decode($products);

        $productArrStr = '';
        foreach ($products as $product) {
            $prd = Stock::getProduct($product->id);
            $productArrStr .= $prd->productId . ',';
        }

        $productArrStr = substr($productArrStr, 0, strlen($productArrStr) - 1);

        $arr = array();
        foreach ($markets as $market) {
            $mrkt = Market::getMarket($market->id);
            $obj = (object)[];

            $prds = array();
            $firstCount = 0;
            $totalCount = 0;
            $totalPrice = 0;
            foreach ($products as $product){

                $prd = Stock::getProduct($product->id);
                $p = DB::select("select stocks.id, products.productName, IF(stocks.total >= $product->quantity, $product->quantity, stocks.total) as quantity,
                                stocks.price, products.photo
                                from stocks
                                inner join markets on markets.id = stocks.marketId
                                inner join products on products.id = stocks.productId
                                where stocks.marketId = '$market->id' and stocks.deleted_at is null
                                and stocks.productId = $prd->productId and stocks.total > 0
                                 ");
                $firstCount += $product->quantity;
                if(count($p) > 0){
                    array_push($prds, $p[0]);
                    $totalCount += $p[0]->quantity;
                    $totalPrice += $p[0]->price * $product->quantity;
                }


            }

            /*
            $prds = DB::select("select stocks.id, products.productName, stocks.total as count,
                                stocks.price, products.photo
                                from stocks
                                inner join markets on markets.id = stocks.marketId
                                inner join products on products.id = stocks.productId
                                where stocks.marketId = '$market->id'
                                and stocks.productId in ($productArrStr) and stocks.total > 0
                                 ");
            */
            if (count($prds) > 0) {
                /*$totalPrice = DB::select("select sum(stocks.price) as totalPrice from stocks
                        inner join products on products.id = stocks.productId
                        where products.id in ($productArrStr) and stocks.marketId = $market->id");*/
                $obj->id = $mrkt->id;
                $obj->longitude = $mrkt->longitude;
                $obj->latitude = $mrkt->latitude;
                $obj->marketName = $mrkt->marketName;
                $obj->products = $prds;
                $obj->count = $totalCount;
                $obj->totalCount = $firstCount;
                $obj->totalPrice = $totalPrice;
                array_push($arr, $obj);
            }
        }

        return response()->json(['Status' => 'Success', 'Data' => $arr]);
    }

    public function destroyShoppingCart(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'stockId' => 'required'
            ],
            [
                'stockId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $stockId = $request->input('stockId');
            ShoppingCart::destroyShoppingCart($customerId, $stockId);
            $marketId = Stock::getMarketIdByStockId($stockId);
            $shoppingCarts = ShoppingCart::getShoppingCarts($customerId, $marketId);
            $shoppingAmount = ShoppingCart::getShoppingAmount($customerId, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => (object)[
                'shoppingCarts' => $shoppingCarts,
                'amount' => $shoppingAmount
            ]
            ]);
        }

    }

    public function destroyShoppingCarts(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'marketId' => 'required'
            ],
            [
                'marketId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $marketId = $request->input('marketId');
            ShoppingCart::destroyShoppingCarts($customerId, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }


    }

    public function destroyShoppingCartItem(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'stockId' => 'required'
            ],
            [
                'stockId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $stockId = $request->input('stockId');
            $marketId = Stock::getMarketIdByStockId($stockId);
            ShoppingCart::destroyShoppingCartsByStockId($customerId, $stockId);
            $shoppingCarts = ShoppingCart::getShoppingCarts($customerId, $marketId);
            $shoppingAmount = ShoppingCart::getShoppingAmount($customerId, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => (object)[
                'shoppingCarts' => $shoppingCarts,
                'amount' => $shoppingAmount
            ]]);
        }

    }

    public function getShoppingAmount(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'marketId' => 'required'
            ],
            [
                'marketId.required' => 'Market seçilmedi.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $marketId = $request->input('marketId');
            $shoppingAmount = ShoppingCart::getShoppingAmount($customerId, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => $shoppingAmount]);
        }
    }

}
