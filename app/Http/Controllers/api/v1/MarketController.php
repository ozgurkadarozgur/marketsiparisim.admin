<?php

namespace App\Http\Controllers\api\v1;

use App\Market;
use App\Product;
use App\Stock;
use App\TopCategory;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MarketController extends Controller
{
    public function find(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'latitude' => 'required',
                'longitude' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $latitude = $request->input('latitude');
            $longitude = $request->input('longitude');
            //$city = $request->input('city');

            $markets = DB::select("select markets.id, markets.marketName, markets.address, markets.minAmount,
            st_distance_sphere(POINT('$longitude', '$latitude'), POINT(markets.longitude, markets.latitude)) as distance,
            longitude, latitude
            from market.markets 
            where st_distance_sphere(POINT('$longitude', '$latitude'), POINT(markets.longitude, markets.latitude)) <= markets.maxDistance;");
            //return $markets;

            if (count($markets) == 0) return response()->json(
                [
                    'Status' => 'Warning',
                    'Data' => [
                        'Message' => 'Yakınınızda market bulunamadı.',
                        'TopCategories' => TopCategory::getAll()
                    ]
                ]);
            else return response()->json(['Status' => 'Success', 'Data' => $markets]);
        }

    }

    public function topCategories($marketId)
    {
        $topCategories = Stock::getStockTopCategories($marketId);
        return response()->json(['Status' => 'Success', 'Data' => $topCategories]);
    }

    public function subCategories($marketId, $topCategoryId)
    {
        $subCategories = Stock::getStockSubCategories($marketId, $topCategoryId);
        return response()->json(['Status' => 'Success', 'Data' => $subCategories]);
    }

    public function products($marketId, $subCategoryId, $limit = 10)
    {
        $products = Stock::getStockProducts($marketId, $subCategoryId, $limit);
        return response()->json(['Status' => 'Success', 'Data' => $products]);
    }

    public function product(Request $request)
    {
        $marketId = $request->input('marketId');
        $stockId = $request->input('stockId');
        $product = Stock::getProductDetail($marketId, $stockId);
        return response()->json(['Status' => 'Success', 'Data' => $product]);
    }

    public function specialProducts($marketId)
    {
        $products = Stock::getStockSpecialProducts($marketId);
        return response()->json(['Status' => 'Success', 'Data' => $products]);
    }

}
