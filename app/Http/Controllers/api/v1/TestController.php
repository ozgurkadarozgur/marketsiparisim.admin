<?php

namespace App\Http\Controllers\api\v1;

use App\CustomerUser;
use App\ShoppingCart;
use App\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TestController extends Controller
{
    public function newShoppingCart(Request $request)
    {
        $stockId = $request->input('stockId');
        $customerId = 1;
        $validator = Validator::make($request->all(),
            [
                'stockId' => "product_in_stock:$customerId"
            ],
            [
                'stockId.product_in_stock' => 'Seçtiğiniz ürün stoklarda tükenmiştir.'
            ]);

        if ($validator->fails()) {
            return response()->json(['Error' => $validator->errors()]);
        } else {
            /*
            $orderedCount = Stock::getOrderedCount($stockId);
            $totalCount = Stock::getTotalCountInStock($stockId);
            $remaining = $totalCount - $orderedCount;
            $totalCountInShoppingCart = Stock::getTotalCountInShoppingCart(1, $stockId);
            if ($remaining - $totalCountInShoppingCart > 0) echo 'eklenebilir';
            */
            return response()->json([
                'result' => 1
            ]);
        }
    }

    public function amount(Request $request)
    {
        $customerId = $request->input('customerId');
        $marketId = $request->input('marketId');
        return ShoppingCart::getShoppingAmount($customerId, $marketId);
    }


    public function testPass()
    {
        $customerId = 47;
        $phoneConfirmationStatus = CustomerUser::getCustomerUser($customerId)->phoneConfirmationStatus;

        if ($phoneConfirmationStatus == 2) {
            return response()->json(['Status' => 'Error', 'Data' => 'Hatali']);
        } else {
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }
    }

    public function prd()
    {
        $stockId = 'e80228a0-b256-11e7-8ad5-208984fd02a1';
        return Stock::getProduct($stockId)->product->productName;
    }

}
