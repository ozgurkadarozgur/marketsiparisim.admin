<?php

namespace App\Http\Controllers\api\v1;

use App\CustomerAddress;
use App\CustomerUser;
use App\Market;
use App\MarketMoney;
use App\PhoneConfirmation;
use App\PhoneUpdateRequest;
use App\Reference;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{

    public function test(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'ad' => 'required',
                'soyad' => 'required'
            ],
            [
                'ad.required' => 'Boş bırakılamaz',
                'soyad.required' => 'soyad bos birakilamaz'
            ]);

        if ($validator->fails()) {
            return response()->json(['Error' => $validator->errors()]);
        } else
            return response()->json(['sonuc' => 1]);

    }

    public function getMarketMoney(Request $request)
    {
        $customerId = $request->user()->id;
        $mm = DB::select("select getCustomerMarketMoney($customerId) as marketMoney");

        $ref = CustomerUser::hasReference($customerId);

        if ($ref) {
            $refInfo = CustomerUser::getCustomerUser($ref);
            return response()->json(['Status' => 'Success', 'Data' => (object)[
                    'reference' => $refInfo->email,
                    'marketMoney' => $mm[0]->marketMoney
                ]
                ]
            );
        } else {
            return response()->json(['Status' => 'Success', 'Data' => (object)[
                    'reference' => '',
                    'marketMoney' => $mm[0]->marketMoney
                ]
                ]
            );
        }

    }

    public function destroyReference(Request $request)
    {
        $customerId = $request->user()->id;
        Reference::destroyReference($customerId);
        return response()->json(['Status' => 'Success', 'Data' => '']);
    }

    public function changeName(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required|min:6'
            ],
            [
                'name.required' => 'Boş bırakılamaz.',
                'name.min' => 'En az 6 karakter olmalıdır.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $name = $request->input('name');
            CustomerUser::updateName($customerId, $name);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }

    }

    public function requestForUpdatePhone(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone' => 'required|unique:customerUsers,phone|numeric'
            ],
            [
                'phone.required' => 'Boş bırakılamaz.',
                'phone.unique' => 'Girdiğiniz telefon numarası kullanılmaktadır.',
                'phone.numeric'=>'Geçerli bir telefon numarası girin.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $newPhone = $request->input('phone');
            $code = rand(1000, 9999);
            PhoneUpdateRequest::newRequest($customerId, $newPhone, $code);
            PhoneConfirmation::sendSMS($customerId, $newPhone, $code);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }

    }

    public function reSendSMSForUpdatePhone(Request $request)
    {
        $customerId = $request->user()->id;
        $codeData = PhoneUpdateRequest::getCodeData($customerId);
        $newPhone = $codeData->newPhone;
        $code = rand(1000, 9999);
        PhoneUpdateRequest::newRequest($customerId, $newPhone, $code);
        PhoneConfirmation::sendSMS($customerId, $newPhone, $code);
        return response()->json(['Status' => 'Success', 'Data' => '']);
    }

    public function completeUpdatePhone(Request $request)
    {
        $customerId = $request->user()->id;
        $codeForVerify = PhoneUpdateRequest::getCodeForVerify($customerId);
        $validator = Validator::make($request->all(),
            [
                'code' => "required|match_phone_code:$codeForVerify|not_expired_upc:$customerId"
            ],
            [
                'code.required' => 'Bir hata oluştu.',
                'code.match_phone_code' => 'Hatalı kod girdiniz.',
                'code.not_expired_upc' => 'Girdiğiniz kodun süresi dolmuştur.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $codeData = PhoneUpdateRequest::getCodeData($customerId);
            $newPhone = $codeData->newPhone;
            CustomerUser::updatePhone($customerId, $newPhone);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }

    }

    public function getTimeToPhoneUpdateVerify(Request $request)
    {
        $customerId = $request->user()->id;
        $remain = PhoneUpdateRequest::getTimeToVerify($customerId);
        $arr = [
            'remain' => $remain
        ];

        $validator = Validator::make($arr,
            [
                'remain' => 'required|min:1'
            ],
            [
                'remain.min' => 'Onaylama kodunun süresi doldu.'
            ]);

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            return response()->json(['Status' => 'Success', 'Data' => $remain]);
        }

    }

    public function changePassword(Request $request)
    {
        $customerId = $request->user()->id;

        $newPassword = $request->input('newPassword');

        $validator = Validator::make($request->all(),
            [
                'password' => "required|match_with_old_password:$customerId",
                'newPassword' => 'required|min:6|'
            ],
            [
                'password.required' => 'Eski şifre girilmedi.',
                'password.match_with_old_password' => 'Eski şifre eşleşmedi.',
                'newPassword.required' => 'Yeni şifre girilmedi.',
                'newPassword.min' => 'Şifre en az 6 karakter olmalıdır.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            CustomerUser::updatePassword($customerId, $newPassword);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }


    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|unique:customerUsers,email|email',
                'password' => 'required|min:6|max:12',
                'phone' => 'required|unique:customerUsers,phone|numeric'
            ],
            [
                'name.required' => 'Ad boş bırakılamaz.',
                'email.required' => 'Email boş bırakılamaz.',
                'email.unique' => 'Girdiğiniz mail adresi başkası tarafından kullanılmaktadır.',
                'email.email' => 'Geçerli bir mail adresi girin.',
                'password.required' => 'Şifre boş bırakılamaz.',
                'password.min' => 'Şifre en az 6 karakter olmalıdır.',
                'password.max' => 'Şifre en fazla 12 karakter olmalıdır.',
                'phone.required' => 'Telefon boş bırakılamaz.',
                'phone.unique' => 'Girdiğiniz telefon numarası başkası tarafından kullanılmaktadır.',
                'phone.numeric'=>'Geçerli bir telefon numarası girin.'
            ]);

        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Error',
                'Data' => $validator->errors()
            ]);
        } else {
            $name = $request->input('name');
            $email = $request->input('email');
            $password = $request->input('password');
            $phone = $request->input('phone');
            $referenceEmail = $request->input('referenceEMail');
            $code = rand(1000, 9999);
            if ($referenceEmail) {

                $referenceValidator = Validator::make($request->all(),
                    [
                        'referenceEMail' => 'exists:customerUsers,email'
                    ],
                    [
                        'referenceEMail.exists' => 'Referans bulunamadı.'
                    ]);

                if ($referenceValidator->fails()) {
                    return response()->json(['Status' => 'Error', 'Data' => $referenceValidator->errors()]);
                } else {
                    $customerId = CustomerUser::register($name, $email, $password, $phone);
                    PhoneConfirmation::sendSMS($customerId, $phone, $code);
                    PhoneConfirmation::newAttempt($customerId, $code);
                    $referenceUser = CustomerUser::getCustomerUserByEmail($referenceEmail);
                    Reference::newReference($customerId, $referenceUser->id);
                    return response()->json(['Status' => 'Success', 'Data' => '']);
                }
            } else {
                $customerId = CustomerUser::register($name, $email, $password, $phone);
                PhoneConfirmation::sendSMS($customerId, $phone, $code);
                PhoneConfirmation::newAttempt($customerId, $code);
                return response()->json(['Status' => 'Success', 'Data' => '']);
            }


        }

    }

    public function getUserInfo(Request $request)
    {
        $customerId = $request->user()->id;
        $info = CustomerUser::getCustomerInfo($customerId);
        return response()->json(['Status' => 'Success', 'Data' => $info]);
    }

    public function newAddress(Request $request)
    {
        $customerId = $request->user()->id;

        $validator = Validator::make($request->all(),
            [
                'addressTitle' => [
                    'required',
                    Rule::unique('customerAddresses')->where(function ($query) use ($customerId) {
                        return $query->where('customerId', $customerId)
                            ->whereNull('deleted_at');
                    })
                ],
                'address' => 'required',
                'addressDescription' => 'required',
                'longitude' => 'required',
                'latitude' => 'required'
            ],
            [
                'addressTitle.required' => 'Adres başlığı boş bırakılamaz.',
                'addressTitle.unique' => 'Adres başlığı başka bir adrese ait.',
                'address.required' => 'Adres boş bırakılamaz.',
                'addressDescription.required' => 'Adres açıklaması boş bırakılamaz.',
                'longitude.required' => 'Bir hata oluştu.',
                'latitude.required' => 'Bir hata oluştu.',
            ]
        );


        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $addressTitle = $request->input('addressTitle');
            $address = $request->input('address');
            $addressDescription = $request->input('addressDescription');
            $longitude = $request->input('longitude');
            $latitude = $request->input('latitude');
            CustomerAddress::newAddress($customerId, $addressTitle, $address, $addressDescription, $longitude, $latitude);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }
    }

    public function getAddresses(Request $request)
    {
        $customerId = $request->user()->id;
        $addresses = CustomerAddress::getAddresses($customerId);
        return response()->json(['Status' => 'Success', 'Data' => $addresses]);
    }

    public function destroyAddress(Request $request)
    {
        $customerId = $request->user()->id;
        $validator = Validator::make($request->all(),
            [
                'addressId' => 'required'
            ],
            [
                'addressId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $addressId = $request->input('addressId');
            CustomerAddress::destroyAddress($addressId);
            $addresses = CustomerAddress::getAddresses($customerId);
            return response()->json(['Status' => 'Success', 'Data' => $addresses]);
        }


    }

    public function changeNotificationStatus(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'status' => 'required'
            ],
            [
                'status.required' => 'Bildirim durumu gönderilmedi'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $status = $request->input('status');
            CustomerUser::changeNotificationStatus($customerId, $status);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }

    }

    public function getAddressForOrder(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'marketId' => 'required'
            ],
            [
                'marketId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $marketId = $request->input('marketId');
            $market = Market::getMarket($marketId);
            $longitude = $market->longitude;
            $latitude = $market->latitude;
            $maxDistance = $market->maxDistance;
            $addresses = CustomerAddress::getAddressForOrder($customerId, $longitude, $latitude, $maxDistance);
            return response()->json(['Status' => 'Success', 'Data' => $addresses]);

        }

    }

}
