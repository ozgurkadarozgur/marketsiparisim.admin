<?php

namespace App\Http\Controllers\api\v1;

use App\CustomerUser;
use App\Events\NewOrderEvent;
use App\Market;
use App\MarketMoney;
use App\Order;
use App\OrderDetail;
use App\PhoneConfirmationStatus;
use App\ShoppingCart;
use App\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function order(Request $request)
    {

        $customerId = $request->user()->id;
        $customerUser = CustomerUser::getCustomerUser($customerId);
        $status = $customerUser->phoneConfirmationStatus;
        if ($status == PhoneConfirmationStatus::$unVerified) {
            return response()->json(['Status' => 'Error', 'Data' => ['phone_confirmed' => [$status]]]);
        } else {

            $validator = Validator::make($request->all(),
                [
                    'marketId' => 'required',
                    'products' => 'required',
                    'paymentMethod' => 'required',
                    'addressId' => 'required'
                ],
                [
                    'marketId.required' => 'Bir hata oluştu:marketId',
                    'products.required' => 'Bir hata oluştu:products',
                    'paymentMethod.required' => 'Bir hata oluştu:paymentMethod',
                    'addressId.required' => 'Bir hata oluştu:address',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
            } else {
                //$customerName = $request->user()->name;
                $marketId = $request->input('marketId');
                $products = $request->input('products');
                $paymentMethodId = intval($request->input('paymentMethod'));
                $addressId = $request->input('addressId');
                $note = $request->input('note');
                //$orderId = str_random(25);

                $products = json_decode($products);

                $orderWarnArr = [];
                foreach ($products as $product) {
                    $totalCount = Stock::getTotalCountInStock($product->id);
                    if ($totalCount < $product->quantity) {
                        $max = $product->quantity - $totalCount;
                        for ($i = 0; $i < $max; $i++) {
                            ShoppingCart::destroyShoppingCart($customerId, $product->id);
                        }
                        $prd = Stock::getProduct($product->id);
                        $obj = (object)[
                            'stockId' => [$product->id],
                            'productName' => [$prd->product->productName],
                            'count' => [$totalCount]
                        ];
                        array_push($orderWarnArr, $obj);
                    }
                }

                if (count($orderWarnArr) > 0) {
                    return response()->json(['Status' => 'Warning', 'Data' => ['cartWarnings' => $orderWarnArr]]);
                } else {
                    $orderId = Order::newOrder($customerId, $marketId, $paymentMethodId, $addressId, $note);
                    foreach ($products as $product) {
                        $prd = Stock::getProduct($product->id);
                        OrderDetail::setOrderDetail($orderId, $product->id, $product->quantity, $prd->price);
                        Stock::decreaseStock($product->id, $product->quantity);
                        ShoppingCart::destroyShoppingCartsByStockId($customerId, $product->id);
                    }
                    $orderDetails = Order::getOrderDetailsFromAPI($orderId);
                    event(new NewOrderEvent($orderDetails));
                    /*
                    $totalBill = OrderDetail::getOrderTotalBill($orderId);

                    MarketMoney::newMarketMoney($customerId, $orderId, $totalBill);
                    if ($ref = CustomerUser::hasReference($customerId)) {
                        MarketMoney::newMarketMoney($ref, $orderId, $totalBill);
                    }
                    */
                    return response()->json(['Status' => 'Success', 'Data' => ['info' => OrderDetail::getOrderInfo($orderId)]]);
                }

            }

        }


    }

    public function getPastOrders(Request $request)
    {
        $customerId = $request->user()->id;
        $latestOrders = Order::getPastOrders($customerId);
        return response()->json(['Status' => 'Success', 'Data' => $latestOrders]);
    }

    public function getOrderDetails(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'orderId' => 'required'
            ],
            [
                'orderId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $orderId = $request->input('orderId');
            $orderDetails = OrderDetail::getOrderDetail($orderId);
            return response()->json(['Status' => 'Success', 'Data' => $orderDetails]);
        }
    }

    public function reOrder(Request $request)
    {
        $customerId = $request->user()->id;
        $orderId = $request->input('orderId');
        $order = Order::getOrder($orderId);
        $marketId = $order->marketId;
        ShoppingCart::destroyShoppingCarts($customerId, $marketId);
        $market = Market::getMarket($marketId);
        $products = Order::getOrderProducts($orderId);

        foreach ($products as $product) {
            for ($i = 0; $i < $product->remain; $i++) {
                ShoppingCart::newShoppingCart($customerId, $product->productId);
            }
        }
        $shoppingCarts = ShoppingCart::getShoppingCarts($customerId, $marketId);
        return response()->json(['Status' => 'Success', 'Data' => (object)[
            'market' => $market,
            'shoppingCarts' => $shoppingCarts
        ]
        ]);
    }

}
