<?php

namespace App\Http\Controllers\api\v1;

use App\NotificationReg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'regId' => 'required'
            ],
            [
                'regId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $customerId = $request->user()->id;
            $regId = $request->input('regId');
            NotificationReg::saveRegId($customerId, $regId);
            return response()->json(['Status' => 'Success', 'Data' => '']);
        }

    }



}
