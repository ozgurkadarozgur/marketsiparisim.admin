<?php

namespace App\Http\Controllers\api\v1;

use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function getProduct($productId)
    {
        return Product::getProduct($productId);
    }

    public function searchByKeyword(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'keyword' => 'required|min:3',
                'marketId' => 'required'
            ],
            [
                'keyword.required' => 'Kelime girin...',
                'keyword.min' => 'Kelime en az 3 karakter olmalıdır.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $keyword = $request->input('keyword');
            $marketId = $request->input('marketId');
            $results = Stock::searchByKeyword($keyword, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => $results]);
        }


    }

    public function searchByBarcode(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'barcode' => 'required',
                'marketId' => 'required'
            ],
            [
                'barcode.required' => 'Barkod girilmedi.',
                'marketId.required' => 'Bir hata oluştu.'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['Status' => 'Error', 'Data' => $validator->errors()]);
        } else {
            $barcode = $request->input('barcode');
            $marketId = $request->input('marketId');
            $results = Stock::searchByBarcode($barcode, $marketId);
            return response()->json(['Status' => 'Success', 'Data' => $results]);
        }


    }

}
