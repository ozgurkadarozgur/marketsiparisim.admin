<?php

namespace App\Http\Controllers\api\v1;

use App\CustomerUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ReferenceController extends Controller
{
    public function check(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'email'=>'required|email|exists:customerUsers,email'
            ],
            [
                'email.required'=> 'Email adresi girilmedi.',
                'email.email'=> 'Geçerli bir email adresi girin.',
                'email.exists'=>'Referans Bulunamadı.'
            ]
        );

        if($validator->fails()){
            return response()->json(['Status'=>'Error', 'Data'=>$validator->errors()]);
        }else{
            return response()->json(['Status'=>'Success', 'Data'=>'']);
        }


    }
}
