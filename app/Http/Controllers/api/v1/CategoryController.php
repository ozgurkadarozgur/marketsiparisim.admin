<?php

namespace App\Http\Controllers\api\v1;

use App\SubCategory;
use App\TopCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function topCategories()
    {
        return TopCategory::all();
    }

    public function subCategories($topCategoryId)
    {
        $topCategory = TopCategory::getCategory($topCategoryId);
        return $topCategory->subCategories;
    }

    public function products($subCategoryId)
    {
        $subCategory = SubCategory::getCategory($subCategoryId);
        return $subCategory->products;
    }

}
