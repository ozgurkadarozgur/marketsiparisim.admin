<?php

namespace App\Http\Controllers\api\v1;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function getCities()
    {
        return City::getCities();
    }
}
