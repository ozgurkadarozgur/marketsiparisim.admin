<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContractController extends Controller
{
    public function getContract($type)
    {
        return response()->view('contracts.'. $type)->header('Content-Type','text/html');
    }
}
