<?php

namespace App\Http\Controllers\api\v1;

use App\MarketRecommendation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketRecommendationController extends Controller
{
    public function newRecommendation(Request $request)
    {
        $customerId = $request->user()->id;
        $marketName = $request->input('marketName');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');

        MarketRecommendation::newRecommendation($customerId, $marketName, $latitude, $longitude);
        return response()->json(['result' => 1]);

    }
}
