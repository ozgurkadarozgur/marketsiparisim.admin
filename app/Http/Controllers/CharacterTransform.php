<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 18.08.2017
 * Time: 19:58
 */

namespace App\Http\Controllers;


class CharacterTransform
{
    public static function transform($value)
    {
        $value = mb_strtolower($value);
        $turkish = array("ı", "ğ", "ü", "ş", "ö", "ç");
        $english   = array("i", "g", "u", "s", "o", "c");

        $finalValue = str_replace($turkish, $english, $value);
        $finalValue = str_replace(' ', '-', $finalValue);
        return $finalValue;
    }
}