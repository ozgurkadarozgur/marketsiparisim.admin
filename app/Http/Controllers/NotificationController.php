<?php

namespace App\Http\Controllers;

use App\Market;
use App\NotificationReg;
use App\Product;
use App\Stock;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function notificationPlaning()
    {
        return view('notification.notificationPlaning');
    }

    public function sendProductNtf(Request $request)
    {
        $marketId = $request->input('marketId');
        $stockId = $request->input('productId');
        $message = $request->input('message');

        $stock = Stock::getProduct($stockId);
        $product = Stock::getProductDetail($marketId, $stockId);

        $data = array(
            'type' => 'product',
            'id' => $stockId,
            'productName' => $product[0]->productName,
            'photo' => json_decode($product[0]->photo),
            'price' => number_format((float)$stock->price, 2, '.', '')

        );

        $marketName = Market::getMarket($marketId)->marketName;
        $tokens = NotificationReg::where('customerId', 1)
            ->orWhere('customerId', 96)
            ->orWhere('customerId', 2)
            ->get()
            ->pluck('regId');

        SysNotification::sendProductNotification($marketName, $message, $tokens, $data);
    }

    public function sendGeneralNtf(Request $request)
    {
        $message = $request->input('message');


        $data = array(
            'type' => 'general',
            'message'=> $message
        );

        $tokens = NotificationReg::where('customerId', 1)
            ->orWhere('customerId', 96)
            ->get()
            ->pluck('regId');
            //
            //->orWhere('customerId', 2)


        SysNotification::sendGeneralNotification($message, $tokens, $data);
    }

}
