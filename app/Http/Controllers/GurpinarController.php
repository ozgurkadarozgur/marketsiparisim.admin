<?php

namespace App\Http\Controllers;

use App\Barcode;
use App\Brand;
use App\GS;
use App\GurpinarStok;
use App\Product;
use App\ProductType;
use App\Stock;
use App\SubCategory;
use App\TopCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\RedisQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GurpinarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function gurpinarStokAktar()
    {
        /*
        $urunler = DB::select("select stocks.productId, products.productName, stocks.total, stocks.unitId, stocks.price from stocks
inner join products on products.id = stocks.productId
where marketId = 6005 and stocks.productId not in
(select stocks.productId from stocks where marketId = 6006)");


        foreach ($urunler as $urun){
            Stock::newStock(6006, $urun->productId, $urun->total, $urun->price, 1);
        }
        */
    }


    public function urunler()
    {
        $butunUrunler = DB::select("SELECT * FROM gurpinar_s LIMIT 1");
        $kayitliUrunler = DB::select("SELECT * FROM stocks
                                INNER JOIN products ON products.id = stocks.productId
                                INNER JOIN barcodes ON barcodes.productId = products.id
                                INNER JOIN gurpinar_s ON gurpinar_s.barcode = barcodes.barcodeCode
                                WHERE stocks.marketId = 6005;");

        $olmayanUrunler = DB::select("SELECT gurpinar_s.productName, gurpinar_s.barcode, gurpinar_s.price, gurpinar_s.count FROM gurpinar_s
                            LEFT JOIN barcodes ON barcodes.barcodeCode = gurpinar_s.barcode
                            WHERE barcodes.barcodeCode IS NULL AND gurpinar_s.id>214");

        //$kayipUrunler = DB::select(DB::raw("select * from gurpinarUrunler where gurpinarUrunler.barcode not in(select barcodeCode from sistemUrunler)"));

        $kayipUrunler = DB::select("SELECT gurpinar_s.barcode, gurpinar_s.price, gurpinar_s.count, products.id AS productId FROM gurpinar_s
INNER JOIN barcodes ON barcodes.barcodeCode = gurpinar_s.barcode
INNER JOIN products ON products.id = barcodes.productId
WHERE barcodes.barcodeCode NOT IN(SELECT barcodes.barcodeCode FROM stocks
INNER JOIN products ON products.id = stocks.productId
INNER JOIN barcodes ON barcodes.productId = products.id
WHERE stocks.marketId = 6005)");

        return $kayipUrunler;
    }


    public function kayipUrunAktar()
    {
        /*$kayipUrunler = DB::select("SELECT gurpinar_s.barcode, gurpinar_s.price, gurpinar_s.count, products.id AS productId FROM gurpinar_s
INNER JOIN barcodes ON barcodes.barcodeCode = gurpinar_s.barcode
INNER JOIN products ON products.id = barcodes.productId
WHERE barcodes.barcodeCode NOT IN(SELECT barcodes.barcodeCode FROM stocks
INNER JOIN products ON products.id = stocks.productId
INNER JOIN barcodes ON barcodes.productId = products.id
WHERE stocks.marketId = 6005)");

        foreach ($kayipUrunler as $urun){
            Stock::newStock(6005, $urun->productId, $urun->count, $urun->price, 1);
        }*/

    }


    public function olmayanUrunler()
    {
        $olmayanUrunler = DB::select("SELECT gurpinar_s.id, gurpinar_s.productName, gurpinar_s.barcode, gurpinar_s.price, gurpinar_s.count FROM gurpinar_s
                            LEFT JOIN barcodes ON barcodes.barcodeCode = gurpinar_s.barcode
                            WHERE barcodes.barcodeCode IS NULL AND gurpinar_s.id>214");

        return view('gurpinar.olmayanUrunler', compact('olmayanUrunler'));

    }

    public function show($urunId)
    {
        $urun = GS::find($urunId);
        $topCategories = TopCategory::getAll();
        $subCategories = SubCategory::getSubCategoryByTopCatId(1);
        $brands = Brand::getBrands();
        return view('gurpinar.urunGoster', compact('urun', 'topCategories', 'subCategories', 'brands'));
    }

    public function urunKaydet(Request $request, $urunId)
    {

        $this->validate($request,
            [
                'subCategory' => 'required',
                'brand' => 'required',
                'photo' => 'required|image'
            ],
            [
                'subCategory.required' => 'Boş bırakılamaz.',
                'brand.required' => 'Boş bırakılamaz.',
                'photo.required' => 'Boş bırakılamaz.',
                'photo.image'=>'Resim ekle. Başka dosya türü değil.'
            ]
        );

        $urun = GS::find($urunId);
        $subCategoryId = $request->input('subCategory');
        $brand = $request->input('brand');
        $photo = $request->file('photo');
        $productName = $urun->productName;
        $barcode = $urun->barcode;
        $productId = Product::newProduct(ProductType::$standart, $subCategoryId, $brand, '', $productName, '', $barcode, '');
        $photoUrl = 'productImages/' . $subCategoryId . '/' . $productId;
        $photoName = $productName . '-' . Carbon::now()->timestamp . '.' . $photo->extension();
        $photoName = CharacterTransform::transform($photoName);
        //$photoName = $productId . '.' . $photo->extension();
        $this->savePhotos($productId, $photo, $photoName, $photoUrl);
        Stock::newStock(6005, $productId, $urun->count, $urun->price, 1);
        return redirect()->route('olmayanUrunler')->with('message', 'Ürün başarıyla eklendi!');
    }


    public function savePhotos($productId, $photo, $photoName, $photoUrl)
    {

        Size::removeSizes();
        Size::newSize('small', 109, 109);
        Size::newSize('medium', 226, 226);
        Size::newSize('large', 400, 400);
        Size::newSize('xLarge', 600, 600);

        $sizeList = Size::getSizeList();
        $obj = (object)[];
        foreach ($sizeList as $size) {
            /*
                        $hashName = $photoUrl . '/' . $size->sizeName;
                        $path = $photo->hashName($hashName);

                        $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
                        Storage::put($path, (string)$img->encode());
            */

            $path = $photoUrl . '/' . $size->sizeName . '-' . $photoName;
            $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
            Storage::put($path, (string)$img->encode());

            $prop = $size->sizeName;
            $obj->$prop = $path;

        }
        Product::updatePhotoName($productId, json_encode($obj));
    }

    public function altKategoriGetir(Request $request)
    {
        $tId = $request->input('tId');
        return SubCategory::getSubCategoryByTopCatId($tId);
    }

    public function aktar()
    {
        /*
                //$gsList = GS::all();//hulya hanim verdigi stoklar
                $gurpinar_stok = GurpinarStok::where('photo', '!=', '')
                    ->join('market.gurpinar_s', 'gurpinar_s.barcode', '=', 'gurpinar_stok.barcode')
                    ->select([
                        'gurpinar_stok.productName',
                        'gurpinar_s.price',
                        'gurpinar_s.count',
                        'gurpinar_stok.barcode',
                        'gurpinar_stok.subCategoryId',
                        'gurpinar_stok.photo'
                    ])
                    ->get();
        */
        /*
                $products = Product::whereDate('created_at', Carbon::now()->toDateString())
                    ->where('id', '>', 9000)
                    ->get();

                Size::removeSizes();
                Size::newSize('small', 109, 109);
                Size::newSize('medium', 226, 226);
                Size::newSize('large', 400, 400);
                Size::newSize('xLarge', 600, 600);

                $sizeList = Size::getSizeList();
        */
        /*
                foreach ($products as $product) {
                    $obj = (object)[];

                    foreach ($sizeList as $size) {
                        $prop = $size->sizeName;
                        $photo = json_decode($product->photo);
                        $path = $photo->$prop;

                        $i = pathinfo(Storage::url('app/gurpinarResim/' . $photo->$prop));

                        $extension = $i["extension"];
                        $filename = $i["filename"];

                        $imgPathToFetch = 'gurpinarResim/' . $path;
                        $imgPathToStore = 'productImages/' . $product->subCategoryId . '/' . $product->id . '/' . $filename . '.' . $extension;
                        Storage::copy($imgPathToFetch, $imgPathToStore);
                        $obj->$prop = 'productImages/' . $product->subCategoryId . '/' . $product->id . '/' . $filename . '.' . $extension;


                    }
                    Product::updatePhotoName($product->id, json_encode($obj));

                    -----
                    $subCategoryId = $stok->subCategoryId;
                    $productName = $stok->productName;
                    $photo = $stok->photo;
                    $barcode = $stok->barcode;
                    //echo $productName. ' - ' .$subCategoryId . ' - ' . $barcode. '<br>';
                    //$pid = Product::newGSProduct($productName, $subCategoryId, $photo, $barcode);
                    //Stock::newStock(6005, $pid, $stok->count, $stok->price, 1);
                    ------
                }
        */

    }
}
