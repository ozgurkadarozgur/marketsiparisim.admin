<?php

namespace App\Http\Controllers;

use App\NewProducts;
use App\OrderDetail;
use App\Product;
use App\Stock;
use Carbon\Carbon;
use const Grpc\STATUS_OUT_OF_RANGE;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Zend\Diactoros\Response;

class TestController extends Controller
{
    public function sendNotification()
    {
        $tokens = [
            'cURBII1ngbY:APA91bG8Mfz1L3zLdEtD3be7MhWn1QSLc4LFtzfUF-jjKelx4SMm1sO5epPsMCqOdqzvrjOCHPcHkxZakzJ_yCb9ijHDcMTOLjXo302jn4LBZYjS3tpJP4wpCfb4SnDGRixbuHMjgXz-',
            'cfil4j9Cp5c:APA91bF56auiPH4njj9xm_VQX4x4cfZguvUFRxzofm8Zqt981J0WGNZBG1aDt6H4flWdbZ6J12MoE6S5-fpcKW69mFXXAfqF_NMurCR8W38L1vZaW3vBaA46dsz3_ZckPFP0NWxtcVcR',
            'dhl23v_Pvb0:APA91bFaCPOQIcn677e4FoaIlpehhcB_yEVPLoBEKdm986nzxdDdsa0_R0yFH7ita77sPfHmdocF4smgOMqXVZWzzKlOvd0TV_lx2xKlyzs9lz-rhXKGV5sZyFA08YNGuCqOJhGrx2Ex',
            'fojyW6XmHhs:APA91bE9869QtIqoMwYs_9eB-R1vz57Ch4BSP8EMY32_gskEh9xCdXS5GZSd4iHccdBJFGOOsPee_zXbZSgJQfSxx6llYRjCjZOGXTg4I_uNERRa8tpWW4iKrX8dtNFi3-avIWJdRtcN',
            'cZn-ypjBJJY:APA91bGjaOcXHn6LXqqiK23J2DqefFkHHbDdOPzaGf5MxvPoOn2c4cCqrZbGSNYv5WBQkLbPNkSj-OAD4G3Y0r1gXLd1wXhvHLwwEYzgy6QNJSEtJz1NZOa1OinYCPNrtfYyd7Eu10kk'
        ];
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'registration_ids' => $tokens,
            'content_available' => true,
            'priority' => 'high',
            'notification' => [
                'body' => 'Köy Peynirinde İndirim.',
                'title' => 'MARKET SİPARİŞİM'
            ],
            'data' => array(

                'id' => '248e9696-d2fa-11e7-be7d-208984fd02a1',
                'productName' => 'Aynes Tam Yağlı Beyaz Peynir 1000 Gr',
                'photo' => (object)[
                    'small' => 'photos/category/1/sub/2/product/small-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg',
                    'medium' => 'photos/category/1/sub/2/product/medium-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg',
                    'large' => 'photos/category/1/sub/2/product/large-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg',
                    'xLarge' => 'photos/category/1/sub/2/product/xLarge-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg'
                ]

            )
        );

        /*

        '{"small":"photos\/category\/1\/sub\/2\/product\/small-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","medium":"photos\/category\/1\/sub\/2\/product\/medium-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","large":"photos\/category\/1\/sub\/2\/product\/large-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg","xLarge":"photos\/category\/1\/sub\/2\/product\/xLarge-aynes-tam-yagli-beyaz-peynir-1000-gr-1505828010.jpeg"}'

         */

        $headers = array(
            'Authorization:key = AAAAsHVPJrU:APA91bE9jk9yGukEvmxRWPMgv1LfbcGN-4EyqE_nCYaHjjaFHPBtGBVF5HIRir9KmvM6037urodBex7Y7HU5gEErb0RFaFvHlbleY9sdBudDR46sj9uUhGFz0bXh3l_0M_g_840p614k',
            'Content-Type: application/json'
        );

        /*
        $headers = array(
            'Authorization:key = AIzaSyCQVSNg0496s9zJnO6ZGOXeZwFDhjjRw5k',
            'Content-Type: application/json'
        );
*/
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public function logo()
    {

        $now = Carbon::now();
        $date = Carbon::createFromFormat('d-m-Y', $now->toDateString());


        $xml = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?>
<SALES_INVOICES>
  <INVOICE DBOP=\"INS\" >
    <TYPE>9</TYPE>
    <NUMBER>DENEME</NUMBER>
    <DATE>11.12.2017</DATE>
    <TIME>1513023130</TIME>
    <ARP_CODE>120.11111</ARP_CODE>
    <POST_FLAGS>247</POST_FLAGS>
    <VAT_RATE>18</VAT_RATE>
    <TOTAL_DISCOUNTED>0</TOTAL_DISCOUNTED>
    <TOTAL_VAT>90</TOTAL_VAT>
    <TOTAL_GROSS>500</TOTAL_GROSS>
    <TOTAL_NET>590</TOTAL_NET>
    <TC_NET>590</TC_NET>
    <RC_XRATE>1</RC_XRATE>
    <RC_NET>590</RC_NET>
    <CREATED_BY>1</CREATED_BY>
    <DATE_CREATED>11.12.2017</DATE_CREATED>
    <HOUR_CREATED>23</HOUR_CREATED>
    <MIN_CREATED>12</MIN_CREATED>
    <SEC_CREATED>10</SEC_CREATED>
    <CURRSEL_TOTALS>1</CURRSEL_TOTALS>
    <DATA_REFERENCE>8027</DATA_REFERENCE>
    <DISPATCHES>
    </DISPATCHES>
    <TRANSACTIONS>
      <TRANSACTION>
        <TYPE>4</TYPE>
        <MASTER_CODE>005</MASTER_CODE>
        <GL_CODE2>391.01.001</GL_CODE2>
        <QUANTITY>1</QUANTITY>
        <PRICE>500</PRICE>
        <TOTAL>500</TOTAL>
        <RC_XRATE>1</RC_XRATE>
        <UNIT_CODE>ADET</UNIT_CODE>
        <UNIT_CONV1>1</UNIT_CONV1>
        <UNIT_CONV2>1</UNIT_CONV2>
        <VAT_RATE>18</VAT_RATE>
        <VAT_AMOUNT>90</VAT_AMOUNT>
        <VAT_BASE>500</VAT_BASE>
        <BILLED>1</BILLED>
        <TOTAL_NET>500</TOTAL_NET>
        <DATA_REFERENCE>9316</DATA_REFERENCE>
        <DIST_ORD_REFERENCE>0</DIST_ORD_REFERENCE>
        <CAMPAIGN_INFOS>
          <CAMPAIGN_INFO>
          </CAMPAIGN_INFO>
        </CAMPAIGN_INFOS>
        <EDT_CURR>160</EDT_CURR>
        <EDT_PRICE>500</EDT_PRICE>
        <ORGLOGOID></ORGLOGOID>
        <GENIUSFLDSLIST>
        </GENIUSFLDSLIST>
        <DEFNFLDSLIST>
        </DEFNFLDSLIST>
        <MONTH>12</MONTH>
        <YEAR>2017</YEAR>
        <PREACCLINES>
        </PREACCLINES>
        <UNIT_GLOBAL_CODE>NIU</UNIT_GLOBAL_CODE>
        <EDTCURR_GLOBAL_CODE>TL</EDTCURR_GLOBAL_CODE>
        <GUID>75D76F7C-4575-4CE7-B881-77D01A2A7C09</GUID>
        <MASTER_DEF>KOMİSYON BEDELİ</MASTER_DEF>
        <FOREIGN_TRADE_TYPE>1</FOREIGN_TRADE_TYPE>
        <DISTRIBUTION_TYPE_WHS>0</DISTRIBUTION_TYPE_WHS>
        <DISTRIBUTION_TYPE_FNO>0</DISTRIBUTION_TYPE_FNO>
      </TRANSACTION>
    </TRANSACTIONS>
    <PAYMENT_LIST>
      <PAYMENT>
        <DATE>11.12.2017</DATE>
        <MODULENR>4</MODULENR>
        <TRCODE>9</TRCODE>
        <TOTAL>590</TOTAL>
        <PROCDATE>11.12.2017</PROCDATE>
        <REPORTRATE>1</REPORTRATE>
        <DATA_REFERENCE>0</DATA_REFERENCE>
        <DISCOUNT_DUEDATE>11.12.2017</DISCOUNT_DUEDATE>
        <PAY_NO>1</PAY_NO>
        <DISCTRLIST>
        </DISCTRLIST>
        <DISCTRDELLIST>0</DISCTRDELLIST>
      </PAYMENT>
    </PAYMENT_LIST>
    <ORGLOGOID></ORGLOGOID>
    <DEFNFLDSLIST>
    </DEFNFLDSLIST>
    <DEDUCTIONPART1>2</DEDUCTIONPART1>
    <DEDUCTIONPART2>3</DEDUCTIONPART2>
    <DATA_LINK_REFERENCE>8027</DATA_LINK_REFERENCE>
    <INTEL_LIST>
      <INTEL>
      </INTEL>
    </INTEL_LIST>
    <AFFECT_RISK>0</AFFECT_RISK>
    <PREACCLINES>
    </PREACCLINES>
    <DOC_DATE>11.12.2017</DOC_DATE>
    <PROFILE_ID>2</PROFILE_ID>
    <GUID>B90DA79D-0849-4F08-A82D-DFE339C3E04C</GUID>
    <EDURATION_TYPE>0</EDURATION_TYPE>
    <EDTCURR_GLOBAL_CODE>TL</EDTCURR_GLOBAL_CODE>
    <TOTAL_NET_STR>Beş yüz doksan TL</TOTAL_NET_STR>
    <TOTAL_SERVICES>500</TOTAL_SERVICES>
    <EXIMVAT>0</EXIMVAT>
    <EARCHIVEDETR_INTPAYMENTTYPE>0</EARCHIVEDETR_INTPAYMENTTYPE>
  </INVOICE>
</SALES_INVOICES>");

/*
        $response = \Illuminate\Support\Facades\Response::make($xml->asXML(), 200);
        $response->header('Cache-Control', 'public');
        $response->header('Content-Description', 'File Transfer');
        $response->header('Content-Disposition', 'attachment; filename=test.xml');
        $response->header('Content-Transfer-Encoding', 'binary');
        $response->header('Content-Type', 'text/xml');
*/

        Storage::put('logoDeneme.xml', $xml->asXML());
        //$xml->saveXML(Storage::url('app/logo.xml'));
        //return MoneyConversionToText::int_to_words(1245);
        //echo  Carbon::now()->timestamp. '<br>'. Carbon::now()->toDateTimeString();

    }


    public function ntfTest()
    {
        Storage::append('ntfTest.txt', Carbon::now()->toDateTimeString() . ' - calisti\n');
    }

    public function ntfTestRead()
    {
        return Storage::disk('local')->get('ntfTest.txt');
    }

/*
    public function fiyatDuzelt()
    {
        $orderDetails = OrderDetail::all();
        foreach ($orderDetails as $orderDetail){
            $stock = Stock::getProduct($orderDetail->productId);
            if($stock){
                $orderDetail->unitPrice = $stock->price;
                $orderDetail->save();
            }
        }
    }
*/

    public function olmayanUrunBul()
    {
        $yeniUrunler = NewProducts::where('photo', '!=', '')
            ->where('id', '>', 9555)
            ->get();

        Size::removeSizes();
        Size::newSize('small', 109, 109);
        Size::newSize('medium', 226, 226);
        Size::newSize('large', 400, 400);
        Size::newSize('xLarge', 600, 600);

        $sizeList = Size::getSizeList();
        foreach ($yeniUrunler as $product){
            if(!Product::getProductByBarcode($product->barcode)){
                $pid= Product::newProduct(1, $product->subCategoryId, 9999, '', $product->productName, '', $product->barcode, '');
                $obj = (object)[];
                foreach ($sizeList as $size) {
                    $prop = $size->sizeName;
                    $photo = json_decode($product->photo);
                    $path = $photo->$prop;


                    $i = pathinfo(Storage::url('app/yeniResimler/' . $photo->$prop));

                    $extension = $i["extension"];
                    $filename = $i["filename"];

                    $imgPathToFetch = 'yeniResimler/' . $path;
                    $imgPathToStore = 'yeniResimler-duzgun/productImages/' . $product->subCategoryId . '/' . $pid . '/' . $filename . '.' . $extension;
                    Storage::copy($imgPathToFetch, $imgPathToStore);
                    $obj->$prop = 'productImages/' . $product->subCategoryId . '/' . $pid . '/' . $filename . '.' . $extension;


                }
                Product::updatePhotoName($pid, json_encode($obj));
            }
        }


        //return \response()->json($yeniUrunler);

    }

    public function aksutStokAktar()
    {
        $cinarStoklar = DB::select("select * from market.stocks where marketId = 6007");
        foreach ($cinarStoklar  as $stok){
            Stock::newStock(6021, $stok->productId, $stok->total, $stok->price, 1);
        }
    }

    public function sanalpos()
    {
        $clientId = "100100000";			//Merchant Id defined by bank to user
        $amount = "9.95";					//Transaction amount
        $oid = "";							//Order Id. Must be unique. If left blank, system will generate a unique one.

        $okUrl = "http://localhost/3dmodelpayment.php";		//URL which client be redirected if authentication is successful
        $failUrl = "http://localhost/3dmodelpayment.php";	//URL which client be redirected if authentication is not successful

        $rnd = microtime();				//A random number, such as date/time
        $currencyVal = "949";			//Currency code, 949 for TL, ISO_4217 standard
        $storekey = "123456";		//Store key value, defined by bank.
        $storetype = "3d";				//3D authentication model
        $lang = "tr";					//Language parameter, "tr" for Turkish (default), "en" for English

        $hash=base64_encode(sha1( $clientId . $oid . $amount . $okUrl . $failUrl . $rnd  . $storekey , "ISO-8859-9")); //Hash value used for validation

        $url = "https://entegrasyon.asseco-see.com.tr/fim/est3Dgate";

        $client = new Client();
        $result = $client->request('POST',$url,
            [
                'allow_redirects'=>true,
                'form_params'=>[
                    'pan' => '4355084355084358',
                    'cv2' => '000',
                    'Ecom_Payment_Card_ExpDate_Month' => '12',
                    'Ecom_Payment_Card_ExpDate_Year' => '2018',
                    'cardType' => '1',
                    'clientId' => $clientId,
                    'amount' => $amount,
                    'oid' => $oid,
                    'okUrl' => $okUrl,
                    'failUrl' => $failUrl,
                    'rnd' => $rnd,
                    'hash' => $hash,
                    'storetype' => $storetype,
                    'lang' => $lang,
                    'currency' => $currencyVal
                ]
            ]
        );
var_dump($result);


    }


}
