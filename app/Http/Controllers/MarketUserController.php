<?php

namespace App\Http\Controllers;

use App\Market;
use App\MarketUser;
use Illuminate\Http\Request;

class MarketUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($marketId)
    {
        $market = Market::getMarket($marketId);
        return view('marketUser.create', compact('market'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $marketId)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'username' => 'required|unique:marketUsers,username',
                'email' => 'required|email|unique:marketUsers,email',
                'password' => 'required'
            ],
            [
                'name.required' => 'Ad - Soyad boş bırakılamaz.',
                'username.required' => 'Kullanıcı Adı boş bırakılamaz.',
                'username.unique' => 'Girdiğiniz kullanıcı adı başka bir kullanıcı tarafından kullanılmaktadır.',
                'email.required' => 'E-Mail boş bırakılamaz.',
                'email.email' => 'Geçerli bir email adresi girin.',
                'email.unique' => 'Girdiğiniz email adresi adı başka bir kullanıcı tarafından kullanılmaktadır.',
                'password.required' => 'Şifre boş bırakılamaz.',
            ]
        );

        $name = $request->input('name');
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        MarketUser::setMarketUser($marketId, $name, $username, $email, $password);
        return redirect()->back()->with('message', 'Kullanıcı başarıyla oluşturuldu !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
