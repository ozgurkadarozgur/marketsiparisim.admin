<?php

namespace App\Http\Controllers;

use App\CustomerUser;
use App\Order;
use App\OrderCancelReason;
use App\OrderDenyReason;
use App\OrderDetail;
use App\PaymentMethod;
use App\StatisticCustomer;
use Illuminate\Http\Request;

class StatisticCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = CustomerUser::getCustomerUsers();
        return view('statistics.customer.index', compact('customers'));
    }

    public function showOrder($orderId)
    {
        $order = Order::getOrder($orderId);
        $totalPrice = Order::getOrderPrice($orderId)[0]->totalPrice;

        return view('statistics.customer.showOrder', compact('order', 'totalPrice', 'cancelReasons', 'denyReasons', 'notDeliverReasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($customerId)
    {
        $customer = CustomerUser::getCustomerUser($customerId);
        $creditCartOrderCount = StatisticCustomer::getCreditCartOrderCount($customerId);
        $creditCartOrderPrice = StatisticCustomer::getOrderPriceByPaymentMethod($customerId, PaymentMethod::$creditCart);
        $cashOrderCount = StatisticCustomer::getCashOrderCount($customerId);
        $cashOrderPrice = StatisticCustomer::getOrderPriceByPaymentMethod($customerId, PaymentMethod::$cash);
        $referenceCount = StatisticCustomer::getReferenceCount($customerId);
        $customerInfo = CustomerUser::getCustomerInfo($customerId);
        $marketMoney = $customerInfo->marketMoney;

        $statistics = (object)[
            'creditCartOrderCount' => $creditCartOrderCount,
            'creditCartOrderPrice' => $creditCartOrderPrice,
            'cashOrderCount' => $cashOrderCount,
            'cashOrderPrice' => $cashOrderPrice,
            'referenceCount' => $referenceCount,
            'marketMoney'=>$marketMoney
        ];

        $latestOrders = Order::getPastOrders($customerId);

        return view('statistics.customer.show', compact('customer', 'statistics', 'latestOrders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
