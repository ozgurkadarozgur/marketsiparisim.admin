<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use App\Stock;
use App\SubCategory;
use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use PhpParser\Node\Expr\Array_;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    //ajax

    public function getMarketProducts(Request $request)
    {
        $marketId = $request->input('marketId');
        return Stock::getStockProductsJSON($marketId);
    }

    //end ajax

    public function search()
    {
        return view('product.search');
    }

    public function findProductByBarcode(Request $request)
    {
        $this->validate($request,
            [
                'barcode'=>'required'
            ],
            [
                'barcode.required'=>'Boş bırakılamaz.'
            ]
        );

        $barcode = $request->input('barcode');

        $result = Product::getProductByBarcode($barcode);
        if($result) $product = Product::getProduct($result->id);
        else $product = 'none';

        return redirect()->back()->with('product', $product);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoryId)
    {
        $subCategory = SubCategory::getCategory($categoryId);
        $brands = Brand::getBrands();
        return view('product.create', compact('subCategory', 'brands'));
    }

//Deneme
    public function urunInsert()
    {
        return view('product.try');
    }

    public function urunStore(Request $request)
    {
        $photo = $request->file('photo');
        $path = $photo->hashName('public/');

        $img = \Intervention\Image\Facades\Image::make($photo)->resize(470, 560);
        Storage::put($path, (string)$img->encode());

        //$img->save(env('PHOTO_URL').'/storage/app/photos/a.jpg');
        //return $img->response('jpg');
    }
//end Deneme

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    public function savePhotos($productId, $photo, $photoName, $photoUrl)
    {

        Size::removeSizes();
        Size::newSize('small', 109, 109);
        Size::newSize('medium', 226, 226);
        Size::newSize('large', 400, 400);
        Size::newSize('xLarge', 600, 600);

        $sizeList = Size::getSizeList();
        $obj = (object)[];
        foreach ($sizeList as $size) {
            /*
                        $hashName = $photoUrl . '/' . $size->sizeName;
                        $path = $photo->hashName($hashName);

                        $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
                        Storage::put($path, (string)$img->encode());
            */

            $path = $photoUrl . '/' . $size->sizeName . '-' . $photoName;
            $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
            Storage::put($path, (string)$img->encode());

            $prop = $size->sizeName;
            $obj->$prop = $path;

        }
        Product::updatePhotoName($productId, json_encode($obj));
    }

    public function store(Request $request, $categoryId)
    {
        $this->validate($request,
            [
                'productCode' => 'required|unique:products,productCode',
                'productName' => 'required',
                'price' => 'required',
                'barcode' => 'required|unique:barcodes,barcodeCode'
            ],
            [
                'productCode.required' => 'Ürün kodu boş bırakılamaz !',
                'productCode.unique' => 'Girdiğiniz ürün kodu başka bir ürüne ait !',
                'productName.required' => 'Ürün adı boş bırakılamaz !',
                'price.required' => 'Fiyat boş bırakılamaz !',
                'barcode.required' => 'Barkod boş bırakılamaz !',
                'barcode.unique' => 'Girdiğiniz barkod başka bir ürüne ait !'
            ]
        );


        $subCategory = SubCategory::getCategory($categoryId);
        $topCategoryId = $subCategory->topCategory->id;

        $photo = $request->file('photo');
        $brandId = $request->input('brandId');
        $productCode = $request->input('productCode');
        $productName = $request->input('productName');
        $price = $request->input('price');
        $barcode = $request->input('barcode');
        $barcode = str_replace(',', '.', $barcode);
        $sequenceNumber = Product::sequenceNumber();
        $productId = Product::newProduct(1 ,$categoryId, $brandId, $productCode, $productName, $price, $barcode, $sequenceNumber);

        $photoUrl = 'productImages/' . $categoryId . '/' . $productId;
        $photoName = $productName . '-' . Carbon::now()->timestamp . '.' . $photo->extension();
        $photoName = CharacterTransform::transform($photoName);
        //$photoName = $productId . '.' . $photo->extension();
        $this->savePhotos($productId, $photo, $photoName, $photoUrl);
        //$photo->storeAs($photoUrl, $photoName);
        //Image::make($photo)->resize(50, 50)->save('foo.jpg');
        //\Intervention\Image\Facades\Image::make($photo)->resize(50, 50)->save('public/foo.jpg');

        return redirect()->back()->with('message', 'Ürün başarıyla eklendi !');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::getProduct($id);
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function editPhoto($productId)
    {
        $product = Product::getProduct($productId);
        return view('product.editPhoto', compact('product'));
    }

    public function updatePhoto(Request $request, $productId)
    {
        $this->validate($request,
            [
                'productPhoto' => 'required|image'
            ],
            [
                'productPhoto.required' => 'Bir resim seçin.',
                'productPhoto.image' => 'Seçtiğiniz dosya bir resim dosyası değil. Lütfen bir resim dosyası seçin.'
            ]
        );

        $product = Product::getProduct($productId);
        $subCategory = $product->subCategory;
        $subCategoryId = $subCategory->id;
        $topCategoryId = $subCategory->topCategory->id;

        $photo = $request->file('productPhoto');
        $photoUrl = 'productImages/' . $subCategoryId . '/' . $productId;
        $photoName = $product->productName . '-' . Carbon::now()->timestamp . '.' . $photo->extension();
        $photoName = CharacterTransform::transform($photoName);

        $this->savePhotos($productId, $photo, $photoName, $photoUrl);

        return redirect()->back()->with('message', 'Ürün fotoğrafı başarıyla güncellendi !');
    }

    public function update(Request $request, $productId)
    {
        $this->validate($request,
            [
                'productCode' => 'required',
                'productName' => 'required'
            ],
            [
                'productCode.required' => 'Ürün kodu boş bırakılamaz !',
                'productName.required' => 'Ürün adı boş bırakılamaz !'
            ]
        );

        $productCode = $request->input('productCode');
        $productName = $request->input('productName');

        $product = Product::getProduct($productId);
        $product->productCode = $productCode;
        $product->productName = $productName;
        $product->save();

        return redirect()->back()->with('message', 'Ürün başarıyla güncellendi !');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($productId)
    {
        $product = Product::getProduct($productId);
        $product->barcodes()->delete();
        $product->delete();
        $subCategoryId = $product->subCategory->id;
        return redirect()->to(env('APP_URL') . 'SubCategory/' . $subCategoryId)->with('message', 'Ürün başarıyla silindi !');
    }
}
