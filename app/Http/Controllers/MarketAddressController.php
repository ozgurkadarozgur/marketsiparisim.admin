<?php

namespace App\Http\Controllers;

use App\Area;
use App\County;
use App\Neighborhood;
use Illuminate\Http\Request;

class MarketAddressController extends Controller
{
    public function getCountiesByCityId(Request $request)
    {
        $cityId = $request->input('cityId');
        return County::getCountiesByCityId($cityId);
    }

    public function getAreasByCountyId(Request $request)
    {
        $countyId = $request->input('countyId');
        return Area::getAreasByCountyId($countyId);
    }

    public function getNeighborhoodsByAreaId(Request $request)
    {
        $areaId = $request->input('areaId');
        return Neighborhood::getNeighborhoodsByAreaId($areaId);
    }

}
