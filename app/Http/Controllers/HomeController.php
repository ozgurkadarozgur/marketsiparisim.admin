<?php

namespace App\Http\Controllers;

use App\CustomerAddress;
use App\CustomerUser;
use App\Events\NewOrderEvent;
use App\Market;
use App\Order;
use App\OrderDetail;
use App\ShoppingCart;
use App\StatisticMarket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Orchestra\Parser\XmlServiceProvider;
use Psy\Util\Json;
use Ramsey\Uuid\Uuid;
use Orchestra\Parser\Xml;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $startDate= Carbon::now()->subDays(15)->toDateString();
        $endDate = Carbon::now()->toDateString();
        $dailyOrderCounts = json_encode(StatisticMarket::getDailyOrderCountByDateRange(1, $startDate, $endDate));
        $marketCount = Market::getMarketCount();
        $customerCount = CustomerUser::getCustomerCount();
        $totalOrderCount = Order::getTotalOrderCount();
        $todayOrderCount = Order::getTodayOrderCount();
        return view('home', compact('dailyOrderCounts', 'marketCount', 'customerCount', 'totalOrderCount', 'todayOrderCount'));
    }

    public function map()
    {
        $latitude = 36.949111;
        $longitude = 35.315614;
    }

    /*
        public function yeniadres()
        {
            CustomerAddress::newAddress(1, 'Ev', 'Mıdık Mahallesi');
        }
    */

    public function bill()
    {
        return Order::getPastOrders(1);
    }

    public function usr()
    {
        return ShoppingCart::getShoppingCarts(1);
    }

    public function order($marketId)
    {
        event(new NewOrderEvent('ozgur', $marketId));
    }

}
