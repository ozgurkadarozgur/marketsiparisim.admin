<?php

namespace App\Http\Controllers;

use App\OrderDenyReason;
use Illuminate\Http\Request;

class OrderDenyReasonController extends Controller
{
    public function index()
    {
        $reasons = OrderDenyReason::getReasons();
        return view('orderDenyReason.index', compact('reasons'));
    }

    public function create()
    {
        return view('orderDenyReason.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'reason'=>'required'
            ],
            [
                'reason.required'=>'Boş bırakılamaz.'
            ]
        );

        $reason = $request->input('reason');
        OrderDenyReason::newReason($reason);
        return redirect()->back()->with('message', 'Sipariş reddetme nedeni başarıyla eklendi !');

    }

}
