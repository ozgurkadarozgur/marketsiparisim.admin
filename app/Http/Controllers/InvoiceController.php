<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceDetail;
use App\InvoiceFinal;
use App\InvoiceType;
use App\Market;
use App\MarketUser;
use App\MSConfig;
use App\PaymentMethod;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function index()
    {
        return view('');
    }

    public function show($marketId)
    {
        $market = Market::getMarket($marketId);
        $invoices = Invoice::where('marketId', $marketId)->get();

        return view('invoice.show', compact('market', 'invoices'));

    }

    public function gecmisFaturaOlustur()
    {

        $invoices = DB::select("SELECT
                week(orders.created_at) AS weekNo,
                month(orders.created_at) AS monthNo,
                year(orders.created_at) AS year,                
                orders.marketId,
                sum(orderDetails.unitPrice * orderDetails.count) AS total
                FROM orderDetails
                INNER JOIN orders ON orders.id = orderDetails.orderId
                WHERE orders.marketId != 1
                GROUP BY week(orders.created_at), 
                month(orders.created_at), 
                year(orders.created_at),                
                orders.marketId");

        foreach ($invoices as $invoice) {
            $date = Carbon::now()->setISODate($invoice->year, $invoice->weekNo + 1);
            $startDate = $date->startOfWeek()->toDateString();
            $endDate = $date->endOfWeek()->toDateString();
            $commissionRate = MSConfig::getCommissionRate();
            $invoiceId = Invoice::createInvoice($invoice->marketId, $invoice->total, $commissionRate, $invoice->weekNo, $invoice->monthNo, $invoice->year, $startDate, $endDate);

            $invoiceDetails = DB::select("select 
                    week(orders.created_at) as weekNo,
                    month(orders.created_at) as month,
                    year(orders.created_at) as year,
                    orders.paymentMethodId,
                    orders.marketId,
                    sum(orderDetails.unitPrice * orderDetails.count) as total
                    from orderDetails
                    inner join orders on orders.id = orderDetails.orderId
                    where orders.marketId = $invoice->marketId AND 
                    week(orders.created_at) = $invoice->weekNo
                    group by 
                    week(orders.created_at), 
                    month(orders.created_at), 
                    year(orders.created_at),
                    orders.paymentMethodId,
                    orders.marketId");


            $commission = 0.0;
            $debtToSystem = 0.0;
            $credit = 0.0;

            foreach ($invoiceDetails as $invoiceDetail){

                switch ($invoiceDetail->paymentMethodId){
                    case PaymentMethod::$cash : {
                        $commission = $invoiceDetail->total * 5 / 100;
                        $debtToSystem = $invoiceDetail->total - $commission;
                        break;
                    }
                    case PaymentMethod::$creditCart : {
                        $commission = $invoiceDetail->total * 5 / 100;
                        break;
                    }
                    case PaymentMethod::$marketMoney : {
                        $commission = $invoiceDetail->total * 5 / 100;
                        break;
                    }
                }

                InvoiceDetail::setInvoiceDetail($invoiceId, $invoiceDetail->paymentMethodId, $invoiceDetail->total, $commission, $debtToSystem, $credit);

                $currentFinal = $debtToSystem - $credit;
                $final = DB::table('invoiceFinal')
                    ->where('marketId', $invoice->marketId)
                    ->sum('currentFinal');

                InvoiceFinal::setInvoiceFinal($invoiceId, $invoice->marketId, $currentFinal, $final + $currentFinal);

            }

        }

    }

}
