<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 11.12.2017
 * Time: 18:41
 */

namespace App\Http\Controllers;


class MoneyConversionToText
{

    public static $nWords = array("sıfır", "bir", "iki", "üç", "dört", "beş", "altı", "yedi",
        "sekiz", "dokuz", "on", "on bir", "on iki", "on üç",
        "on dört", "on beş", "on altı", "on yedi", "on sekiz",
        "on dokuz", "yirmi", 30 => "otuz", 40 => "kırk",
        50 => "elli", 60 => "altmış", 70 => "yetmiş", 80 => "seksen",
        90 => "doksan");


    public static function int_to_words($x)
    {
        $nwords = self::$nWords;
        if (!is_numeric($x))
            $w = '#';
        else if (fmod($x, 1) != 0)
            $w = '#';
        else {
            if ($x < 0) {
                $w = 'eksi ';
                $x = -$x;
            } else
                $w = '';
            // ... now $x is a non-negative integer.

            if ($x < 21)   // 0 to 20
                $w .= $nwords[$x];
            else if ($x < 100) {   // 21 to 99
                $w .= $nwords[10 * floor($x / 10)];
                $r = fmod($x, 10);
                if ($r > 0)
                    $w .= ' ' . $nwords[$r];
            } else if ($x < 1000) {   // 100 to 999
                if(floor($x / 100) > 1)
                    $w .= $nwords[floor($x / 100)] . ' yüz';
                else
                    $w .= ' yüz';
                $r = fmod($x, 100);
                if ($r > 1)
                    $w .= ' ' . self::int_to_words($r);
            } else if ($x < 1000000) {   // 1000 to 999999

                if(floor($x / 1000) > 1)
                    $w .= self::int_to_words(floor($x / 1000)) . ' bin';
                else
                    $w .=' bin';
                $r = fmod($x, 1000);
                if ($r > 0) {
                    $w .= ' ';
                    $w .= self::int_to_words($r);
                }
            } else {    //  millions
                $w .= self::int_to_words(floor($x / 1000000)) . ' milyon';
                $r = fmod($x, 1000000);
                if ($r > 0) {
                    $w .= ' ';
                    $w .= self::int_to_words($r);
                }
            }
        }
        return $w;
    }


}