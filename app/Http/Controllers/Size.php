<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 14.08.2017
 * Time: 13:47
 */

namespace App\Http\Controllers;


use function Symfony\Component\Debug\Tests\testHeader;

class Size
{
    /*
        public static $arr = [
            (object)['sizeName' => 'xLarge', 'x' => 600, 'y' => 600],
            (object)['sizeName' => 'large', 'x' => 400, 'y' => 400],
            (object)['sizeName' => 'medium', 'x' => 226, 'y' => 226],
            (object)['sizeName' => 'small', 'x' => 109, 'y' => 109],
        ];
    */

    public static $arr = array();

    public static function newSize($sizeName, $x, $y)
    {
        $size = (object)[];
        $size->sizeName = $sizeName;
        $size->x = $x;
        $size->y = $y;
        array_push(Size::$arr, $size);
    }

    public static function removeSizes()
    {
        Size::$arr = array();
    }

    public static function getSizeList()
    {
        return Size::$arr;
    }

}