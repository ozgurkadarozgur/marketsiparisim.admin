<?php
/**
 * Created by PhpStorm.
 * User: ozgur
 * Date: 22.12.2017
 * Time: 19:29
 */

namespace App\Http\Controllers;


class MSMail
{
    public static function send($to, $subject, $message)
    {
        $headers = "From: info@marketsiparisim.com\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        mail($to, $subject, $message, $headers);
    }

}