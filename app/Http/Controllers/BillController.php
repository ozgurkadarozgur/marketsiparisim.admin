<?php

namespace App\Http\Controllers;

use App\Bill;
use App\InvoiceType;
use App\Market;
use Illuminate\Http\Request;

class BillController extends Controller
{

    public function index($marketId)
    {
        $market = Market::getMarket($marketId);
        $bills = Bill::getBillsByMarketId($marketId);
        return view('bill.index', compact('market','bills'));
    }

    public function show($billId)
    {
        $bill = Bill::getBill($billId);
        return view('bill.show', compact('bill'));
    }

    public function create($marketId)
    {
        $market = Market::getMarket($marketId);
        $invoiceTypes = InvoiceType::getTypes();
        return view('bill.create', compact('market', 'invoiceTypes'));
    }

    public function store(Request $request, $marketId)
    {
        $this->validate($request,
            [
                'invoiceType' => 'required',
                'price' => 'required',
                'billNo' => 'required'
            ],
            [
                'invoiceType.required' => 'Boş bırakılamaz.',
                'price.required' => 'Boş bırakılamaz.',
                'billNo.required' => 'Boş bırakılamaz.',
            ]
        );

        $billNo = $request->input('billNo');
        $price = $request->input('price');
        $invoiceTypeId = $request->input('invoiceType');

        Bill::storeBill($billNo, $marketId, $price, $invoiceTypeId);

        return redirect()->back()->with('message', 'Fatura başarıyla kaydedildi.');

    }

}
