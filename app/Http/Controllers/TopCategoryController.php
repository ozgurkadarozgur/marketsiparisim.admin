<?php

namespace App\Http\Controllers;

use App\TopCategory;
use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TopCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topCategories = TopCategory::all();
        return view('topCategory.index', compact('topCategories'));
    }

    public function getSubCategories(Request $request)
    {
        $topCatId = $request->input('topCatId');
        $topCategory = TopCategory::getCategory($topCatId);
        return $topCategory->subCategories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'topCategoryName' => 'required'
            ],
            [
                'topCategoryName.required' => 'Kategori adı boş birakilamaz.'
            ]
        );

        $categoryName = $request->input('topCategoryName');
        $photo = $request->file('photo');
        $categoryId = TopCategory::newTopCategory($categoryName);
        $photoUrl = 'categoryPhotos/' . $categoryId;
        $photoName = $categoryName . '-' .Carbon::now()->timestamp . '.' . $photo->extension();
        $photoName = CharacterTransform::transform($photoName);
        $this->saveTopCategoryPhotos($categoryId, $photo, $photoName, $photoUrl);
        //$photoName = $categoryId . '.' . $photo->extension();
        //$photo->storeAs($photoUrl, $photoName);
        //TopCategory::updatePhotoName($categoryId, $photoUrl . '/' . $photoName);
        //

        return redirect()->back();
    }

    public function saveTopCategoryPhotos($categoryId, $photo, $photoName, $photoUrl)
    {
        Size::removeSizes();
        Size::newSize('small', 109, 109);
        Size::newSize('medium', 226, 226);
        Size::newSize('large', 400, 400);
        Size::newSize('xLarge', 600, 600);
        $sizeList = Size::getSizeList();
        $obj = (object)[];
        foreach ($sizeList as $size) {
            /*
                        $hashName = $photoUrl . '/' . $size->sizeName;
                        $path = $photo->hashName($hashName);

                        $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
                        Storage::put($path, (string)$img->encode());
            */
            $path = $photoUrl . '/' . $size->sizeName . '-' . $photoName;
            $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
            Storage::put($path, (string)$img->encode());
            $prop = $size->sizeName;
            $obj->$prop = $path;

        }
        TopCategory::updatePhotoName($categoryId, json_encode($obj));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($categoryId)
    {
        $topCategory = TopCategory::getCategory($categoryId);
        return view('topCategory.edit', compact('topCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categoryId)
    {
        $this->validate($request,
            [
                'topCategoryName' => 'required',
            ],
            [
                'topCategoryName.required' => 'Kategori adı boş bırakılamaz !',
            ]
        );


        $topCategoryName = $request->input('topCategoryName');

        $topCategory = TopCategory::getCategory($categoryId);
        $topCategory->categoryName = $topCategoryName;
        $topCategory->save();

        return redirect()->back()->with('message', 'Ürün başarıyla güncellendi !');

    }

    public function editPhoto($categoryId)
    {
        $topCategory = TopCategory::getCategory($categoryId);
        return view('topCategory.editPhoto', compact('topCategory'));
    }

    public function updatePhoto(Request $request, $categoryId)
    {
        $this->validate($request,
            [
                'topCategoryPhoto' => 'required|image'
            ],
            [
                'topCategoryPhoto.required' => 'Bir resim seçin.',
                'topCategoryPhoto.image' => 'Seçtiğiniz dosya bir resim dosyası değil. Lütfen bir resim dosyası seçin.'
            ]
        );

        $photo = $request->file('topCategoryPhoto');
        $photoUrl = 'categoryPhotos/' . $categoryId;
        $photoName = TopCategory::getCategory($categoryId)->categoryName . '-' .Carbon::now()->timestamp . '.' . $photo->extension();
        $photoName = CharacterTransform::transform($photoName);

        $this->saveTopCategoryPhotos($categoryId, $photo, $photoName, $photoUrl);

        return redirect()->back()->with('message', 'Kategori fotoğrafı başarıyla güncellendi !');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topCategory = TopCategory::getCategory($id);
        $topCategory->subCategories->products->barcodes()->delete();
        $topCategory->subCategories->products()->delete();
        $topCategory->subCategories()->delete();
        $topCategory->delete();
        return redirect()->back()->with('message', 'Temel kategori başarıyla silindi !');
    }
}
