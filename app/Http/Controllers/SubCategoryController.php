<?php

namespace App\Http\Controllers;

use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SubCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $topCategoryId)
    {
        $this->validate($request,
            [
                'subCategoryName' => 'required'
            ],
            [
                'subCategoryName.required' => 'Kategori adı boş birakilamaz.'
            ]
        );

        $categoryName = $request->input('subCategoryName');
        $photo = $request->file('photo');
        $categoryId = SubCategory::newSubCategory($topCategoryId, $categoryName);
        $photoUrl = 'categoryPhotos/' . $topCategoryId . '/sub/' . $categoryId;
        $photoName = $categoryName . '-' . Carbon::now()->timestamp . '.' . $photo->extension();
        $photoName = CharacterTransform::transform($photoName);
        $this->saveSubCategoryPhotos($categoryId, $photo, $photoName, $photoUrl);
        //$photoName = $categoryId . '.' . $photo->extension();
        //$photo->storeAs($photoUrl, $photoName);
        //SubCategory::updatePhotoName($categoryId, $photoUrl . '/' . $photoName);
        return redirect()->back();
    }

    public function saveSubCategoryPhotos($categoryId, $photo, $photoName, $photoUrl)
    {
        Size::removeSizes();
        Size::newSize('small', 109, 109);
        Size::newSize('medium', 226, 226);
        Size::newSize('large', 400, 400);
        Size::newSize('xLarge', 600, 600);
        $sizeList = Size::getSizeList();
        $obj = (object)[];
        foreach ($sizeList as $size) {
            /*
                        $hashName = $photoUrl . '/' . $size->sizeName;
                        $path = $photo->hashName($hashName);
                        $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
                        Storage::put($path, (string)$img->encode());
            */

            $path = $photoUrl . '/' . $size->sizeName . '-' . $photoName;
            $img = \Intervention\Image\Facades\Image::make($photo)->resize($size->x, $size->y);
            Storage::put($path, (string)$img->encode());

            $prop = $size->sizeName;
            $obj->$prop = $path;

        }
        SubCategory::updatePhotoName($categoryId, json_encode($obj));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($categoryId)
    {
        $subCategory = SubCategory::getCategory($categoryId);
        return view('subCategory.show', compact('subCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function editPhoto($categoryId)
    {
        $subCategory = SubCategory::getCategory($categoryId);
        return view('subCategory.editPhoto', compact('subCategory'));
    }

    public function updatePhoto(Request $request, $categoryId)
    {
        $this->validate($request,
            [
                'subCategoryPhoto' => 'required|image'
            ],
            [
                'subCategoryPhoto.required' => 'Bir resim seçin.',
                'subCategoryPhoto.image' => 'Seçtiğiniz dosya bir resim dosyası değil. Lütfen bir resim dosyası seçin.'
            ]
        );

        $photo = $request->file('subCategoryPhoto');
        $topCategoryId = SubCategory::getCategory($categoryId)->topCategory->id;
        $photoUrl = 'categoryPhotos/' . $topCategoryId . '/sub/' . $categoryId;
        $photoName = SubCategory::getCategory($categoryId)->categoryName . '-' . Carbon::now()->timestamp . '.' . $photo->extension();
        $photoName = CharacterTransform::transform($photoName);
        $this->saveSubCategoryPhotos($categoryId, $photo, $photoName, $photoUrl);

        return redirect()->back()->with('message', 'Kategori fotoğrafı başarıyla güncellendi !');
    }

    public function edit($categoryId)
    {
        $subCategory = SubCategory::getCategory($categoryId);
        return view('subCategory.edit', compact('subCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
