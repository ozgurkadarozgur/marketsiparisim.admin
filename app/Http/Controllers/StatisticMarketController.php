<?php

namespace App\Http\Controllers;

use App\Market;
use App\StatisticMarket;
use Illuminate\Http\Request;

class StatisticMarketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $markets = Market::getMarkets();
        return view('statistics.market.index', compact('markets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatisticMarket  $statisticMarket
     * @return \Illuminate\Http\Response
     */
    public function show($marketId)
    {
        $market = Market::getMarket($marketId);
        $totalOrderCount = StatisticMarket::getOrderCount($marketId);
        $userCountHasOrdered = StatisticMarket::getCountUserHasOrdered($marketId);
        $totalProductCount = StatisticMarket::getProductCount($marketId);
        $deniedOrderCount = StatisticMarket::getDeniedOrderCount($marketId);
        $canceledOrderCount = StatisticMarket::getCanceledOrderCount($marketId);
        $creditCartOrderCount = StatisticMarket::getCreditCartOrderCount($marketId);
        $cashOrderCount = StatisticMarket::getCashOrderCount($marketId);

        $statistic = (object)[
            'totalOrderCount'=>$totalOrderCount,
            'totalProductCount'=>$totalProductCount,
            'userCountHasOrdered'=>$userCountHasOrdered,
            'deniedOrderCount'=>$deniedOrderCount,
            'canceledOrderCount'=>$canceledOrderCount,
            'creditCartOrderCount'=> $creditCartOrderCount,
            'cashOrderCount'=> $cashOrderCount
        ];

        return view('statistics.market.show', compact('market','statistic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatisticMarket  $statisticMarket
     * @return \Illuminate\Http\Response
     */
    public function edit(StatisticMarket $statisticMarket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StatisticMarket  $statisticMarket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatisticMarket $statisticMarket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatisticMarket  $statisticMarket
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatisticMarket $statisticMarket)
    {
        //
    }
}
