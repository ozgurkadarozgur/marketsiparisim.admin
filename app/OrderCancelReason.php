<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCancelReason extends Model
{
    protected $table = 'orderCancelReasons';

    public static function getReasons()
    {
        return self::all();
    }

    public static function newReason($reason)
    {
        $cancelReason = new self();
        $cancelReason->reason = $reason;
        $cancelReason->save();
    }

}
