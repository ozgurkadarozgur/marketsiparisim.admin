<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = 'orderStatuses';

    public static $waitingForApproval = 1;
    public static $denied = 2;
    public static $approved = 3;
    public static $canceled = 4;
    public static $onWay = 5;
}
