<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    public $timestamps = true;

    public function subCategory()
    {
        return $this->belongsTo('App\SubCategory', 'subCategoryId');
    }

    public function barcodes()
    {
        return $this->hasMany('App\Barcode', 'productId');
    }

    public static function getProductByBarcode($barcode)
    {
        return self::join('barcodes', 'barcodes.productId', '=', 'products.id')
            ->where('barcodes.barcodeCode', '=', $barcode)
            ->first();
    }

    public static function sequenceNumber()
    {
        $count = Product::count();
        return $count + 1;
    }

    public static function getProducts()
    {
        return self::all();
    }

    public static function newProduct($productType = 1, $subCategoryId, $brandId, $productCode, $productName, $price, $barcode, $sequenceNumber)
    {
        $product = new Product();
        $product->subCategoryId = $subCategoryId;
        $product->brandId = $brandId;
        //$product->productCode = $productCode;
        $product->productName = $productName;
        //$product->price = $price;
        //$product->sequenceNumber = $sequenceNumber;
        $product->productType = $productType;
        $product->save();

        Barcode::newBarcode($product->id, $barcode);

        return $product->id;
    }

    public static function newGSProduct($productName, $subCategoryId, $photo, $barcode)
    {
        $product = new self();
        $product->productType = 1;
        $product->productName = $productName;
        $product->subCategoryId = $subCategoryId;
        $product->photo = $photo;
        $product->brandId = 9999;
        $product->save();
        $pid = $product->id;
        Barcode::newBarcode($pid, $barcode);
        return $pid;
    }

    public static function getProduct($productId)
    {
        return Product::find($productId);
    }

    public static function updatePhotoName($productId, $photo)
    {
        $product = Product::getProduct($productId);
        $product->photo = $photo;
        $product->save();
    }


}
