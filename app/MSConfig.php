<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSConfig extends Model
{
    protected $table = 'ms_config';

    protected static function getConfig()
    {
        return self::first();
    }

    public static function getMarketMoneyRate()
    {
        $config = self::getConfig();
        return $config->marketMoneyRate;
    }

    public static function getCommissionRate()
    {
        $config = self::getConfig();
        return $config->commissionRate;
    }

}
