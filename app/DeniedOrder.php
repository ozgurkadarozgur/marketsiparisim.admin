<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeniedOrder extends Model
{
    protected $table = 'deniedOrders';
}
