<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopCategory extends Model
{
    protected $table = 'topCategories';
    public $timestamps = true;

    public function subCategories()
    {
        return $this->hasMany('App\SubCategory', 'topCategoryId');
    }

    public static function sequenceNumber()
    {
        $count = TopCategory::count();
        return $count + 1;
    }

    public static function getAll()
    {
        return self::all();
    }

    public static function getCategory($categoryId)
    {
        return TopCategory::find($categoryId);
    }

    public static function newTopCategory($categoryName){
        $topCategory = new TopCategory();
        $topCategory->categoryName = $categoryName;
        $topCategory->sequenceNumber = TopCategory::sequenceNumber();
        $topCategory->save();
        return $topCategory->id;
    }

    public static function updatePhotoName($categoryId, $photo)
    {
        $topCategory = TopCategory::getCategory($categoryId);
        $topCategory->photo = $photo;
        $topCategory->save();
    }

}
