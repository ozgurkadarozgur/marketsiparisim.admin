<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class FavoriteProduct extends Model
{

    use SoftDeletes;

    protected $table = 'favoriteProducts';
    public $timestamps = true;
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::uuid1()->toString();
        });
    }

    public static function newFavoriteProduct($customerId, $stockId)
    {
        $favoriteProduct = new self();
        $favoriteProduct->customerId = $customerId;
        $favoriteProduct->stockId = $stockId;
        $favoriteProduct->save();
    }

    public static function getFavoriteProducts($customerId, $marketId)
    {
        //return self::where('customerId', $customerId)->get();

        $products = DB::table("favoriteProducts")
            ->join('stocks', 'stocks.id', '=', 'favoriteProducts.stockId')
            ->join('products', 'products.id', '=', 'stocks.productId')
            ->join('markets', 'stocks.marketId', '=', 'markets.id')
            ->where('favoriteProducts.customerId', "$customerId")
            ->where('markets.id', $marketId)
            ->whereNull('favoriteProducts.deleted_at')
            ->select([
                'stocks.id as id',
                'products.id as productId',
                'favoriteProducts.id as favoriteProductId',
                'products.productName',
                'products.photo',
                'stocks.price',
                'markets.marketName',
                'markets.id as marketId'
            ])
            ->get();

        return $products;
        /*
                $products = DB::select("SELECT favoriteProducts.id, products.productName, products.photo, stocks.price, markets.marketName FROM market.favoriteProducts
                            INNER JOIN stocks ON stocks.id = favoriteProducts.stockId
                            INNER JOIN products ON products.id = stocks.productId
                            INNER JOIN markets ON stocks.marketId = markets.id
                            WHERE favoriteProducts.customerId = '$customerId' AND favoriteProducts.deleted_at= NULL ");
                return $products;
        */
    }

    public static function destroyFavoriteProduct($customerId, $stockId)
    {
        self::where('customerId', $customerId)
            ->where('stockId', $stockId)
            ->delete();
    }

}
