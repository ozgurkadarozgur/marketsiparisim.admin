<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PhoneUpdateRequest extends Model
{
    protected $table = 'phoneUpdateRequests';
    public $timestamps = true;

    public static function newRequest($customerId, $newPhone, $code)
    {
        $phoneUpdateRequest = new self();
        $phoneUpdateRequest->customerId = $customerId;
        $phoneUpdateRequest->newPhone = $newPhone;
        $phoneUpdateRequest->code = $code;
        $phoneUpdateRequest->expires_at = Carbon::now()->addMinute(3)->toDateTimeString();
        $phoneUpdateRequest->save();
    }

    public static function completeUpdate($customerId, $newPhone)
    {
        $customer = CustomerUser::getCustomerUser($customerId);
        $customer->phone = $newPhone;
        $customer->save();
    }

    public static function getCodeForVerify($customerId)
    {
        return self::where('customerId', $customerId)
            ->orderBy('id', 'desc')
            ->first()
            ->code;
    }

    public static function getCodeData($customerId)
    {
        return self::where('customerId', $customerId)
            ->orderBy('id', 'desc')
            ->first();
    }

    public static function getTimeToVerify($customerId)
    {
        $codeData = self::getCodeData($customerId);

        $expires_at = $codeData->expires_at;
        $now = Carbon::now();

        $start = Carbon::parse($now);
        $end = Carbon::parse($expires_at);

        if($end > $start) return $start->diffInSeconds($end);
        else return 0;

    }

}
