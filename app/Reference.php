<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Reference extends Model
{

    use SoftDeletes;

    protected $table = 'references';


    public static function newReference($customerId, $referenceId)
    {
        $reference = new self();
        $reference->customerId = $customerId;
        $reference->referenceId = $referenceId;
        $reference->save();
    }

    public static function checkReference($customerId)
    {
        $reference = self::where('customerId', $customerId)
            ->orWhereNull('deleted_at')
            ->first();

        if ($reference) return true;
        else return false;
    }

    public static function destroyReference($customerId)
    {
        $reference = self::where('customerId', $customerId)
            ->whereNull('deleted_at')
            ->first();

        $reference->delete();

    }

}
