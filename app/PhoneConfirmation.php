<?php

namespace App;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class PhoneConfirmation extends Model
{
    protected $table = 'phoneConfirmations';
    public $timestamps = true;

    public static function newAttempt($customerId, $code)
    {
        $confirmation = new self();
        $confirmation->customerId = $customerId;
        $confirmation->code = $code;
        $confirmation->expires_at = Carbon::now()->addMinute(3)->toDateTimeString();
        $confirmation->save();
    }

    public static function getCustomerCodeForVerify($customerId)
    {
        return PhoneConfirmation::where('customerId', $customerId)
            ->orderBy('id', 'desc')
            ->first()
            ->code;
    }

    public static function getCodeData($customerId)
    {
        return PhoneConfirmation::where('customerId', $customerId)
            ->orderBy('id', 'desc')
            ->first();
    }

    public static function getTimeToVerify($customerId)
    {
        $codeData = self::getCodeData($customerId);

        $expires_at = $codeData->expires_at;
        $now = Carbon::now();

        $start = Carbon::parse($now);
        $end = Carbon::parse($expires_at);

        if($end > $start) return $start->diffInSeconds($end);
        else return 0;

    }

    public static function sendSMS($customerId, $phone, $code)
    {

        $customer = CustomerUser::getCustomerUser($customerId);
        //$phone = $customer->phone;

        $xml = new \SimpleXMLElement("<request>
        <authentication>
                <username>5318282332</username>
                <password>Ass122...</password>
        </authentication>
        <order>
                <sender>SIFIRDANBIR</sender>
                <sendDateTime></sendDateTime>
                <message>
                        <text>MarketSiparişim telefon doğrulama kodunuz: $code</text>
                        <receipents>
                                <number>$phone</number>                               
                        </receipents>
                </message>               
        </order>
</request>");

        $client = new Client();
        $response = $client->post('https://api.iletimerkezi.com/v1/send-sms', [
            'body' => $xml->asXML()
        ]);

        //self::newAttempt($customerId, $code);

    }

}
