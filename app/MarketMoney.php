<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class MarketMoney extends Model
{
    protected $table = 'marketMoney';
    public $timestamps = true;
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::uuid1()->toString();
        });
    }

    public static function newMarketMoney($customerId, $orderId, $money)
    {
        $mmRate = MSConfig::getMarketMoneyRate();
        $mm = $money * $mmRate;
        $marketMoney = new self();
        $marketMoney->customerId = $customerId;
        $marketMoney->orderId = $orderId;
        $marketMoney->money = $mm;
        $marketMoney->save();
    }

}
