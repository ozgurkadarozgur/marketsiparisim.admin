<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewProducts extends Model
{
    protected $table = 'new_products';
    public $timestamps = false;

    public static function getAll()
    {
        return self::all();
    }

}
