<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';

    public function neighborhoods()
    {
        return $this->hasMany(Neighborhood::class, 'NeighborhoodID', 'AreaID');
    }

    public static function getAreasByCountyId($countyId)
    {
        return self::where('CountyID', $countyId)
                ->get();
    }

}
