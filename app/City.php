<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    public function counties()
    {
        return $this->hasMany(County::class, 'CountyID', 'CityID');
    }

    public static function getCities()
    {
        return self::all();
    }

}
