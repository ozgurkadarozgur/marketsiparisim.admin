<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{
    protected $table = 'productUnits';
    public $timestamps = true;

    public static function newUnit($unitName)
    {
        $productUnit = new ProductUnit();
        $productUnit->unitName = $unitName;
        $productUnit->save();
    }

}
