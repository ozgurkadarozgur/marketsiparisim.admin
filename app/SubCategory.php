<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'subCategories';
    public $timestamps = true;

    public function topCategory()
    {
        return $this->belongsTo('App\TopCategory', 'topCategoryId');
    }

    public function products()
    {
        return $this->hasMany('App\Product', 'subCategoryId');
    }

    public static function sequenceNumber()
    {
        $count = SubCategory::count();
        return $count + 1;
    }

    public static function newSubCategory($topCategoryId, $categoryName)
    {
        $subCategory = new SubCategory();
        $subCategory->topCategoryId = $topCategoryId;
        $subCategory->categoryName = $categoryName;
        $subCategory->sequenceNumber = SubCategory::sequenceNumber();
        $subCategory->save();
        return $subCategory->id;
    }

    public static function getSubCategoryByTopCatId($topCategoryId)
    {
        return self::where('topCategoryId', $topCategoryId)
                ->get();
    }

    public static function getCategory($subCategoryId)
    {
        return SubCategory::find($subCategoryId);
    }

    public static function updatePhotoName($categoryId, $photo)
    {
        $category = SubCategory::getCategory($categoryId);
        $category->photo = $photo;
        $category->save();
    }

}
