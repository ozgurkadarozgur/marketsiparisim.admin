<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'productTypes';

    public static $standart = 1;
    public static $special = 2;

}
