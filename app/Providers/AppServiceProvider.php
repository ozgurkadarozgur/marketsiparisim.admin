<?php

namespace App\Providers;

use App\CustomerUser;
use App\PhoneConfirmation;
use App\PhoneConfirmationStatus;
use App\PhoneUpdateRequest;
use App\Stock;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //shoppingCartController@newShoppingCart
        Validator::extend('product_in_stock', function ($attribute, $value, $parameters, $validator) {
            /*
            $stockId = $value;
            $customerId = $parameters[0];
            $orderedCount = Stock::getOrderedCount($stockId);
            $totalCount = Stock::getTotalCountInStock($stockId);
            $remaining = $totalCount - $orderedCount;
            $totalCountInShoppingCart = Stock::getTotalCountInShoppingCart($customerId, $stockId);
            if ($remaining - $totalCountInShoppingCart > 0) return true;
            else return false;
            */
            $stockId = $value;
            $customerId = $parameters[0];
            $totalCount = Stock::getTotalCountInStock($stockId);
            $totalCountInShoppingCart = Stock::getTotalCountInShoppingCart($customerId, $stockId);
            if ($totalCount - $totalCountInShoppingCart > 0) return true;
            else return false;
        });

        Validator::extend('match_phone_code', function ($attribute, $value, $parameters, $validator) {
            $code = $value;
            $codeForVerify = $parameters[0];
            if ($code == $codeForVerify) return true;
            else return false;
        });

        Validator::extend('match_with_old_password', function ($attribute, $value, $parameters, $validator) {
            $password = $value;
            $customerId = $parameters[0];
            $oldPassword = CustomerUser::getCustomerUser($customerId)->password;
            if (Hash::check($password, $oldPassword)) return true;
            else return false;
        });

        Validator::extend('phone_confirmed', function ($attribute, $value, $parameters, $validator) {
            $customerId = $value;
            $status = CustomerUser::getCustomerUser($customerId)->phoneConfirmationStatus;
            if ($status == 1) return true;
            else return false;
        });

        Validator::extend('not_expired_upc', function ($attribute, $value, $parameters, $validator){
            $customerId = $parameters[0];
            $time = PhoneUpdateRequest::getTimeToVerify($customerId);
            if($time == 0) return false;
            else return true;
        });

        Validator::extend('not_expired_pc', function ($attribute, $value, $parameters, $validator){
            $customerId = $parameters[0];
            $time = PhoneConfirmation::getTimeToVerify($customerId);
            if($time == 0) return false;
            else return true;
        });

        Validator::extend('match_with_old_admin_password', function ($attribute, $value, $parameters, $validator) {
            $password = $value;
            $oldPassword = User::find(1)->password;
            if (Hash::check($password, $oldPassword)) return true;
            else return false;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
