<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    protected $table = 'markets';
    public $timestamps = true;

    public function user()
    {
        return $this->hasOne(MarketUser::class, 'marketId');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'cityId', 'CityID');
    }

    public function county()
    {
        return $this->belongsTo(County::class, 'countyId', 'CountyID');
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'areaId', 'AreaID');
    }

    public function neighborhood()
    {
        return $this->belongsTo(Neighborhood::class, 'neighborhoodId', 'NeighborhoodID');
    }

    public static function newMarket($marketName, $inCharge, $latitude, $longitude, $city, $county, $area, $neighborhood)
    {
        $market = new Market();
        $market->marketName = $marketName;
        $market->inCharge = $inCharge;
        $market->latitude = $latitude;
        $market->longitude = $longitude;
        $market->cityId = $city;
        $market->countyId = $county;
        $market->areaId = $area;
        $market->neighborhoodId = $neighborhood;
        $market->save();
    }

    public static function getMarket($marketId)
    {
        return Market::find($marketId);
    }

    public static function getMarkets()
    {
        return Market::paginate(20);
    }

    public static function getAllMarkets()
    {
        return self::all();
    }

    public static function getMarketCount()
    {
        return self::count();
    }

}
