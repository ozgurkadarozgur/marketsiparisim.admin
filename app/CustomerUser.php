<?php

namespace App;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class CustomerUser extends Authenticatable
{
    use HasApiTokens, Notifiable, CanResetPassword;

    protected $table = 'customerUsers';
    public $timestamps = true;

    public function reference()
    {
        return $this->hasOne(Reference::class, 'customerId','id');
    }

    public static function register($name, $email, $password, $phone)
    {
        $customer = new self();
        $customer->name = $name;
        $customer->email = $email;
        $customer->password = bcrypt($password);
        $customer->phone = $phone;
        $customer->save();
        return $customer->id;
    }

    public static function updatePassword($customerId, $password)
    {
        $customer = self::getCustomerUser($customerId);
        $customer->password = bcrypt($password);
        $customer->save();
    }

    public static function updatePhone($customerId, $newPhone)
    {
        $customer = self::getCustomerUser($customerId);
        $customer->phone = $newPhone;
        $customer->save();
    }

    public static function updateName($customerId, $name)
    {
        $customer = self::getCustomerUser($customerId);
        $customer->name = $name;
        $customer->save();
    }

    public static function getCustomerUserByEmail($email)
    {
        return self::where('email', $email)
                ->first();
    }

    public static function getCustomerUser($id)
    {
        return self::find($id);
    }

    public static function getCustomerUsers()
    {
        return self::paginate(15);
    }

    public static function getCustomerInfo($customerId)
    {
        $user = DB::select("SELECT name, email, getCustomerMarketMoney($customerId) as marketMoney from customerUsers WHERE id=$customerId");
        return $user[0];
    }

    public static function getCustomerCount()
    {
        return self::count();
    }

    public static function getCustomerByPhone($phone)
    {
        return self::where('phone', $phone)
                ->first();
    }

    public static function changeNotificationStatus($customerId, $status)
    {
        $customer = self::getCustomerUser($customerId);
        $customer->notificationRequest = $status;
        $customer->save();
    }

    public static function hasReference($customerId)
    {
        $customer = Reference::where('customerId', $customerId)
            ->first();
        if($customer) return $customer->referenceId;
        else return false;
    }

}
