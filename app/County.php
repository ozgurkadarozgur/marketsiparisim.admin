<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $table = 'counties';

    public function areas()
    {
        return $this->hasMany(Neighborhood::class, 'AreaID', 'CountyID');
    }

    public static function getCountiesByCityId($cityId)
    {
        return self::where('CityId', $cityId)
                ->get();
    }

}
