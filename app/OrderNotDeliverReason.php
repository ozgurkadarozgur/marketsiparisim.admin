<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderNotDeliverReason extends Model
{
    protected $table = 'orderNotDeliverReasons';

    public static function getReasons()
    {
        return self::all();
    }

    public static function newReason($reason)
    {
        $denyReason = new self();
        $denyReason->reason = $reason;
        $denyReason->save();
    }

}
