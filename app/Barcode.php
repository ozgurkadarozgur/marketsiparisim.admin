<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barcode extends Model
{
    protected $table = 'barcodes';
    public $timestamps = true;

    public static function newBarcode($productId, $barcodeCode)
    {
        $barcode = new Barcode();
        $barcode->productId = $productId;
        $barcode->barcodeCode = $barcodeCode;
        $barcode->save();
    }

}
