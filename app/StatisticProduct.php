<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StatisticProduct extends Model
{
    public static function getMostOrderedProducts()
    {
        /*

        select products.productName, sum(orderDetails.count) from orderDetails
        inner join stocks on stocks.id = orderDetails.productId
        inner join products on products.id = stocks.productId
        group by orderDetails.productId

         */

        return OrderDetail::join('stocks', 'stocks.id', '=', 'orderDetails.productId')
            ->join('products', 'products.id', '=', 'stocks.productId')
            ->groupBy('orderDetails.productId')
            ->select([
                'products.productName',
                DB::raw('sum(orderDetails.count) as count'),
                DB::raw('stocks.price * sum(orderDetails.count) as price')
            ])
            ->orderBy('count', 'desc')
            ->get();

    }
}
