<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    public $timestamps = true;

    public function products()
    {
        return $this->hasMany('App\Product', 'brandId');
    }

    public static function getBrand($brandId)
    {
        return Brand::find($brandId);
    }

    public static function getBrands()
    {
        return Brand::all();
    }

    public static function newBrand($brandName)
    {
        $brand = new Brand();
        $brand->brandName = $brandName;
        $brand->save();
    }

    public static function updateBrand($brandId, $brandName)
    {
        $brand = Brand::getBrand($brandId);
        $brand->brandName = $brandName;
        $brand->save();
    }

}
