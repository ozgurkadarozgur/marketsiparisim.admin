<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceType extends Model
{
    protected $table = 'invoiceType';
    public $timestamps = false;

    public static function getTypes()
    {
        return self::all();
    }

}
