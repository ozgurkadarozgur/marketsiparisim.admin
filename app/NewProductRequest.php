<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewProductRequest extends Model
{
    protected $table = 'newProductRequests';
    public $timestamps = true;

    public function status()
    {
        return $this->belongsTo(NewProductRequestStatuses::class, 'statusId', 'id');
    }

    public function market()
    {
        return $this->belongsTo(Market::class, 'marketId', 'id');
    }

    public static function getProductRequests()
    {
        $productRequests = self::orderBy('created_at', 'desc')->get();
        return $productRequests;
    }

    public static function getRequestedProduct($id)
    {
        return self::find($id);
    }

    public static function changeStatus($id, $status)
    {
        $product = self::getRequestedProduct($id);
        $product->statusId = $status;
        $product->save();
    }

    public static function saveAsStock($id, $subCatId)
    {
        $product = self::getRequestedProduct($id);
        $productName = $product->productName;
        $price = $product->price;
        $photo = $product->productPhoto;
        $newProductId = Product::newProduct(2, $subCatId, 1, 'ad123123', $productName, $price, '12312312', 2);
        Product::updatePhotoName($newProductId, $photo);
        return $newProductId;
    }

}
