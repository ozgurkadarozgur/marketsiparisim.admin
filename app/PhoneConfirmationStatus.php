<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneConfirmationStatus extends Model
{

    public static $verified = 1;
    public static $unVerified = 2;


    public static function verify($customerId)
    {
        $customer = CustomerUser::getCustomerUser($customerId);
        $customer->phoneConfirmationStatus = self::$verified;
        $customer->save();
    }

    public static function check($customerId)
    {
        $customer = CustomerUser::getCustomerUser($customerId);
        $status = $customer->phoneConfirmationStatus;
        if($status == 1) return true;
        else return false;
    }

}
