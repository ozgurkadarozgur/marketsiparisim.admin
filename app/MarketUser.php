<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketUser extends Model
{
    protected $table = 'marketUsers';
    public $timestamps = true;


    public static function getMarketUserByMarketId($marketId)
    {
        return self::where('marketId', $marketId)
            ->first();
    }

    public static function setMarketUser($marketId, $name, $username, $email, $password)
    {
        $marketUser = self::getMarketUserByMarketId($marketId);
        if ($marketUser) self::updateMarketUser($marketId, $name, $username, $email, $password);
        else self::newMarketUser($marketId, $name, $username, $email, $password);
    }

    public static function newMarketUser($marketId, $name, $username, $email, $password)
    {
        $marketUser = new MarketUser();
        $marketUser->marketId = $marketId;
        $marketUser->name = $name;
        $marketUser->username = $username;
        $marketUser->email = $email;
        $marketUser->password = bcrypt($password);
        $marketUser->save();
    }

    public static function updateMarketUser($marketId, $name, $username, $email, $password)
    {
        $marketUser = self::getMarketUserByMarketId($marketId);
        $marketUser->name = $name;
        $marketUser->username = $username;
        $marketUser->email = $email;
        $marketUser->password = bcrypt($password);
        $marketUser->save();
    }

    public static function getMarketByEmail($email)
    {
        $market = self::where('email', $email)
            ->first();
        if ($market) return $market;
        else return false;
    }

}