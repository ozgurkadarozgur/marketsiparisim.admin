<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StatisticCustomer extends Model
{

    public static function getCreditCartOrderCount($customerId)
    {
        return Order::where('customerId', $customerId)
            ->where('paymentMethodId', PaymentMethod::$creditCart)
            ->count();
    }

    public static function getCashOrderCount($customerId)
    {
        return Order::where('customerId', $customerId)
            ->where('paymentMethodId', PaymentMethod::$cash)
            ->count();
    }

    public static function getReferenceCount($customerId)
    {
        return Reference::where('referenceId', $customerId)
            ->count();
    }

    public static function getOrderPriceByPaymentMethod($customerId, $paymentMethod)
    {
        $sum = OrderDetail::join('orders', 'orders.id','=', 'orderDetails.orderId')
            ->where('orders.customerId', '=', $customerId)
            ->where('orders.paymentMethodId', '=', $paymentMethod)
            ->select(DB::raw("sum(orderDetails.unitPrice * orderDetails.count) AS sum"))
            ->first();

        return $sum->sum;


    }

}
