<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    protected $table = 'neighborhood';

    public static function getNeighborhoodsByAreaId($areaId)
    {
        return self::where('AreaID', $areaId)
            ->get();
    }

}
