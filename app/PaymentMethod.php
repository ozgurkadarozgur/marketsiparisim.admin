<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = 'paymentMethods';

    public static $cash = 1;
    public static $creditCart = 2;
    public static $marketMoney = 3;

}
