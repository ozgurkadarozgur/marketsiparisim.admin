<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewProductRequestStatuses extends Model
{
    public $table = 'newProductRequestStatuses';

    public static $waitingForApproval = 1;
    public static $approved = 2;
    public static $canceled = 3;
}
