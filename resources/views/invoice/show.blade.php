@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{$market->marketName}} Cari Bilgileri</h3>
            <a href="{{route('createBill', $market->id)}}" class="btn btn-primary">Fatura Kes</a>
        </div>

        <div class="box-body">
            @foreach($invoices as $invoice)

                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>{{$invoice->startDate . ' - ' . $invoice->endDate}}</th>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Toplam Tutar</th>
                        <th>MS Komisyonu</th>
                        <th>Market Kazancı</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($invoice->details as $detail)
                            <tr>
                                <td>{{$detail->paymentMethod->method}}</td>
                                <td>{{$detail->total . ' TL'}}</td>
                                <td>{{$detail->total * $invoice->commission . ' TL'}}</td>
                                <td>{{$detail->total - ($detail->total * $invoice->commission) . ' TL'}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <?php $totalCommission = $invoice->total * $invoice->commission?>
                            <td></td>
                            <td>{{$invoice->total . ' TL'}}</td>
                            <td>{{$totalCommission . ' TL'}}</td>
                            <td>{{$invoice->total - $totalCommission . ' TL'}}</td>
                        </tr>
                    </tbody>
                </table>

            @endforeach
        </div>
    </div>
@endsection