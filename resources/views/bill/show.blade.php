@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Fatura No: {{$bill->billNo}}</h3>
        </div>

        <div class="box-body">
            <table class="table table-responsive table-bordered">
                <tr>
                    <th>İşlem Türü</th>
                    <td>{{$bill->invoiceType->type}}</td>
                </tr>
                <tr>
                    <th>Fatura Tarihi</th>
                    <td>{{$bill->created_at}}</td>
                </tr>
                <tr>
                    <th>Fatura Tutarı</th>
                    <td>{{$bill->price . ' TL'}}</td>
                </tr>
            </table>
        </div>

    </div>

@endsection