@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{$market->marketName}} | Faturalar</h3>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fatura No</th>
                        <th>Tutar</th>
                        <th>İşlem Türü</th>
                        <th>Fatura Tarihi</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bills as $bill)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$bill->billNo}}</td>
                            <td>{{$bill->price . ' TL'}}</td>
                            <td>{{$bill->invoiceType->type}}</td>
                            <td>{{$bill->created_at}}</td>
                            <td><a href="{{route('showBill', $bill->id)}}">İncele</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection