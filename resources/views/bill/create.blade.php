@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">{{session()->get('message')}}</div>
    @endif

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{$market->marketName}}</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{route('storeBill', $market->id)}}">
                {{csrf_field()}}
                <table class="table table-bordered table-responsive">
                    <tr>
                        <td>İşlem Türü</td>
                        <td>
                            <select class="form-control" name="invoiceType">
                                @foreach($invoiceTypes as $invoiceType)
                                    <option value="{{$invoiceType->id}}">{{$invoiceType->type}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Fatura Tutarı</td>
                        <td>
                            <input class="form-control" name="price"/>
                            @if($errors->has('price'))
                                <strong style="color: red;">{{$errors->first('price')}}</strong>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Fatura No</td>
                        <td>
                            <input class="form-control" name="billNo"/>
                            @if($errors->has('billNo'))
                                <strong style="color: red;">{{$errors->first('billNo')}}</strong>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" class="btn btn-primary pull-right" value="Kaydet"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

@endsection