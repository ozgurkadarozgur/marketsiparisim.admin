@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Yeni Ürün Talepleri</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th>Market Adı</th>
                    <th>Ürün Adı</th>
                    <th>Ürün Açıklaması</th>
                    <th>Fiyat</th>
                    <th>Tarih</th>
                    <th></th>
                </tr>
                @foreach($productRequests as $productRequest)
                    <tr>
                        <td>{{$productRequest->market->marketName}}</td>
                        <td>{{$productRequest->productName}}</td>
                        <td>{{str_limit($productRequest->productDescription, 45, '...')}}</td>
                        <td>{{$productRequest->price. ' TL'}}</td>
                        <td>{{$productRequest->created_at}}</td>
                        <td><a href="{{route('showRequestedProduct', $productRequest->id)}}">İncele</a></td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>

@endsection