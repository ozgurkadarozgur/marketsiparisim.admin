@extends('layouts.app')

@section('content')

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Kategori Seçin</h4>
                </div>
                <form method="post" action="{{route('approveRequestedProduct', $requestedProduct->id)}}">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>Temel Kategori</th>
                                <td>
                                    <select id="topCategories" name="topCatId" class="form-control">
                                        @foreach($topCategories as $topCategory)
                                            <option value="{{$topCategory->id}}">{{$topCategory->categoryName}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Alt Kategori</th>
                                <td>
                                    <select id="subCategories" name="subCatId" class="form-control">
                                        @foreach($subCategories as $subCategory)
                                            <option value="{{$subCategory->id}}">{{$subCategory->categoryName}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        </table>

                    </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary"  value="Kaydet"/>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <?php
    $photo = json_decode($requestedProduct->productPhoto);
    ?>

    @if(session()->has('message'))
        <div class="alert alert-warning">
            <strong>Uyarı!</strong> {{session()->get('message')}}
        </div>
    @endif

    <div class="row">
        <div class="col-md-4">
            <img width="250" height="250" src="{{env('PHOTO_URL'). Storage::url('app/'.$photo->small)}}">
        </div>
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Tarih {{$requestedProduct->created_at . ' - ' . $requestedProduct->status->status}}</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Market Adı</th>
                            <td>{{$requestedProduct->market->marketName}}</td>
                        </tr>
                        <tr>
                            <th>Ürün Adı</th>
                            <td>{{$requestedProduct->productName}}</td>
                        </tr>
                        <tr>
                            <th>Ürün Açıklaması</th>
                            <td>{{$requestedProduct->productDescription}}</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="pull-right">
                                    <a href="{{route('cancelRequestedProduct', $requestedProduct->id)}}"
                                       class="btn btn-danger">İptal Et</a>
                                    <button type="button" class="btn btn-success" data-toggle="modal"
                                            data-target="#myModal">Onayla
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('scripts')

    <script>

        $('#topCategories').change(function () {
            let topCatId = $(this).val();
            let token = ('{{csrf_token()}}');
            let url = ('{{route('getSubCategories')}}');
            $('#subCategories').html('');
            $.ajax({
                method: 'post',
                url: url,
                data: {_token: token, topCatId: topCatId}
            }).done(function (data) {
                $.each(data, function (index, item) {
                    $('#subCategories').append('<option value="' + item.id + '">' + item.categoryName + '</option>');
                });
            });
        });
    </script>

@endsection