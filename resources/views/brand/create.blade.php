@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Marka Ekle</h3> | <a href="{{env('APP_URL').'Brands'}}">Markaları Görüntüle</a>
        </div>
        <!-- /.box-header -->
        @if(session()->has('message'))
            <div class="alert alert-success">
                <strong>{{session()->get('message')}}</strong>
            </div>
    @endif
    <!-- form start -->

        <form class="form-horizontal" method="post"
              action="{{env('APP_URL').'storeBrand'}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">

                <div class="form-group{{ $errors->has('brandName') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Marka Adı</label>

                    <div class="col-sm-10">
                        <input type="text" name="brandName" class="form-control" value="{{old('brandName')}}"
                               placeholder="Marka Adı">
                        @if($errors->has('brandName'))
                            <span class="help-block">
                        <strong>{{ $errors->first('brandName') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>


                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Ekle</button>
                </div>
                <!-- /.box-footer -->

            </div>
        </form>

    </div>

@endsection