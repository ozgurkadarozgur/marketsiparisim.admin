@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Markalar</h3>&nbsp;| <a href="{{env('APP_URL').'NewBrand'}}">Marka Ekle</a>
                    <!-- /.box-header -->
                </div>
                <!-- /.box -->

                <div class="box-body">
                    <table class="table table-bordered">
                        @foreach($brands as $brand)
                            <tr>
                                <td>{{$brand->brandName}}</td>
                                <td><a href="{{env('APP_URL').'Brand/'.$brand->id.'/Edit'}}">Düzenle</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!--/.row -->
    </div>

@endsection