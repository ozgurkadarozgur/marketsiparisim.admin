<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ADMIN PANEL</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{env('APP_URL').'tema/'}}bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{env('APP_URL').'tema/'}}plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{env('APP_URL').'tema/'}}plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{env('APP_URL').'tema/'}}dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{env('APP_URL').'tema/'}}dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="{{env('APP_URL')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>ADMIN</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="" class="user-image" alt="User Image">
                            <span class="hidden-xs"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="" class="img-circle" alt="User Image">
                            </li>
                            <!-- Menu Body -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{route('settings')}}" class="btn btn-default">Ayarlar</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Çıkış Yap
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header text-center"></li>

                <!-- Ana Sayfa-->
                <li>
                    <a href="{{env('APP_URL')}}">
                        <i class="glyphicon glyphicon-home"></i> <span>Ana Sayfa</span>
                    </a>
                </li>
                <!-- //Ana Sayfa-->

                <!-- Kategoriler-->
                <li>
                    <a href="{{env('APP_URL').'TopCategories'}}">
                        <i class="fa fa-th"></i> <span>Kategoriler</span>
                    </a>
                </li>
                <!-- //Kategoriler-->

                <!-- Marketler-->
                <li>
                    <a href="{{env('APP_URL').'Markets'}}">
                        <i class="fa fa-th"></i> <span>Marketler</span>
                    </a>
                </li>
                <!-- //Marketler-->

                <!-- Markalar-->
                <li>
                    <a href="{{env('APP_URL').'Brands'}}">
                        <i class="fa fa-th"></i> <span>Markalar</span>
                    </a>
                </li>
                <!-- //Markalar-->

                <!-- Bildirim Gönder-->
                <li>
                    <a href="{{route('notificationPlaning')}}">
                        <i class="fa fa-th"></i> <span>Bildirim Gönder</span>
                    </a>
                </li>
                <!-- //Bildirim  Gönder-->

                <!-- Ürün Ara-->

                <li>
                    <a href="{{route('searchProduct')}}">
                        <i class="fa fa-th"></i> <span>Ürün Ara</span>
                    </a>
                </li>

                <!-- //Ürün Ara-->

                <!-- Ürün Birimleri-->
                <li>
                    <a href="{{env('APP_URL').'ProductUnits'}}">
                        <i class="fa fa-th"></i> <span>Ürün Birimleri</span>
                    </a>
                </li>
                <!-- //Ürün Birimleri-->

                <!-- Sipariş Reddetme Nedenleri-->
                <li>
                    <a href="{{route('orderDenyReasons')}}">
                        <i class="fa fa-th"></i> <span>Sipariş Reddetme Nedenleri</span>
                    </a>
                </li>
                <!-- //Sipariş Reddetme Nedenleri-->

                <!-- Sipariş Reddetme Nedenleri-->
                <li>
                    <a href="{{route('orderCancelReasons')}}">
                        <i class="fa fa-th"></i> <span>Sipariş İptal Etme Nedenleri</span>
                    </a>
                </li>
                <!-- //Sipariş Reddetme Nedenleri-->

                <!-- Sipariş Teslim Etmeme Nedenleri-->
                <li>
                    <a href="{{route('orderNotDeliverReasons')}}">
                        <i class="fa fa-th"></i> <span>Sipariş Teslim Etmeme Nedenleri</span>
                    </a>
                </li>
                <!-- //Sipariş Teslim Etmeme Nedenleri-->

                <!-- Yeni Ürün Talepleri-->
                <li>
                    <a href="{{route('newProductRequests')}}">
                        <i class="fa fa-th"></i> <span>Fırsat Ürünü Talepleri</span>
                    </a>
                </li>
                <!-- //Yeni Ürün Talepleri-->

                <!-- Yeni Ürün Talepleri-->
                <li>
                    <a href="{{route('marketMap')}}">
                        <i class="fa fa-th"></i> <span>Market Haritası</span>
                    </a>
                </li>
                <!-- //Yeni Ürün Talepleri-->

                <!-- İstatistikler -->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>İstatistikler</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('showMarketsForStatistic')}}"><i class="fa fa-circle-o"></i> Market İstatistikleri</a></li>
                        <li><a href="{{route('showCustomersForStatistic')}}"><i class="fa fa-circle-o"></i> Kullanıcı İstatistikleri</a></li>
                        <li><a href="{{route('showProductsForStatistic')}}"><i class="fa fa-circle-o"></i> Ürün İstatistikleri</a></li>
                    </ul>
                </li>
                <!-- //İstatistikler -->


            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ADMIN
                <small>Kontrol Paneli</small>
            </h1>
            {{--<ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>--}}
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">

    </footer>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
{{--<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>--}}

<!-- jQuery 2.2.3 -->
<script src="{{env('APP_URL').'tema/'}}plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{env('APP_URL').'tema/'}}bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="{{env('APP_URL').'tema/'}}plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{env('APP_URL').'tema/'}}dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="{{env('APP_URL').'tema/'}}plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{env('APP_URL').'tema/'}}plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{env('APP_URL').'tema/'}}plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{env('APP_URL').'tema/'}}plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{env('APP_URL').'tema/'}}plugins/chartjs/Chart.min.js"></script>
<!-- Select2 -->
<script src="{{env('APP_URL').'tema/'}}plugins/select2/select2.full.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes)
<script src="{{env('APP_URL').'tema/'}}dist/js/pages/dashboard2.js"></script>
-->
<script src="{{env('APP_URL')}}tema/dist/js/grafik/chart.js"></script>
<script src="{{env('APP_URL')}}tema/dist/js/grafik/donutChart.js"></script>
<!-- bootstrap datepicker -->
<script src="{{env('APP_URL')}}tema/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{env('APP_URL').'tema/'}}dist/js/demo.js"></script>
<script src="{{env('APP_URL')}}tema/dist/js/confirmation.js"></script>
@yield('scripts')
</body>
</html>
