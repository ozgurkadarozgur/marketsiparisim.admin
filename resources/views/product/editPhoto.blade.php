@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$product->productName}} | Resim Değiştir</h3>&nbsp;
                    <!-- /.box-header -->
                </div>

                <div class="box-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <strong>{{session()->get('message')}}</strong>
                        </div>
                    @endif
                    <form action="{{env('APP_URL').'Product/'.$product->id.'/UpdatePhoto'}}" method="post"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="form-group{{ $errors->has('productPhoto') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Resim</label>
                            <?php
                            if($product->photo)
                                $photo = json_decode($product->photo)->small;
                            else $photo = '';
                            ?>
                            <div class="col-sm-10">
                                <img id="productPhoto" class="img img-responsive img-thumbnail"
                                     src="{{env('PHOTO_URL').Storage::url('app/'.$photo)}}"
                                     style="cursor: pointer; min-width: 100px; min-height: 100px;">
                                <input id="inputProductPhoto" name="productPhoto" type="file"
                                       class="form-control"
                                       style="display: none;"/>
                                @if($errors->has('productPhoto'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('productPhoto') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="box-footer pull-right">

                            <input type="submit" class="btn btn-primary" value="Kaydet"/>
                            <a href="{{env('APP_URL').'Product/'.$product->id}}" class="btn btn-danger">İptal</a>

                        </div>

                    </form>

                </div>

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>

@endsection

@section('scripts')

    <script>

        $("#productPhoto").click(function () {
            $("#inputProductPhoto").trigger('click');
        });

    </script>

@endsection
