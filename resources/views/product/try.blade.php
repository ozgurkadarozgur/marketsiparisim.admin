@extends('layouts.app')

@section('content')

    <form method="post" enctype="multipart/form-data" action="{{env('APP_URL').'urunEkle'}}">
        {{csrf_field()}}

        <input type="file" name="photo" />
        <input type="submit" />

    </form>

@endsection