<br />
<div class="row">
    <div class="col-md-3">
    <?php
    if ($product->photo)
        $photo = json_decode($product->photo)->medium;
    else $photo = '';
    ?>
    <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img id="productPhoto" class="profile-user-img img-responsive img-circle"
                     src="{{env('PHOTO_URL'). Storage::url('app/'.$photo)}}" alt="Product picture">
                <h3 class="profile-username text-center">{{$product->productName}}</h3>

                <p class="text-muted text-center"><a href="{{env('APP_URL').'Product/'.$product->id.'/EditPhoto'}}">Resim
                        Değiştir</a></p>
                <p class="text-muted text-center">{{$product->subCategory->categoryName}}</p>
                <a class="btn btn-danger btn-block" data-popout="true" data-toggle="confirmation"
                   data-title="Emin misin?"
                   href="{{env('APP_URL').'Product/'.$product->id.'/Destroy'}}">Sil</a>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#product" data-toggle="tab">Ürün Bilgileri</a></li>
                <li><a href="#details" data-toggle="tab">Ürün Detayları</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="product">
                    <form method="post" action="{{env('APP_URL').'Product/'.$product->id.'/Update'}}"
                          class="form-group{{ $errors->has('topCategoryPhoto') ? ' has-error' : '' }}">
                        {{csrf_field()}}
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <strong>{{session()->get('message')}}</strong>
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <tr>
                                <th>Ürün Kodu</th>
                                <td>
                                    <input type="text" name="productCode" class="form-control"
                                           value="{{$product->productCode}}"/>
                                    @if($errors->has('productCode'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('productCode') }}</strong>
                    </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Ürün Adı</th>
                                <td>
                                    <input type="text" name="productName" class="form-control"
                                           value="{{$product->productName}}"/>
                                    @if($errors->has('productName'))
                                        <span class="help-block">
                        <strong>{{ $errors->first('productName') }}</strong>
                    </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Barkod</th>
                                <td>
                                    @foreach($product->barcodes as $barcode)
                                        <p>{{$barcode->barcodeCode}}</p>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="submit" class="btn btn-primary pull-right" value="Kaydet">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="details">
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

