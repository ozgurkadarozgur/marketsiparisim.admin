@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{$subCategory->categoryName}} | <a href="{{env('APP_URL').'SubCategory/'.$subCategory->id}}">Görüntüle</a> | Ürün Ekle</h3>
        </div>
        <!-- /.box-header -->
        @if(session()->has('message'))
            <div class="alert alert-success">
                <strong>{{session()->get('message')}}</strong>
            </div>
        @endif
    <!-- form start -->

        <form class="form-horizontal" method="post"
              action="{{env('APP_URL').'SubCategory/'.$subCategory->id.'/StoreProduct'}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">

                <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Resim</label>

                    <div class="col-sm-10">
                        <input type="file" name="photo" class="form-control" placeholder="Resim">
                        @if($errors->has('photo'))
                            <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('brand') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Marka</label>

                    <div class="col-sm-10">
                        <select id="brandList" name="brandId" class="form-control select2" style="width: 100%;">
                            @foreach($brands as $brand)
                                <option value="{{$brand->id}}">{{$brand->brandName}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('brand'))
                            <span class="help-block">
                        <strong>{{ $errors->first('brand') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('productCode') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Ürün Kodu</label>

                    <div class="col-sm-10">
                        <input type="text" name="productCode" class="form-control" value="{{old('productCode')}}"
                               placeholder="Ürün Kodu">
                        @if($errors->has('productCode'))
                            <span class="help-block">
                        <strong>{{ $errors->first('productCode') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('productName') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Ürün Adı</label>

                    <div class="col-sm-10">
                        <input type="text" name="productName" class="form-control" value="{{old('productName')}}"
                               placeholder="Ürün Adı">
                        @if($errors->has('productName'))
                            <span class="help-block">
                        <strong>{{ $errors->first('productName') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Fiyat</label>

                    <div class="col-sm-10">
                        <input type="text" name="price" class="form-control" value="{{old('price')}}"
                               placeholder="Fiyat">
                        @if($errors->has('price'))
                            <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="barcodeList">
                    <div class="form-group{{ $errors->has('barcode') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Barkod</label>

                        <div class="col-sm-10">
                            <input type="text" name="barcode" class="form-control" placeholder="Barkod">
                            @if($errors->has('barcode'))
                                <span class="help-block">
                        <strong>{{ $errors->first('barcode') }}</strong>
                    </span>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="form-group{{ $errors->has('barcode[]') ? ' has-error' : '' }}">
                    {{--<a href="#" id="newBarcode">Yeni Barkod Ekle</a>--}}
                    <div class="col-sm-10">

                    </div>

                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Ekle</button>
                </div>
                <!-- /.box-footer -->

            </div>
        </form>

    </div>

@endsection

@section('scripts')
    <script>
        $("#brandList").select2();
       /* $('#newBarcode').click(function () {
            var barcode = '<div class="form-group">' +
                '<label class="col-sm-2 control-label">Barkod</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" name="barcode[]" class="form-control" placeholder="Barkod">' +
                '</div></div>';

            var barcodeList = $('.barcodeList');
            barcodeList.append(barcode);

        });*/

    </script>
@endsection