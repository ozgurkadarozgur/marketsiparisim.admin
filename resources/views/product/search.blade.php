@extends('layouts.app')

@section('content')

<div class="row">
<div class="col-md-8">
    <form action="{{route('findProductByBarcode')}}" method="post">
        {{csrf_field()}}

        <input type="text" name="barcode" class="form-control" />
        <br />
        <input type="submit" value="Ara" class="btn btn-primary pull-right">

    </form>

</div>
</div>


    @if(session()->has('product'))

        @if(session()->get('product') == 'none')
            <strong style="color: red;">Ürün Bulunamadı</strong>
         @else
            @include('product.searchResult', ['product'=>session()->get('product')])
        @endif


    @endif


@endsection