<h4>Kim bu Market Siparişim?</h4>
<p>Marketsiparişim, paket servis hizmeti bulunan marketler ile market siparişi vermek isteyen internet kullanıcılarını aynı ortamda buluşturmak amacı ile kurulmuş bir sitedir.</p>
<p>Kullanıcılar, www.marketsiparisim.com sitesi ve mobil uygulamaları ile anlaşmalı olan tüm marketlerin en son güncellenmiş ürünlerine ulaşarak ekstra ücret vermeden ürün siparişi verebilirler. Verilen sipariş kullanıcıya ulaştırılmak üzere en geç bir dakika içerisinde teknolojinin en son yenilikleri kullanılarak ilgili markete iletilir.</p>


<h4>Hakkında</h4>
<p>Marketsiparisim.com Eternal İnternet Hizmetleri Bilişim ve Teknoloji Limited Şirketi tarafından 2017 yılında Adana'da kurulmuştur.</p>
<p>Amacımız, tüketicinin gelişen teknolojiyi günlük ihtiyaçları için kullanmasını sağlayarak  zamandan ve paradan tasarruf etmesi ile birlikte, küçük ve orta ölçekli marketlerin online sisteme geçmelerini sağlayarak geçmişte tüketicilerin ihtiyaçlarını en ekonomik şekilde karşılayan bakkal amcalar ile sıkı dostluğumuzu günümüzdeki internet dünyasında devam ettirerek gönül bağımızı koparmadan kesintisiz ticaretimizi devam ettirmektir. </p>
<p>
    Sosyal sorumluluk bilinci doğrultusunda halkımıza saygılı, ilkeli ticaret anlayışı ile önce Adana'da, ilerleyen dönemlerde tüm Türkiye'de hizmet vermek için çabalıyoruz.</p>