<ol>
    <li>
        <h4>Market Siparişim' e Nasıl üye olunur?</h4>
        <p>Market Siparişim’e üye olmak için önce bilgisayardan giriş yapıyorsanız www.marketsiparisim.com web sitesi veya mobil cihazlardan giriş yapıyorsanız Apple Store veya Google Play store da “<strong>Market Siparişim</strong>” kelimelerini aratmanız yeterlidir.
            Web sitesinden üye olmak istediğinizde; Ana sayfanın sol üst köşesinde menüyü temsil eden alt alta olan 3 çizgiye tıklayarak açılan sekmeden “<strong>Giriş Yap</strong>” bağlantısına tıklayarak, karşınıza çıkan yeni sayfada E-posta ile Üye Ol ya da Sosyal Ağ ile Üye Ol bağlantılarından herhangi birisinin altındaki formu doldurarak <strong>Market Siparişim</strong> üyesi olabilirsiniz. Market Siparişim üzerinden gerçekleşen işlemler mesafeli sözleşmeler yönetmeliği kapsamında sayılmamaktadır.
        </p>
    </li>
    <li>
        <h4>Ürün Seçimi Nasıl Yapılır?</h4>
        <p>Market Siparişim'den ürün seçimi yapmak için, Market Siparişim web sitesi veya mobil uygulamalardan, “<strong>Market</strong>” butonundan açılan pencerede dilediğiniz marketi seçip, istediğiniz ürüne ait kategorileri detaylarına gitmeniz ve seçtiğiniz ürünün üzerine gelip (+) işaretine tıklamanız gerekmektedir.</p>
    </li>
    <li>
        <h4>Market Siparişim’de Sipariş Nasıl Verilir?</h4>
        <p>Ürün siparişi vermek için öncelikle sepetinize toplamda seçmiş olduğunuz marketin gönderim için belirlemiş olduğu “<strong>Minimum Paket Tutarı</strong>”nı aşan en az bir ürün eklemelisiniz. Bu koşulu sağladığınız takdirde, sepetinizin altında görebileceğiniz “<strong>Sipariş Ver</strong>” bağlantısına tıklayarak sepetinizi onaylayıp, ödeme sayfasına geçebilirsiniz. Bu adımda da gönderim yapılmasını istediğiniz adresi ve ödeme şeklinizi seçerek ve sonrasında “<strong>Devam Et</strong>” butonuna tıklayarak siparişinizi verebilirsiniz.
            “<strong>Not</strong>” bölümünden markete özel istekleriniz için not bırakabilirsiniz.</p>
    </li>
    <li>
        <h4>Market Siparişimde verilen siparişin ödemesi nasıl yapılır?</h4>
        <p>Siparişinizin ödemesi sipariş anında internetten “<strong>Online Kredi Kartı</strong>” veya “<strong>Market Para</strong>” yoluyla ya da siparişi teslim alırken “<strong>Kapıda Nakit Ödeme</strong>”  gibi yöntemlerle yapılabilir. Ancak istediğiniz şekilde ödemenizi yapabilmek için siparişinizi Sipariş Ver butonuna tıklayarak tamamlamadan önce, “<strong>Ödeme Yöntemi</strong>” Seçimi panelinden seçmelisiniz. Eğer Online Kredi Kartı yoluyla ödemeyi seçtiyseniz, Kredi Kartı Numaranızı ve CVV bilgisini girerek ödemenizi yapabilirsiniz. Siparişinizi verdikten sonra ekranda Siparişiniz İletildi mesajınızı takiben karşınıza çıkacak ekranda, ödeme bilgilerinizin de olduğu Sipariş Özetinizi görebilir, bir yanlışlık olduğunu düşünüyorsanız Destek bağlantısı üzerinden müşteri temsilcimizle görüşebilirsiniz.</p>
    </li>
    <li>
        <h4>Kişisel veriler ve uyuşmazlıkların çözümü?</h4>
        <p>Marketsiparişim’in kişisel verilere ilişkin gizlilik kurallarına ve uyuşmazlıkların çözümü için gerekli bilgilendirmelere, sitenin en altında bulunan "<strong>Kullanıcı Hizmet Sözleşmesi</strong>" linki üzerinden erişebilirsiniz.</p>
    </li>
    <li>
        <h4>Market Siparişim'e üye olmak için bir ücret ödemem gerekir mi?</h4>
        <p>Ana sayfanın sol üst köşesinde menüyü temsil eden alt alta olan 3 çizgiye tıklayarak açılan sekmeden "<strong>Giriş Yap</strong>" butonuna tıklayarak üyelik formunu doldurabilirsiniz. Üyelik tamamen <strong>ÜCRETSİZ</strong> ve çok kolay.</p>
    </li>
    <li>
        <h4>Ürün siparişimi Market Siparişim Üzerinden verdim, ilave bir ücret ödeyecek miyim?</h4>
        <p>Hizmetimiz internet yoluyla bilgisayar veya mobil cihazlardan sipariş verenler için tamamıyla ücretsizdir. İlave olarak bizim üzerimizden gidecek siparişlerinizde market para biriktirebilirsiniz.</p>
    </li>
    <li>
        <h4>Kredi Kartı bilgilerini vermek istemeyenler için?</h4>
        <p>Marketsiparişim asla kredi kartı numaranızı kayıt etmemektedir.</p>
    </li>
    <li>
        <h4>İlk defa kaydolurken gönderilen kod nedir?</h4>
        <p>Bilgisayar tarafından otomatik olarak üretilen girişimleri birbirinden ayırmaya yardımcı olmak için kullanılan bir güvenlik yöntemidir.</p>
    </li>
    <li>
        <h4>Şifremi veya kullanıcı adımı unuttum?</h4>
        <p>Giriş bölümünde "Şifremi unuttum" butonuna kayıt olurken vermiş olduğunuz mail adresini yada cep telefonu numaranızı yazıp gönder dediğiniz an, kullanıcı adını ve geçici şifreniz gelecektir. Şifre değiştir bölümünden yeni şifre oluşturabilirsiniz.</p>
    </li>
    <li>
        <h4>Neden kayıt olmam gerekiyor?</h4>
        <p>Sadece kampanyalar ve fırsat ürünlerini öğrenebilmek için değil marketlerinizden sipariş verebilmek için kayıt olmanız gerekiyor.</p>
    </li>
    <li>
        <h4>Haritada birçok market var ama ben beğendiğim marketlerden sipariş vermek istiyorum?</h4>
        <p>Sizin beğendiğiniz marketler bizim içinde değerlidir. Ana sayfadan “<strong>Market Öner</strong>” bölümünde beğendiğiniz marketi bize iletin, marketinize bizden bahsedin bizde en kısa sürede sizin için marketi sisteme alalım.</p>
    </li>
    <li>
        <h4>Market Siparişim hangi saatler arası açık?</h4>
        <p>Bölgenizdeki marketlerin çalışma saatleri içerisinde müşterilere hizmet vermekteyiz. Ancak mesela sabah kahvaltısı için sipariş vermeniz gerektiğinde marketin kapalı olduğu saatlerde sipariş verip marketin çalışma saatleri içinde ihtiyacınız olan saatte ürünlerinizi getirmesini isteyebilirsiniz. Siparişleriniz anında marketin ekranına düşecek ve sabah açılış anında marketiniz görebilecektir.</p>
    </li>
    <li>
        <h4>Telefon ile sipariş vermek varken neden internet üzerinden sipariş vereyim?</h4>
        <p>Online sipariş vermenin telefonla sipariş vermeye göre kıyaslanamayacak kadar artıları vardır. Öncelikle sipariş verirken meşgul sesini duyup 5 dakika daha beklemeyeceksiniz, sipariş vermek için telefonunuzu meşgul etmeyeceksiniz. Ürünlerin markalarını ve fiyatlarını görüp karşılaştırma yapabilecek, istediğiniz ürünü anlatmaya çalışmayacak doğru ürün siparişi vereceksiniz. Acaba yanlış mı hesaplandı fazla mı ödeme yaptım demeyeceksiniz sepetinizde net tutarı göreceksiniz. Anneniz, çocuğunuz veya başka bir dostunuz için alış veriş yapmak zorunda kaldığınızda market aramayacak, Market Siparişim üzerinden göndereceğiniz adrese en yakın marketten sipariş verip gönderebileceksiniz. Sisteme girdiğiniz adresler kayıtlı olacağından yanlış adrese gitme riski olmayacak.</p>
    </li>
    <li>
        <h4>Siparişimin sizin elinize ulaşıp ulaşmadığını nasıl öğrenirim?</h4>
        <p>Siparişinizi tamamladığınızda www.marketsiparisim.com üzerinden siparişiniz doğrudan tercih ettiğiniz markete düşecek ve market onayını takiben siparişiniz yola çıktığında marketten bir bilgi mesajı tarafınıza gönderilecektir.</p>
    </li>
    <li>
        <h4>Siparişim markete ne zaman ve nasıl ulaşıyor?</h4>
        <p>www.marketsiparisim.com üzerinden verdiğiniz sipariş tercih ettiğiniz markete saniyeler içerisinde ulaşıyor.</p>
    </li>
    <li>
        <h4>Sipariş verdikten sonra Market Siparişim yada market beni arayacak mı?</h4>
        <p>Verdiğiniz siparişte bir sorun olmadığı takdirde <strong>Market Siparişim</strong> sizi aramayacaktır. Market adresinizi bulamaz ise veya ürünler ile ilgili bir sıkıntı varsa market sizi arayabilir. Normal şartlarda sizi rahatsız etmekten imtina etmekteyiz.</p>
    </li>
    <li>
        <h4>Bilgilerimi market ile paylaşacak mısınız?</h4>
        <p>Siparişinizin size sorunsuz ulaşabilmesi için telefon numaranızı ve adresinizi marketler ile paylaşıyoruz.</p>
    </li>
    <li>
        <h4>İnternet üzerinden ödeme yapıp başka bir adrese ürün gönderebilir miyim?</h4>
        <p>Online kredi kartı ile ödeme yaptıktan sonra marketlerin gönderme alanında yer alan istediğiniz adrese ürün gönderebilirsiniz.</p>
    </li>
    <li>
        <h4>Siparişim ulaşmazsa veya bir sorun varsa ne yapmalıyım?</h4>
        <p>Ana sayfada yer alan "Destek" bölümünden bize ulaşabilirsiniz.</p>
    </li>
    <li>
        <h4>Sepetimden ürün silmek istiyorum?</h4>
        <p>Sepetinizde yer alan ürünün yanındaki veya reyonda bulunan seçtiğiniz ürünün altındaki (-) işaretini tıklayarak ürünü sepetinizden çıkara bilirsiniz.</p>
    </li>
    <li>
        <h4>Verdiğim bir siparişin detaylarını nasıl görebilirim?</h4>
        <p>Ana sayfanın sol üst köşesinde menüyü temsil eden alt alta olan 3 çizgiye tıklayarak açılan sekmeden "<strong>Siparişlerim</strong>" sekmesini tıklayarak sipariş detaylarını görebilirsiniz.</p>
    </li>
    <li>
        <h4>Verdiğim bir siparişi nasıl iptal edebilirim?</h4>
        <p>Verdiğiniz siparişi iptal etmek için "Canlı destek" bölümünden bize ulaşa bilirsiniz.</p>
    </li>
    <li>
        <h4>Marketlerin minimum paket tutarı ne kadardır?</h4>
        <p>Sistemde yer alan marketler paket tutarlarını kendileri belirlemektedir. Seçtiğiniz markete sipariş verme esnasında minimum tutarı göreceksiniz.</p>
    </li>
    <li>
        <h4>Farklı bir şehirden sipariş vereceğim. Aynı hesap üzerinde farklı bir adres ekleyebilir miyim?</h4>
        <p>Market Siparişim uygulaması şimdilik sadece Adana'da. Ancak panel üzerinde farklı adresler girip Adana içerisinde kayıtlı marketlerimizin hizmet verdiği istediğiniz yere ürün göndere bilirsiniz.</p>
    </li>
    <li>
        <h4>Önceki siparişlerimi nasıl kolayca tekrarlaya bilirim?</h4>
        <p>Ana sayfanın sol üst köşesinde menüyü temsil eden alt alta olan 3 çizgiye tıklayarak açılan sekmeden "<strong>Siparişlerim</strong>" sekmesini tıklayarak yenilenmesini istediğiniz siparişin üzerine gelip "<strong>Siparişi Tekrarla</strong>" sekmesinden yapabilirsiniz.</p>
    </li>
    <li>
        <h4>Önceki siparişlerimden herhangi birini silebilir miyim?</h4>
        <p>Siparişlerin arşivlenmesi için geçmiş siparişler silinememektedir.</p>
    </li>
    <li>
        <h4>Favorilerim nedir? Nasıl kullanabilirim?</h4>
        <p>Ürünlere tıkladığınızda açılan pop up görüntüde üstte bulunan ♥/Beğen sembolüne tıkladığınızda ürün favorilerinize eklenecektir. Çok kullandığınız ve beğendiğiniz ürünleri favorilerinize ekleyerek ürünü tekrar aramak zorunda kalmayacaksınız ve kolaylıkla sipariş verebileceksiniz. </p>
    </li>
    <li>
        <h4>Nedir bu kampanyalar ve fırsatlar?</h4>
        <p>Market Siparişim fırsatları tüm üyelerimize açıktır. Fırsatlar, Market Siparişim bünyesinde bulunan marketlerde sadece Market Siparişim'de geçerli olmak üzere yapılan indirim veya farklı promosyonlardan oluşur.</p>
    </li>
    <li>
        <h4>Nasıl yararlanabilirim?</h4>
        <p>Marketsiparişim kullanıcıları  marketlerin sundukları tüm fırsatlardan faydalanabilirler.</p>
    </li>
    <li>
        <h4>Mobil cihazlar için özel bir uygulama bulunuyor mu?</h4>
        <p>Android ve IOS için özel uygulamalar mevcuttur. Apple Store veya Google Play Store da “Market Siparişim” aramasını yapmanız yeterli.</p>
    </li>
    <li>
        <h4>Market Siparişim mobil ücretlimidir?</h4>
        <p>Market siparişimin web ve mobil uygulamaları tamamen ücretsizdir.</p>
    </li>
    <li>
        <h4>Market Siparişim Mobil uygulamasını hangi telefonlarda kullanabilirim?</h4>
        <p>Marketsiparisim.com' a internet erişimi olan tüm akıllı telefonlardan ulaşabilirisiniz.</p>
    </li>
    <li>
        <h4>Market Siparişim mobile bağlanamıyorum bir sorun olabilir mi?</h4>
        <p>Market Siparişim mobil sayfalarına ulaşabilmek için telefonunuzun internet bağlantısı olması gerekmektedir.</p>
    </li>
    <li>
        <h4>Var olan teslimat adresimi nasıl değiştirebilirim?</h4>
        <p>Market Siparişim uygulamasına birden fazla adres ekleyebilir ve teslimat adresi olarak herhangi birini kullanabilirsiniz.</p>
    </li>
    <li>
        <h4>Yeni bir teslimat adresi nasıl yaratırım?</h4>
        <p>Üye ana sayfanızda bulunan "<strong>Adreslerim</strong>" kısmından dilediğiniz kadar yeni adres ekleyebilir ya da mevcut adreslerinizi değiştirebilirsiniz. Ayrıca, sepetinize ürün atıp "<strong>Sipariş Ver</strong>" butonuna bastıktan sonra karşınıza çıkan “<strong>Adresim</strong>” ekranından da yeni bir adres ekleyebilir ya da mevcut adreslerinizi değiştirebilirsiniz.</p>
    </li>
    <li>
        <h4>Teslimat adreslerime nereden ulaşabilirim?</h4>
        <p>“<strong>Adreslerim</strong>” sekmesine geldiğinizde tüm adreslerinizi görebilir, yeni adres ekleyebilir veya istediğiniz adresi silebilirsiniz.</p>
    </li>
    <li>
        <h4>İptal olan bir siparişte yorum ve puanlama yapabilir miyim?</h4>
        <p>Maalesef iptal olan bir siparişe puan veremezsiniz ve yorum yapamazsınız, fakat şikayet ve önerilerinizi bize iletebilirsiniz. Ekibimiz konuyla hemen ilgilenecektir.</p>
    </li>
    <li>
        <h4>Puanlama sistemi nedir? Nasıl çalışıyor?</h4>
        <p>Yakında.</p>
    </li>
    <li>
        <h4>Yorumlama sistemi nedir?</h4>
        <p>Yakında.</p>
    </li>
    <li>
        <h4>Sisteme verdiğim kredi kart bilgileri başkaları tarafından görüntülenebilir mi?</h4>
        <p>Tarayıcınızın (browser) alt kısmında görmekte olduğunuz anahtar ikonu, o sayfada tarayıcınız vasıtası ile gönderdiğiniz hiç bir bilginin işlem sırasında üçüncü şahıslar tarafından görünemeyeceğini belirtmektedir. 128 bit SSL şifreleme tekniğiyle şifrelenen bilgilerinizin onayı, Akbank Sanal POS sistemleri üzerinden ilgili banka ile irtibat kurularak alınır. Bilgiler hiçbir şekilde markete, Market Siparişim yetkililerine veya üçüncü şahıslara ulaşmaz. Ayrıca bilgileriniz kayıt altına alınmaz.</p>
    </li>
    <li>
        <h4>Ödememi online kredi kartı ile yaptım. Siparişimi iptal etmek istiyorum ne yapmam gerekiyor?</h4>
        <p>Destek Birimimiz ile iletişime geçerek siparişinizin iptalini talep edebilirsiniz. İşlemin gerçekleşmesi durumunda tutar hesabınıza iade edilebilir. İadenin ekstrenize yansıması bankanıza bağlı olarak değişiklik gösterebilir.</p>
    </li>
    <li>
        <h4>Ödemesini Online Kredi Kartı ile gerçekleştirdiğim siparişi iptal ettim, ücret iade prosedürü nedir?</h4>
        <p>Siparişinizin tutarı hemen iade edilmektedir fakat iadenin ekstrenize yansıması bankanıza bağlı olarak değişiklik gösterebilir.</p>
    </li>
    <li>
        <h4>Online kredi kartı ile ödeme seçeneği nedir?</h4>
        <p>Online Kredi Kartı ile ödeme seçene Market Siparişim 'in kullanıcılara sunduğu bir ödeme opsiyonudur. Kredi kartınızla internet ortamında siparişinizin ödemesini gerçekleştirebilirsiniz, hiçbir şekilde kredi kartınıza fiziksel olarak ihtiyaç duymazsınız ve bu sayede kapıda ödeme yapmazsınız.</p>
    </li>
    <li>
        <h4>Ödemeyi online kredi kartı ile yaptım. Daha sonra siparişimden ürün çıkarmak istiyorum?</h4>
        <p>Destek Birimimiz ile iletişime geçerek siparişinizden istediğiniz ürünün çıkartılmasını talep edebilirsiniz. İşlemin gerçekleşmesi durumunda tutar hesabınıza iade edilebilir. İadenin ekstrenize yansıması bankanıza bağlı olarak değişiklik gösterebilir.</p>
    </li>
    <li>
        <h4>Ödemeyi online kredi kartı ile yaptım. Daha sonra siparişime ürün eklemek istiyorum?</h4>
        <p>Online Kredi Kartı ile ödemesini gerçekleştirdiğiniz siparişlerde, siparişi verdikten sonra ürün ekleme yapamazsınız.</p>
    </li>
    <li>
        <h4>Güvenlik kodu nedir?</h4>
        <p>Güvenlik Kodu, kredi kartınızın arkasında bulunan rakamın son üç hanesidir. Kartın manyetik şeridine yazılmayan bu numara sayesinde kredi kart numaranız ve son kullanma tarihiniz ele geçirilse bile Güvenlik Kodu gizli kalacağından kartınız isteğiniz dışında kullanılamaz.</p>
    </li>
    <li>
        <h4>Market para nedir?</h4>
        <p>Marketpara, Market Siparişim kullanıcılarına sunulan bir harcadıkça kazan yöntemidir. Yaptığınız alışverişlerden puan toplayarak paraya çevirebilir ve biriktirdiğiniz paralar ile Market Siparişim üzerinden alış veriş yapabilirsiniz. Ayrıca sadece siz değil size referans olan kişi de siz harcadıkça market para kazanır. Aynı şekilde siz de tanıdıklarınıza referans olursanız onlar harcadıkça hem onlar hem de siz kazanabilirsiniz.</p>
    </li>
    <li>
        <h4>Market Siparişi üyesiyim, nasıl market para oluştura bilirim?</h4>
        <p>Alış veriş yaptıkça market paralarınız oluşmaya başlayacaktır.</p>
    </li>
    <li>
        <h4>Market para ile nasıl ödeme yapabilirim?</h4>
        <p>Sipariş onay ekranında "<strong>Marketpara</strong>" seçeneğini kullanarak ödeme yapabilirsiniz.</p>
    </li>
    <li>
        <h4>Ödememi market para ile yaptım ürün çıkarmak istiyorum?</h4>
        <p>Destek birimimiz ile iletişime geçerek ürün çıkarabilirsiniz.</p>
    </li>
    <li>
        <h4>Ödememi market para ile yaptım ürün eklemek istiyorum?</h4>
        <p>Ödeme yapıldıktan sonra ürün eklemek maalesef mümkün değil, ancak yeni sipariş oluşturabilirsiniz.</p>
    </li>
    <li>
        <h4>Ödememi market para ile yaptım siparişimi iptal etmek istiyorum?</h4>
        <p>Destek Birimimiz ile iletişime geçerek siparişi iptalini talep edebilirsiniz.</p>
    </li>
    <li>
        <h4>Market para bakiyemi kullanmak için belirli bir zaman kısıtlaması var mı?</h4>
        <p>Biriktirdiğiniz market parayı istediğiniz zaman kullanabilirsiniz.</p>
    </li>
    <li>
        <h4>Kartımı nasıl kaydedebilirim?</h4>
        <p>Şu anda kart bilgilerinizi kayıt etmeniz için herhangi bir altyapı bulundurmuyoruz.</p>
    </li>
</ol>