@extends('layouts.app')

@section('content')

    <!-- TopCategoryModal -->
    <div id="createTopCategoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Temel Kategori Ekle</h4>
                </div>
                <div class="modal-body">
                    <form class="form-group form-horizontal" method="post" enctype="multipart/form-data"
                          action="{{env('APP_URL').'storeTopCategory'}}">
                        {{csrf_field()}}
                        <div class="form-group{{ $errors->has('topCategoryName') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Kategori</label>

                            <div class="col-sm-10">
                                <input type="text" name="topCategoryName" class="form-control"
                                       value="{{old('topCategoryName')}}" placeholder="Kategori Adı">
                                @if($errors->has('topCategoryName'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('topCategoryName') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Resim</label>

                            <div class="col-sm-10">
                                <input type="file" name="photo" class="form-control">
                                @if($errors->has('photo'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-default" value="Ekle"/>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
    <!-- endTopCategoryModal -->

    <!-- SubCategoryModal -->
    <div id="createSubCategoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group{{ $errors->has('subCategoryName') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Kategori</label>

                            <div class="col-sm-10">
                                <input type="text" name="subCategoryName" class="form-control"
                                       value="{{old('subCategoryName')}}" placeholder="Kategori Adı">
                                @if($errors->has('subCategoryName'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('subCategoryName') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Resim</label>

                            <div class="col-sm-10">
                                <input type="file" name="photo" class="form-control">
                                @if($errors->has('photo'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-default" value="Ekle"/>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
    <!-- endSubCategoryModal -->

    @if($errors->has('topCategoryName'))
        <div class="alert alert-danger">
            <strong>UYARI!</strong> Kategori adı boş bırakılamaz !
        </div>
    @endif

    @if($errors->has('subCategoryName'))
        <div class="alert alert-danger">
            <strong>UYARI!</strong> Kategori adı boş bırakılamaz !
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Temel Kategoriler</h3>&nbsp;
                    <a href="#"
                       data-toggle="modal"
                       data-target="#createTopCategoryModal">Ekle
                    </a>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <!-- /.row -->
                    </div>
                    <!-- ./box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>


    @foreach($topCategories as $topCategory)
        <?php
        $topPhoto = json_decode($topCategory->photo);
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$topCategory->categoryName}}</h3>&nbsp;
                        <a href="#"
                           categoryname="{{$topCategory->categoryName}}"
                           categoryid="{{$topCategory->id}}"
                           class="newSubCategoryBtn"
                           data-toggle="modal"
                           data-target="#createSubCategoryModal">Alt Kategori Ekle
                        </a>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-plus"></i>
                            </button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul>
                            @foreach($topCategory->subCategories as $subCategory)
                                <?php
                                $subPhoto = json_decode($subCategory->photo);
                                ?>
                                <li>{{$subCategory->categoryName}} | <a
                                            href="{{env('APP_URL').'SubCategory/'.$subCategory->id.'/Products'}}">Ürünleri
                                        Görüntüle</a> | <a
                                            href="{{env('APP_URL').'SubCategory/'.$subCategory->id.'/NewProduct'}}">Ürün
                                        Ekle</a>
                                </li>
                            @endforeach
                        </ul>
                        <!-- /.row -->
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer">
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    @endforeach

@endsection

@section('scripts')

    <script>

        {{--@if($errors->has('topCategoryName'))
        $(document).ready(function () {
            $('#createTopCategoryModal').modal({show: true});
        });
        @endif

        @if($errors->has('subCategoryName'))
        $(document).ready(function () {
            $('#createSubCategoryModal').modal({show: true});
        });
        @endif--}}

        $('.newSubCategoryBtn').click(function () {
            var categoryId = $(this).attr('categoryid');
            var categoryName = $(this).attr('categoryname');
            var modal = $('#createSubCategoryModal');
            modal.find('.modal-title').text(categoryName + ' | Alt Kategori Ekle');
            var url = ('{{env('APP_URL')}}');
            url += 'topCategory/' + categoryId + '/storeSubCategory';
            modal.find('form').attr('action', url);
        });

    </script>

@endsection