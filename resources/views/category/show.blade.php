@extends('layouts.app')

@section('styles')
    <style>

        .ls-products {
            padding-left: 0;
            padding-bottom: 1px;
            margin-bottom: 20px;
            list-style: none;
            overflow: hidden;
        }

        .ls-products li {
            float: left;
            width: 25%;
            height: 150px;
            padding: 10px;
            margin: 0 -1px -1px 0;
            font-size: 12px;
            line-height: 1.4;
            text-align: center;
            border: 1px solid #ddd;
        }

        .ls-products img {
            margin-top: 5px;
            margin-bottom: 10px;
            font-size: 24px;
            width: 100%;
            max-width: 75px;
            max-height: 75px;
        }

        .ls-products .product-name {
            display: block;
            text-align: center;
            word-wrap: break-word;
        }

        .ls-products li:hover {
            background-color: rgba(86, 61, 124, .1);
        }

        @media (min-width: 768px) {
            .ls-products li {
                width: 12.5%;
            }
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{$subCategory->categoryName}}</h3>&nbsp;| <a href="{{env('APP_URL').'SubCategory/'.$subCategory->id.'/NewProduct'}}">Ürün Ekle</a>
        </div>
        <!-- /.box-header -->
        <div id="products" class="box-body">
            <ul class="ls-products">
                @foreach($subCategory->products as $product)
                    <?php
                    $photo = json_decode($product->photo);
                    ?>
                    <li>
                        <span><a href="{{env('APP_URL').'Product/'.$product->id}}"><img src="{{env('PHOTO_URL'). Storage::url('app/'.$photo->small)}}"/></a></span>
                        <span class="product-name">{{$product->productName}}</span>
                    </li>
                @endforeach
            </ul>
            <!-- /.row -->
        </div>
        <!-- ./box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
@endsection