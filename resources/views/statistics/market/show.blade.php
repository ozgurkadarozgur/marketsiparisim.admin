@extends('layouts.app')

@section('content')

    <h3>{{$market->marketName}}</h3>

    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sipariş Sayısı</span>
                    <span class="info-box-number">{{$statistic->totalOrderCount}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Bölgedeki Kullanıcı Sayısı</span>
                    <span class="info-box-number">{{$statistic->userCountHasOrdered}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">Ürün Sayısı</span>
                    <span class="info-box-number">{{$statistic->totalProductCount}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        {{--<div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Puan</span>
                    <span class="info-box-number">4.3</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>--}}
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Sipariş İstatistikleri</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-responsive">
                <tr>
                    <th>Onaylanmayan Sipariş Sayısı</th>
                    <td>{{$statistic->deniedOrderCount}}</td>
                </tr>
                <tr>
                    <th>İptal Edilen Sipariş Sayısı</th>
                    <td>{{$statistic->canceledOrderCount}}</td>
                </tr>
                <tr>
                    <th>Kredi Kartı İle Yapılan Alışveriş Sayısı</th>
                    <td>{{$statistic->creditCartOrderCount}}</td>
                </tr>
                <tr>
                    <th>Kredi Kartı İle Yapılan Alışveriş Tutarı</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Kapıda Ödeme İle Yapılan Alışveriş Sayısı</th>
                    <td>{{$statistic->cashOrderCount}}</td>
                </tr>
                <tr>
                    <th>Kapıda Ödeme İle Yapılan Alışveriş Tutarı</th>
                    <td></td>
                </tr>
            </table>
        </div>

    </div>

@endsection