@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Ürün İstatistikleri</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>Ürün Adı</th>
                    <th>Toplam Sipariş Sayısı</th>
                    <th>Toplam Satış Tutarı</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->productName}}</td>
                            <td>{{($product->count)}}</td>
                            <td>{{($product->price . ' TL')}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection