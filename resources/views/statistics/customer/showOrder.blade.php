@extends('layouts.app')

@section('content')

    <!--Map Modal -->
    <div id="mapModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Adres</h4>
                </div>
                <div class="modal-body">
                    <div id="map" style="width: 100%; height: 500px;"></div>
                </div>
            </div>

        </div>
    </div>
    <!--End Map Modal -->


    <div class="row">
        <div class="col-md-12">


            <div class="row">
                <div class="col-md-12">
                    <span class="pull-left">Sipariş Durumu : {{$order->statusInf->status}}</span>

                    <button id="print" class="btn btn-primary" style="float:right"><span
                                class="glyphicon glyphicon-print"></span>&nbsp;Siparişi
                        Yazdır
                    </button>
                </div>
            </div>

            @if($order->status == 4)
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            <strong>{{$order->canceledOrder->description}}</strong>
                        </div>
                    </div>
                </div>
            @endif

            @if($order->status == 2)
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            <strong>{{$order->deniedOrder->description}}</strong>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-7">
            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Sipariş Detayları</h3>
                </div>

                <div class="box-body">
                    <table id="orderDetailTable" class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Adet</td>
                            <td>Ürün Adı</td>
                            <td>Birim Fiyat</td>
                            <td>Toplam Tutar</td>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($order->details as $detail)
                            <tr>
                                <th>{{$detail->count}}</th>
                                <td>{{$detail->product->productDetail->productName}}</td>
                                <td>{{$detail->product->price}} TL</td>
                                <th>{{$detail->count * $detail->product->price}} TL</th>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2"></td>
                            <td style="text-align: right"><strong>Toplam Tutar:</strong></td>
                            <td style="text-align: right"><strong>{{$totalPrice}} TL</strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Sipariş Tarihi</td>
                                    <td><strong>{{$order->created_at}} ({{$order->created_at->diffForHumans()}})</strong></td>
                                </tr>
                                <tr>
                                    <td>Ödeme Şekli</td>
                                    <td><strong>{{$order->paymentMethod->method}}</strong></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Müşteri Bilgileri</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td style="min-width: 100px;">Ad-Soyad</td>
                                    <td><strong>{{$order->customer->name}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Telefon</td>
                                    <td><strong>{{$order->customer->phone}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Sipariş Notu</td>
                                    <td><strong>{{$order->note}}</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        Adres &nbsp;<a href="#"><span class="glyphicon glyphicon-map-marker"
                                                                      data-toggle="modal"
                                                                      data-target="#mapModal"></span></a>
                                    </td>
                                    <td><strong>{{$order->customerAddress->address}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Adres Tarifi</td>
                                    <td><strong>{{$order->customerAddress->addressDescription}}</strong></td>
                                </tr>
                                @if($order->status == 1)
                                    <tr>
                                        <td>
                                            <a style="width: 100%;" class="btn btn-large btn-success"
                                               data-btn-ok-label="Evet"
                                               data-btn-cancel-label="Hayır"
                                               data-toggle="confirmation"
                                               data-title="Siparişi Onayla?"
                                               href="{{route('approveOrder', $order->id)}}">
                                                <span class="glyphicon glyphicon-ok"></span>&nbsp;Onayla</a>
                                            {{--<a href="{{route('approveOrder', $order->id)}}" style="width: 100%;"
                                               class="btn btn-success"><span
                                                        class="glyphicon glyphicon-ok"></span>&nbsp;Onayla
                                            </a>--}}
                                        </td>
                                        <td>
                                            <button style="width: 100%;" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#denyOrderModal"><span
                                                        class="glyphicon glyphicon-ban-circle"></span>&nbsp;Reddet
                                            </button>
                                        </td>

                                    </tr>
                                @elseif($order->status == 3)
                                    <tr>
                                        <td>
                                            <a style="width: 100%;" class="btn btn-large btn-primary"
                                               data-btn-ok-label="Evet"
                                               data-btn-cancel-label="Hayır"
                                               data-toggle="confirmation"
                                               data-title="Siparişi Gönder?"
                                               href="{{route('onWayOrder', $order->id)}}">
                                                <span class="glyphicon glyphicon-road"></span>&nbsp;Siparişi Gönder</a>
                                        </td>
                                        <td>
                                            <button style="width: 100%;" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#cancelOrderModal"><span
                                                        class="glyphicon glyphicon-ban-circle"></span>&nbsp;İptal Et
                                            </button>
                                        </td>

                                    </tr>
                                @elseif($order->status == 5)
                                    <tr>
                                        {{--<td>
                                            <a style="width: 100%;" class="btn btn-large btn-success"
                                               data-btn-ok-label="Evet"
                                               data-btn-cancel-label="Hayır"
                                               data-toggle="confirmation"
                                               data-title="Teslim Edildi?"
                                               href="{{route('deliverOrder', $order->id)}}">
                                                <span class="glyphicon glyphicon-ok"></span>&nbsp;Teslim Edildi</a>
                                        </td>--}}
                                        <td colspan="2">
                                            <button class="btn btn-primary center-block" data-toggle="modal"
                                                    data-target="#notDeliveredOrderModal"><span
                                                        class="glyphicon glyphicon-ban-circle"></span>&nbsp;Teslim Edilmedi Olarak Bildir
                                            </button>
                                        </td>

                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>


        $('#print').click(function () {
            let orderDetails = '<table>' +
                '<thead>' +
                '<tr>' +
                '<th>Ürün Adı</th>'+
                '<th>Fiyat</th>'+
                '</tr>'+
                '</thead>';

            @foreach($order->details as $detail)
                orderDetails += '<tr>\n' +
                '                <td><span style="font-size:12px">{{$detail->product->productDetail->productName}}</span></td>\n' +
                '                <td style="font-size:12px">{{$detail->product->price . ' &#8378; X' . $detail->count}}</td>\n' +
                '                <td style="font-size:12px">{{$detail->count * $detail->product->price}} &#8378;</td>\n' +
                '                </tr>\n' +
                '             ';
            @endforeach
                orderDetails += '</table><p>Tutar:{{$totalPrice}} &#8378;</p>';
            orderDetails += '<table>' +
                '<tr>' +
                '<td>Müşteri</td>'+
                '<td>{{$order->customer->name}}</td>'+
                '</tr>'+
                '<tr>'+
                '<td>Telefon</td>' +
                '<td>{{$order->customer->phone}}</td>' +
                '</tr>'+
                '<tr>'+
                '<td>Adres</td>'+
                '<td>{{$order->customerAddress->address}}</td>'+
                '</tr>'+
                '</table>';
            var myWindow = window.open('', '');
            myWindow.document.write(orderDetails);
            myWindow.print();
        });

        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',

            // other options
        });


        function initMap() {

            let latitude = ('{{$order->customerAddress->latitude}}');
            let longitude = ('{{$order->customerAddress->longitude}}');

            var myLatLng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }

        $('#mapModal').on('shown.bs.modal', function () {
            google.maps.event.trigger(map, "resize");
            initMap();
        });

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7DIJ048XamPeX1x2e_DS_NBHVQT4CyrE&callback=initMap">
    </script>
@endsection
