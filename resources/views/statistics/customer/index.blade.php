@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Kullanıcılar</h3>
            <span class="pull-right">{{$customers->links()}}</span>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Ad Soyad</th>
                    <th>Email</th>
                    <th>Telefon</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                    <tr>
                        <td>{{$loop->iteration + ($customers->count() *  ($customers->currentPage() - 1))}}</td>
                        <td><a href="{{route('showCustomerStatistic', $customer->id)}}">{{$customer->name}}</a></td>
                        <td>{{$customer->email}}</td>
                        <td>{{$customer->phone}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <span class="pull-right">{{$customers->links()}}</span>
        </div>

    </div>

@endsection