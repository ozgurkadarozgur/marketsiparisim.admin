@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{$customer->name}}</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-responsive">
                <tr>
                    <th>Ad Soyad</th>
                    <td>{{$customer->name}}</td>
                </tr>
                <tr>
                    <th>Kredi Kartı İle Yaptığı Alışveriş Sayısı</th>
                    <td>{{$statistics->creditCartOrderCount}}</td>
                </tr>
                <tr>
                    <th>Kredi Kartı İle Yaptığı Alışveriş Toplam Tutarı</th>
                    <td>{{($statistics->creditCartOrderPrice) ? $statistics->creditCartOrderPrice . ' TL' : '0 TL'}}</td>
                </tr>
                <tr>
                    <th>Kapıda Ödeme İle Yaptığı Alışveriş Sayısı</th>
                    <td>{{$statistics->cashOrderCount}}</td>
                </tr>
                <tr>
                    <th>Kapıda Ödeme İle Yaptığı Alışveriş Toplam Tutarı</th>
                    <td>{{($statistics->cashOrderPrice) ? $statistics->cashOrderPrice . ' TL' : '0 TL'}}</td>
                </tr>
                <tr>
                    <th>Referans Sayısı</th>
                    <td>{{$statistics->referenceCount}}</td>
                </tr>
                <tr>
                    <th>Market Para</th>
                    <td>{{$statistics->marketMoney}}</td>
                </tr>
            </table>
        </div>

    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Geçmiş Siparişler</h3>
        </div>
        <div class="box-body">

            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>Market Adı</th>
                        <th>Tarih</th>
                        <th>Ödeme Türü</th>
                        <th>Fiyat</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($latestOrders as $latestOrder)
                        <tr>
                            <td>{{$latestOrder->marketName}}</td>
                            <td>{{$latestOrder->date}}</td>
                            <td>{{$latestOrder->paymentMethod}}</td>
                            <td>{{($latestOrder->price) ? $latestOrder->price . ' TL' : ''}}</td>
                            <td><a href="{{route('statisticShowOrder', $latestOrder->id)}}">İncele</a></td>
                        </tr>
                    @endforeach
                </tbody>

            </table>

        </div>
    </div>


@endsection