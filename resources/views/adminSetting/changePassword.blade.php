@extends('layouts.app')

@section('content')

    @if(session()->has('message'))

        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>

    @endif

    <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title">Şifre Değiştir</h3>
        </div>

        <div class="box-body">
            <form action="{{route('updatePassword')}}" method="post">
                {{csrf_field()}}

                <table class="table table-bordered table-responsive">
                    <tr>
                        <td>Eski Şifre</td>
                        <td>
                            <input name="oldPassword" class="form-control" value="{{old('oldPassword')}}" required />
                            @if($errors->has('oldPassword'))
                                <span style="color: red;">{{$errors->first('oldPassword')}}</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Yeni Şifre</td>
                        <td>
                            <input name="newPassword" class="form-control" value="{{old('newPassword')}}" required />
                            @if($errors->has('newPassword'))
                                <span style="color: red;">{{$errors->first('newPassword')}}</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Yeni Şifre (Tekrar)</td>
                        <td>
                            <input name="newPasswordAgain" class="form-control" required />
                            @if($errors->has('newPasswordAgain'))
                                <span style="color: red;">{{$errors->first('newPasswordAgain')}}</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" class="btn btn-primary pull-right" value="Kaydet" /></td>
                    </tr>
                </table>

            </form>
        </div>

    </div>

@endsection