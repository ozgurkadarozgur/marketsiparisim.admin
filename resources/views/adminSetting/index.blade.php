@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Hesap Ayarları</h3>
        </div>
        <div class="box-body">
            <a href="{{route('changePassword')}}">Şifre Değiştir</a>
        </div>
    </div>

@endsection