@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Marketler</h3>&nbsp;| <a href="{{env('APP_URL').'NewMarket'}}">Yeni Ekle</a>
                    <div class="box-tools pull-right">
                        {{$markets->links()}}
                    </div>
                    <!-- /.box-header -->
                </div>
                <!-- /.box -->

                <div class="box-body">
                    <table class="table table-bordered table-responsive">
                        <tr>
                            <th>Market Adı</th>
                            <th>Yetkili</th>
                            <th>İl</th>
                            <th>İlçe</th>
                            <th>Bölge</th>
                            <th>Mahalle</th>
                        </tr>
                        @foreach($markets as $market)
                            <tr>
                                <td><a href="{{env('APP_URL').'Market/'.$market->id}}">{{$market->marketName}}</a></td>
                                <td>{{$market->inCharge}}</td>
                                <td>{{$market->city->CityName}}</td>
                                <td>{{$market->county->CountyName}}</td>
                                <td>{{$market->area->AreaName}}</td>
                                <td>{{$market->neighborhood->NeighborhoodName}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>




@endsection