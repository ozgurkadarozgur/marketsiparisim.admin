@extends('layouts.app')


@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Market Ekle</h3> | <a href="{{env('APP_URL').'Markets'}}">Marketleri Görüntüle</a>
            <input type="text" id="pac-input" class="form-control" placeholder="Adres girin...">
        </div>
        <!-- /.box-header -->
        @if(session()->has('message'))
            <div class="alert alert-success">
                <strong>{{session()->get('message')}}</strong>
            </div>
        @endif
        <div id="map" style="width: 100%; height: 300px"></div>
        <!-- form start -->

        <form class="form-horizontal" method="post"
              action="{{env('APP_URL').'storeMarket'}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" id="latitude" name="latitude"/>
            <input type="hidden" id="longitude" name="longitude"/>
            @if($errors->has('latitude'))
                <span class="help-block">
                        <strong>{{ $errors->first('latitude') }}</strong>
                    </span>
            @endif
            @if($errors->has('longitude'))
                <span class="help-block">
                        <strong>{{ $errors->first('longitude') }}</strong>
                    </span>
            @endif
            <div class="box-body">

                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">İl</label>

                    <div class="col-sm-10">
                        <select id="city" name="city" class="form-control">
                            <option selected disabled>Seçiniz..</option>
                            @foreach($cities as $city)
                                <option value="{{$city->CityID}}">{{$city->CityName}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('city'))
                            <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('county') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">İlçe</label>

                    <div class="col-sm-10">
                        <select id="county" name="county" class="form-control">
                            <option selected disabled>Seçiniz..</option>
                        </select>
                        @if($errors->has('county'))
                            <span class="help-block">
                        <strong>{{ $errors->first('county') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Bölge</label>

                    <div class="col-sm-10">
                        <select id="area" name="area" class="form-control">
                            <option selected disabled>Seçiniz..</option>
                        </select>
                        @if($errors->has('area'))
                            <span class="help-block">
                        <strong>{{ $errors->first('area') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('neighborhood') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Mahalle</label>

                    <div class="col-sm-10">
                        <select id="neighborhood" name="neighborhood" class="form-control">
                            <option selected disabled>Seçiniz..</option>
                        </select>
                        @if($errors->has('neighborhood'))
                            <span class="help-block">
                        <strong>{{ $errors->first('neighborhood') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Adres</label>

                    <div class="col-sm-10">
                        <textarea name="address" id="address" class="form-control"
                                  placeholder="Adres" style="resize: none">{{old('address')}}</textarea>
                        @if($errors->has('address'))
                            <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('marketName') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Market Adı</label>

                    <div class="col-sm-10">
                        <input type="text" name="marketName" class="form-control" value="{{old('marketName')}}"
                               placeholder="Market Adı">
                        @if($errors->has('marketName'))
                            <span class="help-block">
                        <strong>{{ $errors->first('marketName') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('inCharge') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Yetkili</label>

                    <div class="col-sm-10">
                        <input type="text" name="inCharge" class="form-control" value="{{old('inCharge')}}"
                               placeholder="Yetkili Kişi">
                        @if($errors->has('inCharge'))
                            <span class="help-block">
                        <strong>{{ $errors->first('inCharge') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Telefon</label>

                    <div class="col-sm-10">
                        <input type="text" name="phone" class="form-control" value="{{old('phone')}}"
                               placeholder="Telefon">
                        @if($errors->has('phone'))
                            <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('eMail') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">E-Mail</label>

                    <div class="col-sm-10">
                        <input type="email" name="eMail" class="form-control" value="{{old('eMail')}}"
                               placeholder="E-Mail">
                        @if($errors->has('eMail'))
                            <span class="help-block">
                        <strong>{{ $errors->first('eMail') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>


                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Ekle</button>
                </div>
                <!-- /.box-footer -->

            </div>
        </form>

    </div>

@endsection

@section('scripts')

    <script>

        $('#city').change(function () {
            var _this = $(this);
            var cityId = _this.val();
            var _token = ('{{csrf_token()}}');
            var url = ('{{route('getCounties')}}');

            $.ajax({
                method: 'post',
                url: url,
                data: {cityId: cityId, _token: _token}
            }).done(function (data) {
                console.log(data);
                $('#county option[class="countyName"]').remove();
                $.each(data, function (index, item) {
                    var option = '<option value="' + item.CountyID + '" class="countyName">' + item.CountyName + '</option>';
                    $('#county').append(option);
                });
            });
        });

        $('#county').change(function () {
            var _this = $(this);
            var countyId = _this.val();
            var _token = ('{{csrf_token()}}');
            var url = ('{{route('getAreas')}}');

            $.ajax({
                method: 'post',
                url: url,
                data: {countyId: countyId, _token: _token}
            }).done(function (data) {

                $('#area option[class="areaName"]').remove();
                $.each(data, function (index, item) {
                    var option = '<option value="' + item.AreaID + '" class="areaName">' + item.AreaName + '</option>';
                    $('#area').append(option);
                });

            });
        });

        $('#area').change(function () {
            var _this = $(this);
            var areaId = _this.val();
            var _token = ('{{csrf_token()}}');
            var url = ('{{route('getNeighborhoods')}}');

            $.ajax({
                method: 'post',
                url: url,
                data: {areaId: areaId, _token: _token}
            }).done(function (data) {

                $('#neighborhood option[class="neighborhoodName"]').remove();
                $.each(data, function (index, item) {
                    var option = '<option value="' + item.NeighborhoodID + '" class="neighborhoodName">' + item.NeighborhoodName + '</option>';
                    $('#neighborhood').append(option);
                });

            });

        });


        var map;
        var markers = [];

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: new google.maps.LatLng(36.99035065926988, 35.33048629760742),
                mapTypeId: 'terrain'
            });

            var input = document.getElementById('pac-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function () {

                var place = autocomplete.getPlace();


                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }


            });

            google.maps.event.addListener(map, 'click', function (event) {
                deleteMarkers();
                var coordinate = event.latLng;
                var latitude = coordinate.lat();
                var longitude = coordinate.lng();

                $('#latitude').val(latitude);
                $('#longitude').val(longitude);

                $.ajax({
                    method: 'get',
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?&latlng=' + latitude + ',' + longitude
                }).done(function (data) {
                    console.log(data);
                    var address = data.results[0].formatted_address;
                    /*
                    var address_components = data.results[0].address_components;
                    var city = address_components[4].long_name;
                    var district = address_components[3].long_name;
                    var neighborhood = address_components[2].long_name;


                    $('#city').val(city);
                    $('#district').val(district);
                    $('#neighborhood').val(neighborhood);
                    */
                    $('#address').text(address);

                    addMarker(coordinate, address);
                });
            });
        }

        function addMarker(location, address) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            var infowindow = new google.maps.InfoWindow({
                content: address
            });
            infowindow.open(map, marker);
            markers.push(marker);
        }

        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll(null);
        }

        // Shows any markers currently in the array.
        function showMarkers() {
            setMapOnAll(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }


    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7DIJ048XamPeX1x2e_DS_NBHVQT4CyrE&libraries=places&callback=initMap">
    </script>
@endsection