@extends('layouts.app')

@section('content')


    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">Market Haritası</h3>
            <input type="text" id="pac-input" class="form-control" placeholder="Adres girin...">
        </div>

        <div class="box-body">
            <div id="map" style="width: 100%; height: 300px"></div>
        </div>

    </div>

@endsection

@section('scripts')

    <script>

        var map;
        var markers = [];

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: new google.maps.LatLng(36.99035065926988, 35.33048629760742),
                mapTypeId: 'terrain'
            });


            let markets = ('<?= $markets ?>');
            let marketArr = JSON.parse(markets);


            $.each(marketArr, function (index, item) {
                console.log(item.maxDistance);
                let location = {lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)};
                addMarker(location, item.marketName);
                drawCircle(map, parseInt(item.maxDistance), location);
            });


            var input = document.getElementById('pac-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function () {

                var place = autocomplete.getPlace();


                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }


            });

        }

        function drawCircle(map, radius, location) {
            var cityCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                center: location,
                radius: radius,
                geodesic: true
            });
        }

        function addMarker(location, marketName) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            var infowindow = new google.maps.InfoWindow({
                content: marketName
            });
            infowindow.open(map, marker);
        }

    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7DIJ048XamPeX1x2e_DS_NBHVQT4CyrE&libraries=places&callback=initMap">
    </script>

@endsection