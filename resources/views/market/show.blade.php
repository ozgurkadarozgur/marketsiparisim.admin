@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$market->marketName}}</h3> - <a href="{{route('createMessage', $market->id)}}">Mesaj Gönder</a>
                    <!-- /.box-header -->
                </div>
                <div class="box-body">
                    <a href="{{env('APP_URL').'Market/'.$market->id.'/CreateUser'}}">Kullanıcı Oluştur</a>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>

@endsection