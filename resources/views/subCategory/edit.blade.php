@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$subCategory->categoryName}}</h3>&nbsp;
                    <!-- /.box-header -->
                </div>

                <div class="box-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <strong>{{session()->get('message')}}</strong>
                        </div>
                    @endif
                    <form action="{{env('APP_URL').'SubCategory/'.$subCategory->id.'/Update'}}" method="post"
                          class="form-horizontal">
                        {{csrf_field()}}

                        <div class="form-group{{ $errors->has('topCategoryPhoto') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Resim</label>
                            <?php
                            $photo = json_decode($subCategory->photo);
                            ?>
                            <div class="col-sm-10">

                                <img id="topCategoryPhoto" class="img img-responsive img-circle"
                                     {{--src="{{env('PHOTO_URL').Storage::url('app/'.$photo->small)}}"--}}
                                     style="cursor: pointer">

                                <a href="{{env('APP_URL').'SubCategory/'.$subCategory->id.'/EditPhoto'}}">Resim
                                    Değiştir</a>
                                @if($errors->has('topCategoryPhoto'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('topCategoryPhoto') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('topCategoryName') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Kategori</label>

                            <div class="col-sm-10">
                                <input type="text" name="topCategoryName" class="form-control"
                                       value="{{$subCategory->categoryName}}" placeholder="Kategori Adı">
                                @if($errors->has('topCategoryName'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('topCategoryName') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="box-footer pull-right">

                            <input type="submit" class="btn btn-primary" value="Kaydet"/>
                            <a href="{{env('APP_URL').'TopCategories'}}" class="btn btn-danger">İptal</a>

                        </div>

                    </form>

                </div>

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>

@endsection
