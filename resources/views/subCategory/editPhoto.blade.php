@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$subCategory->categoryName}} | Resim Değiştir</h3>&nbsp;
                    <!-- /.box-header -->
                </div>

                <div class="box-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <strong>{{session()->get('message')}}</strong>
                        </div>
                    @endif
                    <form action="{{env('APP_URL').'SubCategory/'.$subCategory->id.'/UpdatePhoto'}}" method="post"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="form-group{{ $errors->has('subCategoryPhoto') ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label">Resim</label>
                            <?php
                            $photo = json_decode($subCategory->photo);
                            ?>
                            <div class="col-sm-10">
                                <img id="subCategoryPhoto" class="img img-responsive img-circle"
                                     {{--src="{{env('PHOTO_URL').Storage::url('app/'.$photo->small)}}"--}}
                                     style="cursor: pointer;min-width: 109px;min-height: 109px;">
                                <input id="inputSubCategoryPhoto" name="subCategoryPhoto" type="file"
                                       class="form-control"
                                       style="display: none;"/>
                                @if($errors->has('subCategoryPhoto'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('subCategoryPhoto') }}</strong>
                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="box-footer pull-right">

                            <input type="submit" class="btn btn-primary" value="Kaydet"/>
                            <a href="{{env('APP_URL').'TopCategory/'.$subCategory->id.'/Edit'}}" class="btn btn-danger">İptal</a>

                        </div>

                    </form>

                </div>

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>

@endsection

@section('scripts')

    <script>
        $("#subCategoryPhoto").click(function () {
            $("#inputSubCategoryPhoto").trigger('click');
        });


    </script>

@endsection
