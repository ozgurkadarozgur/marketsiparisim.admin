@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{$market->marketName}}</h3>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p>Mesaj</p>
                    <form action="{{route('sendMessage', $market->id)}}" method="post">
                        {{csrf_field()}}
                        <textarea class="form-control" name="message" rows="2" style="width: 100%;" placeholder="Mesaj..."></textarea>
                        <br />
                        <input class="btn btn-primary pull-right" type="submit" value="Gönder" />
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h4>Önceki Mesajlar</h4>

                    @forelse($messages as $message)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    {{$message->created_at}}
                                </div>
                            </div>
                            <div class="panel-body">{{$message->message}}</div>
                        </div>
                    @empty
                        <p>Hiç mesaj bulunamadı.</p>
                    @endforelse

                </div>
            </div>

        </div>

    </div>

@endsection