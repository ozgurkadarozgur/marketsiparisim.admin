@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Bildirim Türü Seçin</h3>
                </div>
                <div class="box-body">

                    <label><input type="radio" nType="nTG" name="nType"/> Genel Bildirim</label>
                    <br/>
                    <label><input type="radio" nType="nTP" name="nType"/> Ürün Bildirimi</label>

                </div>
            </div>
        </div>
    </div>


    <div id="ntfMarketContent"></div>
    <div id="ntfProductContent"></div>
    <div id="ntfMessageContent"></div>


@endsection

@section('scripts')

    <script>


        $('input[name="nType"]').click(function () {
            let type = $(this).attr('nType');
            switch (type) {
                case 'nTG' : {
                    let content = generalNtfContent();
                    setNtfMarketContent(content);
                    break;
                }
                case 'nTP' : {
                    let content = privateNtfContent();

                    getMarketList().then(function (data) {
                        setNtfMarketContent(content);
                        data.map(function (item, index) {
                            $('#marketList').append('<option value="' + item.id + '">' + item.marketName + '</option>');
                        });
                        $('#marketList').select2();

                        $('#marketSecNext').click(function () {
                            let prdContent = privateNtfProductContent();
                            let marketId = $('#marketList').val();
                            getProductList(marketId).then(function (data) {
                                setNtfProductContent(prdContent);
                                $('#productList').html('');
                                data.map(function (item, index) {
                                    $('#productList').append('<option value="' + item.stockId + '">' + item.productName+ '</option>');
                                });
                                $('#productList').select2();
                            })
                        });

                        $(document).on('click', '#urunSecNext', function () {
                            let msgContent = privateNtfMessageContent();
                            setNtfMsgContent(msgContent);
                        });

                    });
                    break;
                }
            }
        });

        function getMarketList() {

            let url = ('{{route('getMarketList')}}');

            return $.ajax({
                method: 'get',
                url: url
            }).done(function (data) {
                return data;
            });

        }

        function getProductList(marketId){
            let url = ('{{route('getMarketProducts')}}');
            let _token = ('{{csrf_token()}}');
            return $.ajax({
                method: 'post',
                url: url,
                data: {_token: _token, marketId :marketId}
            }).done(function (data) {
                return data;
            });
        }

        $(document).on('click', '#sendPrdNtf', function () {

            let url = ('{{route('sendProductNtf')}}');
            let _token = ('{{csrf_token()}}');

            let marketId = $('#marketList').val();
            let productId = $('#productList').val();
            let message = $('textarea').val();

            $.ajax({
                method: 'post',
                url : url,
                data: {_token : _token, marketId : marketId, productId : productId, message: message}
            }).done(function (data) {
                console.log(data);
            });

        });

        $(document).on('click', '#sendGnrlNtf', function () {

            let url = ('{{route('sendGeneralNtf')}}');
            let _token = ('{{csrf_token()}}');

            let message = $('textarea').val();

            $.ajax({
                method: 'post',
                url : url,
                data: {_token : _token, message: message}
            }).done(function (data) {
                console.log(data);
            });

        });

        function setNtfMarketContent(content) {
            $('#ntfMarketContent').html(content);
        }

        function setNtfProductContent(content) {
            $('#ntfProductContent').html(content);
        }

        function setNtfMsgContent(content){
            $('#ntfMessageContent').html(content);
        }

        function generalNtfContent() {
            let content = '<div class="box box-primary">\n' +
                '        <div class="box-header with-border">\n' +
                '            <h3 class="box-title">Gönderilecek Mesaj</h3>\n' +
                '        </div>\n' +
                '        <div class="box-body">\n' +
                '\n' +
                '<textarea id="ntfMessage" class="form-control"></textarea>' +
                '<br /><input type="button" id="sendGnrlNtf" class="btn btn-primary pull-right" value="Gönder"/>' +
                '\n' +
                '        </div>\n' +
                '    </div>';

            return content;
        }

        function privateNtfContent() {
            let content = '<div class="box box-primary">\n' +
                '        <div class="box-header with-border">\n' +
                '            <h3 class="box-title">Market Seçin</h3>\n' +
                '        </div>\n' +
                '        <div class="box-body">\n' +
                '\n' +
                '<select id="marketList" class="form-control select2">' +
                '</select>' +
                '<br />'+
                '<input type="button" id="marketSecNext" class="btn btn-primary btn-sm pull-right" value="Devam" />'+
                '\n' +
                '        </div>\n' +
                '    </div>';

            return content;
        }

        function privateNtfMessageContent(){
            let content = '<div class="box box-primary">\n' +
                '        <div class="box-header with-border">\n' +
                '            <h3 class="box-title">Gönderilecek Mesaj</h3>\n' +
                '        </div>\n' +
                '        <div class="box-body">\n' +
                '\n' +
                '<textarea class="form-control"></textarea>' +
                '<br /><input type="button" id="sendPrdNtf" class="btn btn-primary pull-right" value="Gönder"/>' +
                '\n' +
                '        </div>\n' +
                '    </div>';

            return content;
        }

        function privateNtfProductContent(){
            let content = '<div class="box box-primary">\n' +
                '        <div class="box-header with-border">\n' +
                '            <h3 class="box-title">Ürün Seçin</h3>\n' +
                '        </div>\n' +
                '        <div class="box-body">\n' +
                '\n' +
                '<select id="productList" class="form-control select2">' +
                '</select>' +
                '<br />'+
                '<input type="button" id="urunSecNext" class="btn btn-primary btn-sm pull-right" value="Devam" />'+
                '\n' +
                '        </div>\n' +
                '    </div>';

            return content;
        }

    </script>

@endsection