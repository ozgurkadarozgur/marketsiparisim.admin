@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Gürpınar Olmayan Ürünler</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Barkod</th>
                    <th>Ürün Adı</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($olmayanUrunler as $product)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$product->barcode}}</td>
                        <td>{{$product->productName}}</td>
                        <td><a href="{{route('gurpinarUrunGoster', $product->id)}}">İncele</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>




@endsection