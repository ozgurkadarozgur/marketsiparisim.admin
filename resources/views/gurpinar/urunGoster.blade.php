@extends('layouts.app')

@section('content')

    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{$urun->productName}}</h3>
        </div>

        <form method="post" action="{{route('urunKaydet', $urun->id)}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <table class="table table-responsive table-bordered">
                    <tr>
                        <th>Temel Kategori</th>
                        <td>
                            <select name="topCategory" id="topCategory" class="form-control">
                                @foreach($topCategories as $topCategory)
                                    <option value="{{$topCategory->id}}">{{$topCategory->categoryName}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Alt Kategori</th>
                        <td>
                            <select name="subCategory" id="subCategory" class="form-control">
                                @foreach($subCategories as $subCategory)
                                    <option value="{{$subCategory->id}}">{{$subCategory->categoryName}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('subCategory'))
                                <strong style="color: red;">{{$errors->first('photo')}}</strong>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Marka</th>
                        <td>
                            <select name="brand" id="brand" class="form-control select2">
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->brandName}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('brand'))
                                <strong style="color: red;">{{$errors->first('photo')}}</strong>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Resim</th>
                        <td>
                            <input name="photo" type="file" class="form-control" />
                            @if($errors->has('photo'))
                                <strong style="color: red;">{{$errors->first('photo')}}</strong>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="float: right;">
                                <input class="btn btn-primary" type="submit" value="Kaydet"/>
                                <a href="{{route('olmayanUrunler')}}" class="btn btn-danger">İptal</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </form>

    </div>

@endsection


@section('scripts')

    <script>

        $(document).ready(function () {
           $('#brand').select2();
           $('#topCategory').select2();
           $('#subCategory').select2();
        });

        $('#topCategory').change(function () {
            let tId = $(this).val();
            let _token = ('{{csrf_token()}}');
            let url = ('{{route('altKategoriGetir')}}')

            $.ajax({
                method: 'post',
                url: url,
                data: {tId: tId, _token: _token}
            }).done(function (data) {
                $(' #subCategory').html('');
                $.each(data, function (index, item) {
                    $('#subCategory').append('<option value="'+item.id+'">'+item.categoryName+'</option>');
                });
            });

        });
    </script>

@endsection