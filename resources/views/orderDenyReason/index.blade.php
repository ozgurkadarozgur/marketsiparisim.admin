@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Sipariş Reddetme Nedenleri - <a href="{{route('createOrderDenyReason')}}">Yeni
                    Ekle</a></h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sebep</th>
                </tr>
                </thead>
                <tbody>
                @forelse($reasons as $reason)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$reason->reason}}</td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="2">Kayıt bulunamadı</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

    </div>

@endsection