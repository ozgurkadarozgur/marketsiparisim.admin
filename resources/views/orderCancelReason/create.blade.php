@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Sipariş İptal Nedeni Ekle</h3>
        </div>

        <form class="form-horizontal" action="{{route('storeOrderCancelReason')}}" method="post">
            {{csrf_field()}}
            <div class="box-body">

                <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Sebep</label>

                    <div class="col-sm-10">
                        <input type="text" name="reason" class="form-control">
                        @if($errors->has('reason'))
                            <span class="help-block">
                        <strong>{{ $errors->first('reason') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="box-footer">
                    <div>
                        <input type="submit" class="btn btn-primary" value="Ekle">
                    </div>
                </div>
            </div>
        </form>

    </div>

@endsection