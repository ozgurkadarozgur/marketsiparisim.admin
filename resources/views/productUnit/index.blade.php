@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Birimler</h3>&nbsp;| <a href="{{env('APP_URL').'NewProductUnit'}}">Yeni Ekle</a>
                    <!-- /.box-header -->
                </div>
                <!-- /.box -->

                <div class="box-body">
                    <table class="table table-bordered table-responsive">
                        <tr>
                            <th>Birim Adı</th>
                        </tr>
                        @foreach($productUnits as $unit)
                            <tr>
                                <td>{{$unit->unitName}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>




@endsection