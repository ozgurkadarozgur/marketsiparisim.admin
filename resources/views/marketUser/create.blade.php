@extends('layouts.app')

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{$market->marketName. ' - Kullanıcı Oluştur'}}</h3>
        </div>
        <!-- /.box-header -->
        @if(session()->has('message'))
            <div class="alert alert-success">
                <strong>{{session()->get('message')}}</strong>
            </div>
    @endif
    <!-- form start -->

        <form class="form-horizontal" method="post"
              action="{{env('APP_URL').'Market/'.$market->id.'/storeUser'}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Ad-Soyad</label>

                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" value="{{($market->user) ? $market->user->name : ''}}"
                               placeholder="Ad - Soyad">
                        @if($errors->has('name'))
                            <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Kullanıcı Adı</label>

                    <div class="col-sm-10">
                        <input type="text" name="username" class="form-control" value="{{($market->user) ? $market->user->username : ''}}"
                               placeholder="Kullanıcı Adı">
                        @if($errors->has('username'))
                            <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">E-Mail</label>

                    <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" value="{{($market->user) ? $market->user->email : ''}}"
                               placeholder="E-Mail">
                        @if($errors->has('email'))
                            <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-sm-2 control-label">Şifre</label>

                    <div class="col-sm-10">
                        <input type="text" name="password" class="form-control" value=""
                               placeholder="Şifre">
                        @if($errors->has('password'))
                            <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Ekle</button>
                </div>
                <!-- /.box-footer -->

            </div>
        </form>

    </div>

@endsection
