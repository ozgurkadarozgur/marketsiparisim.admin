<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', 'api\v1\CustomerController@getUserInfo');


Route::group(['namespace' => 'api\v1', 'prefix' => 'v1'], function () {

    Route::get('/contract-{type}', 'ContractController@getContract');

    Route::post('/register', 'CustomerController@register');
    Route::post('/findMarkets', 'MarketController@find');
    Route::get('/Market/{marketId}/TopCategories', 'MarketController@topCategories');
    Route::get('/Market/{marketId}/TopCategory/{topCategoryId}/SubCategories', 'MarketController@subCategories');
    Route::get('/Market/{marketId}/SubCategory/{subCategoryId}/Products/{limit?}', 'MarketController@products');
    Route::post('/getProductDetail', 'MarketController@product');
    Route::get('/Market/{marketId}/SpecialProducts', 'MarketController@specialProducts');

    Route::post('/resetPasswordRequest', 'ResetPasswordController@newRequest');
    Route::post('/resetPassword', 'ResetPasswordController@reset');

    Route::post('/searchByKeyword', 'ProductController@searchByKeyword');
    Route::post('/searchByBarcode', 'ProductController@searchByBarcode');
    Route::get('/sendSMS', 'PhoneConfirmationController@sendSMS');

    Route::post('/newMarketRecommendation', 'MarketRecommendationController@newRecommendation');

    Route::post('/checkReference', 'ReferenceController@check');
    Route::get('/testPass', 'TestController@testPass');
    Route::get('/testPrd', 'TestController@prd');
    Route::post('/getAlternativesForShopping', 'ShoppingCartController@getAlternatives');
});

Route::group(['namespace' => 'api\v1', 'prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    Route::post('/verifyPhone', 'PhoneConfirmationController@verify');
    //Route::get('/user', 'CustomerController@getUserInfo');
    Route::post('/pay', 'My3dController@pay');
    //Customer Menu

    //--shoppingCart
    Route::post('/postShoppingCarts', 'ShoppingCartController@postShoppingCarts');
    Route::post('/getShoppingCarts', 'ShoppingCartController@getShoppingCarts');
    Route::post('/newShoppingCart', 'ShoppingCartController@newShoppingCart');
    Route::post('/destroyShoppingCart', 'ShoppingCartController@destroyShoppingCart');
    Route::post('/destroyShoppingCarts', 'ShoppingCartController@destroyShoppingCarts');
    Route::post('/destroyShoppingCartItem', 'ShoppingCartController@destroyShoppingCartItem');
    Route::post('/getShoppingAmount', 'ShoppingCartController@getShoppingAmount');
    //--end shoppingCart

    //--address
    //Route::get('/getCities', 'AddressController@getCities');
    Route::get('/getAddresses', 'CustomerController@getAddresses');
    Route::post('/newAddress', 'CustomerController@newAddress');
    Route::post('/destroyAddress', 'CustomerController@destroyAddress');
    Route::post('/getAddressesForOrder', 'CustomerController@getAddressForOrder');
    //--end address

    //--favorite products
    Route::post('/getFavoriteProducts', 'FavoriteProductController@getFavoriteProducts');
    Route::post('/newFavoriteProduct', 'FavoriteProductController@newFavoriteProduct');
    Route::post('/destroyFavoriteProduct', 'FavoriteProductController@destroyFavoriteProduct');
    //--end favorite products

    //past orders
    Route::get('/getPastOrders', 'OrderController@getPastOrders');
    //end past orders

    //shoppingList
    Route::get('/getShoppingList', 'ShoppingListController@getShoppingList');
    Route::post('/newShoppingListItem', 'ShoppingListController@newItem');
    Route::post('/destroyShoppingListItem', 'ShoppingListController@destroyItem');
    Route::post('/editShoppingListItem', 'ShoppingListController@editItem');
    //end shoppingList

    //end Customer Menu

    Route::post('/registerForNtf', 'NotificationController@register');

    Route::post('/postShoppingCartsForAlternatives', 'ShoppingCartController@postShoppingCartsForAlternatives');

    Route::post('/phoneUpdateRequest', 'CustomerController@requestForUpdatePhone');
    Route::post('/completeUpdatePhone', 'CustomerController@completeUpdatePhone');
    Route::get('/getTimeToPhoneUpdateVerify', 'CustomerController@getTimeToPhoneUpdateVerify');
    Route::get('/reSendSMSForUpdatePhone', 'CustomerController@reSendSMSForUpdatePhone');
    //order
    Route::post('/order', 'OrderController@order');
    Route::post('/getOrderDetails', 'OrderController@getOrderDetails');
    Route::post('/reOrder', 'OrderController@reOrder');
    //end order
    Route::get('/getMarketMoney', 'CustomerController@getMarketMoney');
    Route::get('/destroyReference', 'CustomerController@destroyReference');
    Route::post('/changeName', 'CustomerController@changeName');
    Route::post('/changePassword', 'CustomerController@changePassword');
    Route::post('/changeNotificationStatus', 'CustomerController@changeNotificationStatus');
    Route::get('/sendVerifyCode', 'PhoneConfirmationController@sendSMS');
    Route::get('/getTimeToVerify', 'PhoneConfirmationController@getTimeToVerify');
});
