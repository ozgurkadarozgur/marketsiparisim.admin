<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/map', 'HomeController@map');
Route::get('/', 'HomeController@index');

Route::get('/Settings', 'AdminSettingController@index')->name('settings');
Route::get('/ChangePassword', 'AdminSettingController@changePassword')->name('changePassword');
Route::post('/updatePassword', 'AdminSettingController@updatePassword')->name('updatePassword');
//notification

Route::get('/NotificationPlaning', 'NotificationController@notificationPlaning')->name('notificationPlaning');

//end - notification

//order
Route::get('/OrderCancelReasons', 'OrderCancelReasonController@index')->name('orderCancelReasons');
Route::get('/NewOrderCancelReason', 'OrderCancelReasonController@create')->name('createOrderCancelReason');
Route::post('/storeOrderCancelReason', 'OrderCancelReasonController@store')->name('storeOrderCancelReason');

Route::get('/OrderDenyReasons', 'OrderDenyReasonController@index')->name('orderDenyReasons');
Route::get('/NewOrderDenyReason', 'OrderDenyReasonController@create')->name('createOrderDenyReason');
Route::post('/storeOrderDenyReason', 'OrderDenyReasonController@store')->name('storeOrderDenyReason');


Route::get('/OrderNotDeliverReasons', 'OrderNotDeliverReasonController@index')->name('orderNotDeliverReasons');
Route::get('/NewOrderNotDeliverReason', 'OrderNotDeliverReasonController@create')->name('createOrderNotDeliverReason');
Route::post('/storeOrderNotDeliverReason', 'OrderNotDeliverReasonController@store')->name('storeOrderNotDeliverReason');
//end order

//Top Category
Route::get('/TopCategories', 'TopCategoryController@index');
Route::post('/storeTopCategory', 'TopCategoryController@store');
Route::get('/TopCategory/{categoryId}/Edit', 'TopCategoryController@edit');
Route::post('TopCategory/{categoryId}/Update', 'TopCategoryController@update');
Route::get('/TopCategory/{categoryId}/EditPhoto', 'TopCategoryController@editPhoto');
Route::post('/TopCategory/{categoryId}/UpdatePhoto', 'TopCategoryController@updatePhoto');
Route::get('/TopCategory/{categoryId}/Destroy', 'TopCategoryController@destroy');


//-json ajax
Route::post('/getSubCategories', 'TopCategoryController@getSubCategories')->name('getSubCategories');
Route::get('/getMarketList', 'MarketController@getMarketList')->name('getMarketList');
Route::post('/getMarketProducts', 'ProductController@getMarketProducts')->name('getMarketProducts');
Route::post('/sendProductNtf', 'NotificationController@sendProductNtf')->name('sendProductNtf');
Route::post('/sendGeneralNtf', 'NotificationController@sendGeneralNtf')->name('sendGeneralNtf');
//-

//end Top Category

//Sub Category
Route::post('/topCategory/{topCategoryId}/storeSubCategory', 'SubCategoryController@store');
Route::get('/SubCategory/{categoryId}', 'SubCategoryController@show');
Route::get('/SubCategory/{categoryId}/Edit', 'SubCategoryController@edit');
Route::post('/SubCategory/{categoryId}/Update', 'SubCategoryController@update');
Route::get('/SubCategory/{categoryId}/EditPhoto', 'SubCategoryController@editPhoto');
Route::post('/SubCategory/{categoryId}/UpdatePhoto', 'SubCategoryController@updatePhoto');
//end Sub Category

//Product
Route::get('/SubCategory/{categoryId}/NewProduct', 'ProductController@create');
Route::post('/SubCategory/{categoryId}/StoreProduct', 'ProductController@store');
Route::get('/Product/{id}', 'ProductController@show');
Route::post('/Product/{productId}/Update', 'ProductController@update');
Route::get('/Product/{productId}/Destroy', 'ProductController@destroy');
Route::get('/Product/{productId}/EditPhoto', 'ProductController@editPhoto');
Route::post('/Product/{productId}/UpdatePhoto', 'ProductController@updatePhoto');
Route::get('/SearchProduct', 'ProductController@search')->name('searchProduct');
Route::post('/findProductByBarcode', 'ProductController@findProductByBarcode')->name('findProductByBarcode');
//end Product

//Product Requests
Route::get('/NewProductRequests', 'NewProductRequestController@index')->name('newProductRequests');
Route::get('/ShowRequestedProduct/{id}', 'NewProductRequestController@show')->name('showRequestedProduct');
Route::post('/ApproveRequestedProduct/{id}', 'NewProductRequestController@approve')->name('approveRequestedProduct');
Route::get('/CancelRequestedProduct/{id}', 'NewProductRequestController@cancel')->name('cancelRequestedProduct');
//end Product Requests

//ProductUnit
Route::get('/ProductUnits', 'ProductUnitController@index');
Route::get('/NewProductUnit', 'ProductUnitController@create');
Route::post('/storeProductUnit', 'ProductUnitController@store');
//end ProductUnit

//Market
Route::get('/MarketMap', 'MarketController@map')->name('marketMap');
Route::get('/Markets', 'MarketController@index');
Route::get('/Market/{marketId}', 'MarketController@show');
Route::get('/NewMarket', 'MarketController@create');
Route::post('/storeMarket', 'MarketController@store');
//end Market

//MarketAddress - JSON

Route::post('/getCountiesByCityId', 'MarketAddressController@getCountiesByCityId')->name('getCounties');
Route::post('/getAreasByCountyId', 'MarketAddressController@getAreasByCountyId')->name('getAreas');
Route::post('/getNeighborhoodsByAreaId', 'MarketAddressController@getNeighborhoodsByAreaId')->name('getNeighborhoods');

//end MarketAddress



//MarketUser
Route::get('Market/{marketId}/CreateUser', 'MarketUserController@create');
Route::post('Market/{marketId}/storeUser', 'MarketUserController@store');
//end MarketUser

//Brand
Route::get('/Brands', 'BrandController@index');
Route::get('/NewBrand', 'BrandController@create');
Route::post('/storeBrand', 'BrandController@store');
Route::get('/Brand/{brandId}/Edit', 'BrandController@edit');
Route::post('/Brand/{brandId}/Update', 'BrandController@update');
//end Brand


//StatisticsMarket
Route::get('/StatisticMarket', 'StatisticMarketController@index')->name('showMarketsForStatistic');
Route::get('/StatisticMarket/{marketId}', 'StatisticMarketController@show')->name('showMarketStatistic');
//endStatisticsMarket

//StatisticsCustomer
Route::get('/StatisticCustomer', 'StatisticCustomerController@index')->name('showCustomersForStatistic');
Route::get('/StatisticCustomer/{customerId}', 'StatisticCustomerController@show')->name('showCustomerStatistic');
Route::get('/StatisticCustomer/ShowOrder/{orderId}', 'StatisticCustomerController@showOrder')->name('statisticShowOrder');
//endStatisticsCustomer

//StatisticsProducts
Route::get('/StatisticProduct', 'StatisticProductController@index')->name('showProductsForStatistic');
Route::get('/StatisticProduct/{productId}', 'StatisticController@show')->name('showProductStatistic');
//end StatisticsProducts

//StatisticsProducts

//end StatisticsProducts

Route::get('/CreateMessage/{marketId}', 'SysMessageController@create')->name('createMessage');
Route::post('/sendMessage/{marketId}', 'SysMessageController@send')->name('sendMessage');


Route::get('/SysInvoice', 'SysInvoiceController@index');
Route::get('/Invoice', 'InvoiceController@index');
Route::get('/Invoice/{marketId}', 'InvoiceController@show');
Route::get('/Bill/{billId}', 'BillController@show')->name('showBill');
Route::get('/Bills/{marketId}', 'BillController@index')->name('bills');
Route::get('/CreateBill/{marketId}', 'BillController@create')->name('createBill');
Route::post('/storeBill/{marketId}', 'BillController@store')->name('storeBill');
//Route::get('/gecmisFaturaOlustur', 'InvoiceController@gecmisFaturaOlustur');
//deneme
//Route::get('/gurpinar', 'GurpinarController@aktar');
Route::get('/olmayanUrunler', 'GurpinarController@olmayanUrunler')->name('olmayanUrunler');
//Route::get('/kayipUrunAktar', 'GurpinarController@kayipUrunAktar');
Route::get('/gurpinarUrunler', 'GurpinarController@urunler');
Route::get('/gurpinarUrunGoster/{urunId}', 'GurpinarController@show')->name('gurpinarUrunGoster');
//json
Route::post('altKategoriGetir', 'GurpinarController@altKategoriGetir')->name('altKategoriGetir');
Route::post('/urunKaydet/{urunId}', 'GurpinarController@urunKaydet')->name('urunKaydet');


Route::get('/sanalpos', 'TestController@sanalpos');
Route::get('/bill', 'HomeController@bill');
Route::get('/yeniadres', 'HomeController@yeniadres');
Route::post('urunEkle', 'ProductController@urunStore');
Route::get('urunInsert', 'ProductController@urunInsert');
Route::get('/usr', 'HomeController@usr');
Route::get('/ordertry/{marketId}', 'HomeController@order');

Route::get('/logo', 'TestController@logo');
Route::get('/ntf', 'TestController@sendNotification');

Route::get('/ntfTest', 'TestController@ntfTest');
Route::get('/ntfTestRead', 'TestController@ntfTestRead');

//Route::get('/olmayanUrunBul', 'TestController@olmayanUrunBul');
//Route::get('/fiyatDuzelt', 'TestController@fiyatDuzelt');

//Route::get('/gurpinarStokAktar', 'GurpinarController@gurpinarStokAktar');
//Route::get('/karmarStokAktar', 'TestController@karmarStokAktar');
//Route::get('/cinarStokAktar', 'TestController@cinarStokAktar');
//Route::get('/aksutStokAktar', 'TestController@aksutStokAktar');
//end deneme